#%%
from pyepw.epw import EPW
from pathlib import Path
import pandas as pd
import datetime

P = Path(__file__).parent.absolute()


def retrieve_column(epw_file, col_name, year=None):
    """Return a column of values from an EPW file.

    :param epw_file: Filename of the EPW.
    :type epw_file: str or Path
    :param col_name: Name of the column
    :type col_name: str
    :param year: Year which has to be assigned to EPW data since it does not
        really have this field, defaults to None
    :type year: int, optional
    :return: DataFrame with DateTime indeces and selected column values
    :rtype: class pandas.core.frame.DataFrame
    """
    epw = EPW()
    epw.read(epw_file)
    length = len(epw.weatherdata)
    if year is not None:
        years = [year for y in range(length)]
    else:
        years = [epw.weatherdata[y].year for y in range(length)]
    days = [epw.weatherdata[y].day for y in range(length)]
    months = [epw.weatherdata[y].month for y in range(length)]
    hours = [epw.weatherdata[y].hour - 1 for y in range(length)]
    indeces = [
        datetime.datetime.strptime(
            " ".join([str(x), str(y).zfill(2), str(z), str(w).zfill(2)]),
            "%Y %m %d %H",
        )
        for x, y, z, w in zip(years, months, days, hours)
    ]
    length = len(epw.weatherdata)
    column = [getattr(epw.weatherdata[h], col_name) for h in range(length)]
    df = pd.DataFrame({"{}".format(col_name): column}, index=indeces)

    return df
