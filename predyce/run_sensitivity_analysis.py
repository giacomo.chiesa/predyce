#!/usr/bin/env python
# coding: utf-8
from predyce.IDF_class import IDF
from predyce.runner import Runner
from pathlib import Path
import json
import yaml
import argparse
import tempfile

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()

# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

# Argument parameters which override the ones in configuration file.
parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data absolute path (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=128, type=int)
parser.add_argument("-d", "--output-directory", help="Output directory absolute path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument("-f", "--input-file", help="Input data file absolute path (default: in.json in executable directory" , default=P / "in.json")
parser.add_argument("-j", "--jobs", help= "Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data", help="Measured data file absolute path (default: None)", default="")
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=Path(tempfile.gettempdir()) / "predyce")
parser.add_argument("-w", "--weather", help= "Weather file absolute path (default: "" and must be in case specified in input JSON file)", default=CONFIG["epw"])
parser.add_argument("model", help= "Building model path (IDF format only accepted)")

args = parser.parse_args()

# IDF file.
fname1 = args.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
INPUT = args.input_file

# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if weather.is_file():
    EPW = weather
    EPW_DIR = weather.parent
elif weather.is_dir():
    EPW = None
    EPW_DIR = weather
else:
    raise IOError("Error in the EPW weather file")

idf1 = IDF(fname1, EPW)

# Checkpoint data.
CHECKPOINT_DATA = args.checkpoint_data


def main():
    # Load input file.
    with open(INPUT, "r") as f:
        input = json.loads(f.read())

    config = {
        "plot_dir": args.plot_directory,
        "temp_dir": args.temp_directory,
        "out_dir": args.output_directory,
        "num_of_cpus": args.jobs,
        "epw_dir": EPW_DIR,
        "data_true_dir": args.measured_data,
    }

    # Runner instance.
    runner = Runner(
        idf1,
        input,
        **config,
        checkpoint_interval=args.checkpoint_interval,
        checkpoint_data=CHECKPOINT_DATA,
        graph=True
    )

    runner.create_dataframe(input, args.original)
    print(runner.df)
    runner.run()
    runner.print_summary()


if __name__ == "__main__":
    main()
