#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions for KPI computations."""
import pandas as pd
import datetime as dt
import numpy as np
from predyce import epw_reader
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import Axes3D
import statsmodels.api as sm
import copy
from pathlib import Path
import os

# TODO adjust nomenclature and udm (particularly for consumption)
class KPI:
    """Class which computes KPIs."""

    def __init__(self, plot_dir, graph=False):
        """
        :param plot_dir: Directory where to save plots
        :type plot_dir: str or Path
        :param graph: Whether to save plots, defaults to `False`
        :type graph: bool, optional
        """
        self.plot_dir = Path(plot_dir)
        self.graph = graph
        if self.graph:
            try:
                os.mkdir(self.plot_dir)
            except Exception:
                pass

    def fictitious_cooling(self, dataframe_off, dataframe_on, 
                           eu_norm='16798-1:2019', alpha=0.8):
        """Return fictitious cooling computed as the amount of cooling
        consumption needed in a building without mechanical system in order
        to reach category I comfort of adaptive comfort model.

        :param dataframe_off: dataframe containing at least 
            "Date/Time", "T_op_i[C]", "T_db_e[C]"
            columns in order to compute adaptive comfort model categories.
        :type dataframe_off: class:`pandas.core.frame.DataFrame`
        :param dataframe_on: dataframe containing at least "Date/Time" and 
            "Q_c[Wh/m2]" columns, of the same building with 
            HVAC installad and ACH ventilation set to 0.
        :type dataframe_on: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious cooling total [kWh/m2]
        :rtype: float
        """
        
        df_off = copy.deepcopy(dataframe_off)
        df_on = copy.deepcopy(dataframe_on)

        # Aggregate hourly
        df_on = df_on.set_index("Date/Time")
        df_on = df_on.resample("1H").agg({"Q_c[Wh/m2]": "sum"})

        df_off = df_off.set_index("Date/Time")
        df_off = df_off.resample("1H").mean()

        df_off["Q_c[Wh/m2]"] = df_on["Q_c[Wh/m2]"]
        df_off = df_off.reset_index()
        col_index = df_off.columns.get_loc("T_db_e[C]")

        # Adaptive Comfort Model formula
        if eu_norm == '16798-1:2019':
            for i in range(7*24,df_off.shape[0]):
                df_off.loc[i,"T_rm"] = (df_off.iloc[i-1*24:i,col_index].mean()+\
                                            0.8*df_off.iloc[i-2*24:i-1*24,\
                                                col_index].mean() +\
                                            0.6*df_off.iloc[i-3*24:i-2*24,\
                                                col_index].mean() +\
                                            0.5*df_off.iloc[i-4*24:i-3*24,\
                                                col_index].mean() +\
                                            0.4*df_off.iloc[i-5*24:i-4*24,\
                                                col_index].mean() +\
                                            0.3*df_off.iloc[i-6*24:i-5*24,\
                                                col_index].mean() +\
                                            0.2*df_off.iloc[i-7*24:i-6*24,\
                                                col_index].mean())/3.8

        elif eu_norm == '15251:2007':
            # TODO still to be tested
            # set first day running means
            for i in range(1*24,2*24):
                df_off.loc[i,"T_rm"] = (1-alpha)*df_off.iloc[i-1*24:i,\
                                        col_index].mean()

            for i in range(2*24+1,df_off.shape[0]):
                df_off.loc[i,"T_rm"] = (1-alpha)*(df_off.iloc[i-1*24:i,\
                                                  col_index].mean()) +\
                                                  alpha*df_off.iloc[i-1*24,-1]

        # Compute distances from comfort lines
        df_middle = df_off.where((df_off["T_rm"] > 10) & (df_off["T_rm"] < 33)).dropna()
        df_up =  df_off.where(df_off["T_rm"] > 33).dropna()

        df_middle["dist"] = df_middle["T_op_i[C]"] -\
                            (0.33*df_middle["T_rm"]+18.8)
        df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)

        df =  df_middle.append(df_up)

        # Compute fictitious cooling
        conditions = [df['dist'] < 2,
                    (df["dist"] >= 2) & (df['dist']<= 3),
                    df["dist"] > 3]
        choices = [0,df["Q_c[Wh/m2]"]*(df["dist"]-2),df["Q_c[Wh/m2]"]]

        df['fictitious_cooling'] = np.select(conditions, choices, default=0)

        # Insert here desired graph
        if self.graph:
            # # Graph referred to chosen example week
            # weeks = [x.isocalendar()[1] for x in df["Date/Time"]]
            # df["week"] = weeks
            # #first week of July
            # df_week = df.where(df["week"] == 27).dropna()

            # # Indoor operative temperature versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_week["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_week["Date/Time"]) if x.hour == 1]
            # plt.plot(df_week["Date/Time"], df_week["T_op_i[C]"],ls='',\
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$\theta_{\mathrm{op}}\,\left[°C\right]$')
            # plt.title("Indoor operative temperature over time")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_top.png",
            #             transparent=False)
            # plt.close(fig)

            # # Fictitious cooling versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_week["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_week["Date/Time"]) if x.hour == 1]
            # plt.plot(df_week["Date/Time"], df_week["fictitious_cooling"],\
            #          ls='', marker='.', color='indigo', alpha=0.8)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$Q\,\left[W/m^2\right]$')
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.title("Fictitious cooling over time")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_Q.png",
            #             transparent=False)
            # plt.close(fig)

            # # Full cooling needs versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_week["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_week["Date/Time"]) if x.hour == 1]
            # plt.plot(df_week["Date/Time"], df_week["Q_c[Wh/m2]"],ls='',\
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$Q\,\left[W/m^2\right]$')
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.title("Full cooling consumption over time")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_full_Q.png",
            #             transparent=False)
            # plt.close(fig)

            # # Histograms of fictitious and full consumptions
            # fig = plt.figure(constrained_layout=True)
            # colors = ["tab:orange", "tab:blue"]
            # datas = [df_week["Q_c[Wh/m2]"], df_week["fictitious_cooling"]]
            # for i, d in enumerate(datas):
            #     plt.hist(d, alpha=0.5, color=colors[i], histtype="bar")
            # plt.legend(["Full", "Fictitious"])
            # plt.ylabel("Occurrencies")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_hist.png",
            #             transparent=False)
            # plt.close(fig)

            # Fictitious cooling cdf
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(df["fictitious_cooling"])
            y = np.arange(len(df["fictitious_cooling"]))/float(len(df["fictitious_cooling"]))
            ax.plot(x, y, linewidth = 4)
            ax.set_xlabel("kWh/m2")
            plt.title("CDF fictitious cooling")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_fict_cool.png")
            plt.close(fig)

        return (df["fictitious_cooling"].sum())
        
    def adaptive_residuals(self, dataframe_off, 
                           eu_norm='16798-1:2019', alpha=0.8):
        """Return average distance from Adaptive Comfort Model upper categories
        thresholds.

        :param dataframe_off: Dataframe containing at least 'Date/Time',
                'T_db_e[C]' and 'T_op_i[C]' columns.
        :type dataframe_off: class:`pandas.core.frame.DataFrame`
        :param eu_norm: EU normative to compute Adaptive Comfort Model
            thresholds. It can be '16798-1:2019' or '15251:2007', defaults to
            '16798-1:2019'
        :type eu_norm: str, optional
        :param alpha: EU '15251:2007' free parameter ranging [0,1), defaults to
            0.8
        :type alpha: float, optional
        :return: Dictionary where '>3': average distance from cat 1 upper
            bound; '>0': average distance from central line of cat 1
        :rtype: dict
        """      
        df_off = copy.deepcopy(dataframe_off)

        df_off = df_off.set_index("Date/Time")
        df_off = df_off.resample("1H").mean()

        df_off = df_off.reset_index()
        col_index = df_off.columns.get_loc("T_db_e[C]")

        # Adaptive Comfort Model formula
        if eu_norm == '16798-1:2019':
            for i in range(7*24,df_off.shape[0]):
                df_off.loc[i,"T_rm"] = (df_off.iloc[i-1*24:i,col_index].mean() +\
                                            0.8*df_off.iloc[i-2*24:i-1*24,\
                                                col_index].mean() +\
                                            0.6*df_off.iloc[i-3*24:i-2*24,\
                                                col_index].mean() +\
                                            0.5*df_off.iloc[i-4*24:i-3*24,\
                                                col_index].mean() +\
                                            0.4*df_off.iloc[i-5*24:i-4*24,\
                                                col_index].mean() +\
                                            0.3*df_off.iloc[i-6*24:i-5*24,\
                                                col_index].mean() +\
                                            0.2*df_off.iloc[i-7*24:i-6*24,\
                                                col_index].mean())/3.8

        elif eu_norm == '15251:2007':
            # TODO still to be tested
            # set first day running means
            for i in range(1*24,2*24):
                df_off.loc[i,"T_rm"] = (1-alpha)*df_off.iloc[i-1*24:i,\
                                                             col_index].mean()

            for i in range(2*24+1,df_off.shape[0]):
                df_off.loc[i,"T_rm"] = (1-alpha)*(df_off.iloc[i-1*24:i,\
                                                  col_index].mean()) +\
                                                  alpha*df_off.iloc[i-1*24,-1]

        df_middle = df_off.where((df_off["T_rm"] >= 10) & (df_off["T_rm"] <= 33)).dropna()
        df_up =  df_off.where(df_off["T_rm"] > 33).dropna()

        # Compute distances from comfort line and comfort+3 line
        df_middle["dist"] = df_middle["T_op_i[C]"]-\
                            (0.33*df_middle["T_rm"]+18.8)
        df_up["dist"] = df_up["T_op_i[C]"] - (0.33*33+18.8)
        df_middle["dist_2"] = df_middle["T_op_i[C]"] -\
                             (0.33*df_middle["T_rm"]+18.8+3)
        df_up["dist_2"] = df_up["T_op_i[C]"] - (0.33*33+18.8+3)

        df =  df_middle.append(df_up)

        plt.rcParams.update({"font.size": 45})
        fig, ax = plt.subplots(figsize=(50, 25))
        x = df["Date/Time"]
        y = df["dist"]
        ax.plot(x, y, linewidth = 4)
        ax.set_xlabel("Datetime")
        plt.title("Distance from middle line")
        plt.grid()
        fig.autofmt_xdate()
        plt.savefig(self.plot_dir / "adaptive_res.png")
        plt.close(fig)

        # to get all results
        # result_0 = list(df["dist"])
        # result_2 = list(df["dist_2"])
        result_0 = round(df["dist"].where(df["dist"]>0).sum(), 2)
        result_2 = round(df["dist_2"].where(df["dist_2"]>0).sum(), 2)
        # return average hourly distance from lines
        return {">0": result_0/len(df), ">3": result_2/len(df)}

    def pmv_ppd(self, df, vel=0.1, met=1.2, clo=0.7, wme=0,
                standard="ISO 7730-2006", filter_by_occupancy = 0):
        """Return Predicted Mean Vote (PMV) and Predicted Percentage of
        Dissatisfied (PPD) calculated in accordance to ISO 7730-2006 standard.

        :param df: dataframe containing at least "Date/Time",
            "T_db_i[C]", "T_rad_i[C]" and "RH_i[%]" columns.
            Optional "Occupancy column" accepting only 0 and 1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param vel: relative air speed, defaults 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] defaults 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] defaults 0.5
        :type clo: float, optional
        :param wme: external work, [met] defaults 0
        :type wme: float, optional
        :param standard: default "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or not,
            default 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "pmv" (Predicted Mean Vote) and
            "ppd" (Predicted Percentage of Dissatisfied occupants)
        :rtype: dict
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        rh = np.array(list(df["RH_i[%]"]))
        ta = np.array(list(df["T_db_i[C]"]))
        tr = np.array(list(df["T_rad_i[C]"]))
        pmv = []
        ppd = []

        fnps = np.exp(16.6536 - 4030.183 / (ta + 235)) # water vapor pressure in ambient air kPa
        pa = rh * 10 * fnps # partial water vapor pressure in ambient air 	
        icl = 0.155 * clo  # thermal insulation of the clothing in M2K/W
        m = met * 58.15  # metabolic rate in W/M2
        w = wme * 58.15  # external work in W/M2
        mw = m - w  # internal heat production in the human body

        # ratio of clothed body surface over total body surface
        if icl <= 0.078:
            fcl = 1 + (1.29 * icl)  
        else:
            fcl = 1.05 + (0.645 * icl)

        # heat transfer coefficient by forced convection
        hcf = 12.1 * np.sqrt(vel)

        # temperatures in Kelvin
        taa = ta + 273 
        tra = tr + 273 

        # iterative computation of clothing surface temperature
        tcla = taa + (35.5 - ta) / (3.5*icl + 0.1) # first tempt

        p1 = icl * fcl
        p2 = p1 * 3.96
        p3 = p1 * 100
        p4 = p1 * taa
        p5 = (308.7 - 0.028*mw) + (p2*(tra/100.0)**4)
        xn = tcla / 100
        xf = tcla / 50
        # end criterion
        eps = 0.00015
        for i in range (0,len(df)):
            n = 0 # number of iterations
            while abs(xn[i] - xf[i]) > eps:
                xf[i] = (xf[i] + xn[i]) / 2
                # heat transfer coefficient for natural convection
                hcn = 2.38 * abs(100.0*xf[i] - taa[i])**0.25
                if hcf > hcn:
                    hc = hcf
                else:
                    hc = hcn
                xn[i] = (p5[i] + p4[i]*hc - p2*xf[i]**4) / (100 + p3*hc)
                n += 1
                if n > 150:
                    pmv.append(np.inf)
                    ppd.append(100)

            # clothing surface temperature      
            tcl = 100*xn[i] - 273

            # heat loss diff. through skin
            hl1 = 3.05*0.001*(5733 - (6.99*mw) - pa[i])
            # heat loss by sweating
            if mw > 58.15:
                hl2 = 0.42 * (mw - 58.15)
            else:
                hl2 = 0
            # latent respiration heat loss
            hl3 = 1.7 * 0.00001 * m * (5867 - pa[i])
            # dry respiration heat loss
            hl4 = 0.0014 * m * (34 - ta[i])
            # heat loss by radiation
            hl5 = 3.96 * fcl * (xn[i] ** 4 - (tra[i] / 100.0) ** 4)
            # heat loss by convection
            hl6 = fcl * hc * (tcl - ta[i])
            # conversion coefficient of thermal sensation
            ts = 0.303 * np.exp(-0.036 * m) + 0.028

            # final formulas
            pmv.append(round(ts * (mw - hl1 - hl2 - hl3 - hl4 - hl5 - hl6),1))
            ppd.append(int(100.0 - 95.0 * np.exp(-0.03353 * pow(pmv[-1],\
                                                 4.0) - 0.2179 * pow(pmv[-1],\
                                                                     2.0))))

        if self.graph:
            pass
            # # Graph referred to chosen example month
            # #May
            # df["Date/Time"] = df.index
            # # assert False
            # df_month = df[pd.DatetimeIndex(df["Date/Time"]).month==6]

            # # Indoor operative temperature versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # plt.plot(df_month["Date/Time"], df_month["T_db_i[C]"],ls='-', lw=0.5,
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$\theta_{\mathrm{op}}\,\left[°C\right]$')
            # plt.title("Indoor drybulb temperature over time")
            # plt.savefig(self.plot_dir / "KPI_db_temperature.png",
            #             transparent=False)
            # plt.close(fig)

        # to return all list of all values
        my_dict = {"pmv": pmv, "ppd": ppd}
        # return my_dict

        # NB without ABS it takes both hot and cold: 20% dissatisfied with PMV < -07 and > 07
        # to return POR or other kinds of aggregate results
        temp_df = pd.DataFrame(my_dict,index=df.index)
        if filter_by_occupancy:
            temp_df = temp_df.where(df["Occupancy"] != 0).dropna()
        # POR
        por = (temp_df["ppd"].where(abs(temp_df["pmv"])>0.7).count())/len(temp_df)
        # Number of hours of discomfort
        n_h = (temp_df["ppd"].where(abs(temp_df["pmv"])>0.7).count())
        my_dict = {"POR": por, "No_h_discomfort": n_h}
        return my_dict

    def adaptive_comfort_model(self, df, eu_norm='16798-1:2019', alpha=0.8,
                                filter_by_occupancy=0,when={}):
        """Compute adaptive comfort model in a standardized format.

        :param df: dataframe should contain "Date/Time" column in format
            'year/month/day hour:minutes:seconds', "T_db_e[C]" preferably with a
            subhourly timestep and "T_op_i[C]". Optional "Occupancy" column
            accepting only 0/1 values.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or not,
            default 0.
        :type filter_by_occupancy: int, optional
        :param when: dictionary with 'start' and 'end' keys and values in format 'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Number of hours in each of the 7 comfort
            categories and POR computed as % of hours outside cat 2 boundaries.
        :rtype: dict
        """

        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        df = df.reset_index()   
        col_index = df.columns.get_loc("T_db_e[C]")

        if eu_norm == '16798-1:2019':
            for i in range(7*24,df.shape[0]):
                df.loc[i,"T_rm"] = (df.iloc[i-1*24:i,col_index].mean() +\
                                            0.8*df.iloc[i-2*24:i-1*24,\
                                                col_index].mean() +\
                                            0.6*df.iloc[i-3*24:i-2*24,\
                                                col_index].mean() +\
                                            0.5*df.iloc[i-4*24:i-3*24,\
                                                col_index].mean() +\
                                            0.4*df.iloc[i-5*24:i-4*24,\
                                                col_index].mean() +\
                                            0.3*df.iloc[i-6*24:i-5*24,\
                                                col_index].mean() +\
                                            0.2*df.iloc[i-7*24:i-6*24,\
                                                col_index].mean())/3.8

        if eu_norm == '15251:2007':
            # TODO still to be tested (verificare che funzioni e che abbia anche senso)
            # set first day running means
            for i in range(1*24,2*24):
                df.loc[i,"T_rm"] = (1-alpha)*df.iloc[i-1*24:i,col_index].mean()

            for i in range(2*24+1,df.shape[0]):
                df.loc[i,"T_rm"] = (1-alpha)*(df.iloc[i-1*24:i,\
                                              col_index].mean()) +\
                                              alpha*df.iloc[i-1*24,-1]

        if filter_by_occupancy:
            df = df.where(df["Occupancy"] != 0).dropna()

        if "start" in when.keys() and "end" in when.keys():
            df = df.set_index("Date/Time")
            start = when["start"]
            end=when["end"]
            start = dt.datetime.strptime(start, "%Y/%m/%d %H:%M:%S")
            end = dt.datetime.strptime(end, "%Y/%m/%d %H:%M:%S")
            start_idx = df.index.get_loc(start, method='nearest')
            end_idx = df.index.get_loc(end, method='nearest')
            df = df.iloc[start_idx:end_idx+1, :]

        df_down = df.where(df["T_rm"] < 10).dropna()
        df_middle = df.where((df["T_rm"] >= 10) & (df["T_rm"] <= 33)).dropna()
        df_up =  df.where(df["T_rm"] > 33).dropna()

        category_1 = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-3)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+2))).sum() +\
                    ((df_down["T_op_i[C]"] >=(0.33*10+18.8-3)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+2))).sum() +\
                    ((df_up["T_op_i[C]"] >=(0.33*33+18.8-3)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+2))).sum()

        category_2_up = ((df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+2)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+3))).sum() +\
                    ((df_down["T_op_i[C]"] >(0.33*10+18.8+2)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+3))).sum() +\
                    ((df_up["T_op_i[C]"] >(0.33*33+18.8+2)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+3))).sum()

        category_3_up = ((df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+3)) &
                    (df_middle["T_op_i[C]"] <= (0.33*df_middle["T_rm"]+18.8+4))).sum() +\
                    ((df_down["T_op_i[C]"] >(0.33*10+18.8+3)) &
                    (df_down["T_op_i[C]"] <= (0.33*10+18.8+4))).sum() +\
                    ((df_up["T_op_i[C]"] >(0.33*33+18.8+3)) &
                    (df_up["T_op_i[C]"] <= (0.33*33+18.8+4))).sum()

        category_over_3 = (df_middle["T_op_i[C]"] >(0.33*df_middle["T_rm"]+18.8+4)).sum() +\
                        (df_down["T_op_i[C]"] > (0.33*10+18.8+4)).sum() +\
                        (df_up["T_op_i[C]"] > (0.33*33+18.8+4)).sum()

        category_2_down = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-4)) &
                          (df_middle["T_op_i[C]"] < (0.33*df_middle["T_rm"]+18.8-3))).sum() +\
                          ((df_down["T_op_i[C]"] >=(0.33*10+18.8-4)) &
                          (df_down["T_op_i[C]"] < (0.33*10+18.8-3))).sum() +\
                          ((df_up["T_op_i[C]"] >=(0.33*33+18.8-4)) &
                          (df_up["T_op_i[C]"] < (0.33*33+18.8-3))).sum()

        category_3_down = ((df_middle["T_op_i[C]"] >=(0.33*df_middle["T_rm"]+18.8-5)) &
                    (df_middle["T_op_i[C]"] < (0.33*df_middle["T_rm"]+18.8-4))).sum() +\
                    ((df_down["T_op_i[C]"] >=(0.33*10+18.8-5)) &
                    (df_down["T_op_i[C]"] < (0.33*10+18.8-4))).sum() +\
                    ((df_up["T_op_i[C]"] >=(0.33*33+18.8-5)) &
                    (df_up["T_op_i[C]"] < (0.33*33+18.8-4))).sum()

        category_under_3 = (df_middle["T_op_i[C]"] <(0.33*df_middle["T_rm"]+18.8-5)).sum()+\
                    (df_down["T_op_i[C]"] <(0.33*10+18.8-5)).sum()+\
                    (df_up["T_op_i[C]"] < (0.33*33+18.8-5)).sum()

        if self.graph:
            plt.rcParams.update({"font.size": 45})
            # Graph referred to chosen example month
            #May          
            # df_month = df[pd.DatetimeIndex(df["Date/Time"]).month==6]

            # Indoor operative temperature versus time
            # fig = plt.figure(constrained_layout=True)
            # plt.grid()
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.hour == 1]
            # x_ticks = [x for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # x_labels = [str(x.day) + "/" + str(x.month) for x in list(df_month["Date/Time"]) if x.isocalendar()[2] == 1 and x.hour == 1]
            # plt.plot(df_month["Date/Time"], df_month["T_op_i[C]"],ls='-', lw=0.5,
            #          marker='.', color='indigo', alpha=0.8)
            # plt.xticks(ticks=x_ticks, labels = x_labels)
            # plt.xlabel(r'$time$')
            # plt.ylabel(r'$\theta_{\mathrm{op}}\,\left[°C\right]$')
            # plt.title("Indoor operative temperature over time")
            # plt.savefig(self.plot_dir / "KPI_fictitious_cooling_top.png",
            #             transparent=False)
            # plt.close(fig)

            fig = plt.figure(figsize=(18, 15), constrained_layout=True)
            plt.grid()
            X = np.linspace(-10,40)
            Y_comfort =  [x*0.33+18.8 if 10<=x<=33 else 10*0.33+18.8 if x<10 else 33*0.33+18.8 for x in X]
            Y_cat1_up = [x*0.33+18.8+2 if 10<=x<=33 else 10*0.33+18.8+2 if x<10 else 33*0.33+18.8+2 for x in X]
            Y_cat1_down = [x*0.33+18.8-3 if 10<=x<=33 else 10*0.33+18.8-3 if x<10 else 33*0.33+18.8-3 for x in X]
            Y_cat2_up = [x*0.33+18.8+3 if 10<=x<=33 else 10*0.33+18.8+3 if x<10 else 33*0.33+18.8+3 for x in X]
            Y_cat2_down = [x*0.33+18.8-4 if 10<=x<=33 else 10*0.33+18.8-4 if x<10 else 33*0.33+18.8-4 for x in X]
            Y_cat3_up = [x*0.33+18.8+4 if 10<=x<=33 else 10*0.33+18.8+4 if x<10 else 33*0.33+18.8+4 for x in X]
            Y_cat3_down = [x*0.33+18.8-5 if 10<=x<=33 else 10*0.33+18.8-5 if x<10 else 33*0.33+18.8-5 for x in X]
            plt.plot(df["T_rm"], df["T_op_i[C]"],ls='', marker='o', color='indigo', alpha=0.8)
            plt.plot(X,Y_comfort,color='r', lw=4)
            plt.plot(X,Y_cat1_up,color='r',linestyle='--', lw=4)
            plt.plot(X,Y_cat1_down,color='r',linestyle='--', lw=4)
            plt.plot(X,Y_cat2_up,color='r',linestyle='-.', lw=4)
            plt.plot(X,Y_cat2_down,color='r',linestyle='-.', lw=4)
            plt.plot(X,Y_cat3_up,color='r',linestyle=':', lw=4)
            plt.plot(X,Y_cat3_down,color='r',linestyle=':', lw=4)
            plt.xlabel(r'$\theta_{\mathrm{rm}}$ [°C]')
            plt.ylabel(r'$\theta_{\mathrm{op}}$ [°C]')
            plt.savefig(self.plot_dir / "KPI_adaptive_comfort_model.png",
                        transparent=False)
            plt.close(fig)
 
            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(df["T_op_i[C]"])
            y = np.arange(len(df["T_op_i[C]"]))/float(len(df["T_op_i[C]"]))
            ax.plot(x, y, linewidth = 4)
            ax.set_xlabel('°C')
            plt.title("CDF indoor operative temperature")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_t_op_i.png")
            plt.close(fig)

        # return No of hours in categories, add /len(df) to get fraction
        return {"cat I": category_1,
                "cat II up": category_2_up,
                "cat III up": category_3_up,
                "cat over III": category_over_3,
                "cat II down": category_2_down,
                "cat III down": category_3_down,
                "cat under III": category_under_3,
                "POR": (category_3_up+category_over_3+category_3_down+category_under_3)/len(df)}

    def energy_signature(self, df):
        """Compute energy signature in a standardized format: dataframe used
        should contain "Date/Time", "T_db_e[C]", "T_db_i[C]",
        "Rad_global_e[W/m2]" and HVAC consumptions columns, "Q_c[Wh/m3]" and
        "Q_h[Wh/m3]".

        :param dataframe: dataframe containing at least "Date/Time",
            "T_db_e[C]", "T_db_i[C]", "Rad_global_e[W/m2]","Q_h[Wh/m3]",
            "Q_c[Wh/m3]" columns
        :type dataframe: class:`pandas.core.frame.DataFrame`
        :return: Minimum points allowing to build 1D and 2D graphs of energy
            signature.
        :rtype: dict

        :Example:

        Returned dictionary structure:

        .. code-block:: python

            {
                "1D": {
                    "cooling": {
                        "deltaT": list of 2 points,
                        "cooling": list of 2 points,
                    },
                    "heating": {
                        "deltaT": list of 2 points,
                        "heating": list of 2 points,
                    },
                },
                "2D": {
                    "cooling": {
                        "deltaT": list of 4 points,
                        "solarRadiation": list of 4 points,
                        "cooling": list of 4 points,
                    },
                    "heating": {
                        "deltaT": list of 4 points,
                        "solarRadiation": list of 4 points,
                        "heating": list of 4 points,
                    },
                },
            }
        """

        df["deltaT"] = df["T_db_i[C]"]-df["T_db_e[C]"]
        df["Rad_global_e[W/m2]"][df["Rad_global_e[W/m2]"] == 0] = np.nan
        df = df.resample("1W").mean()  

        # Remove DHW contribution
        # TODO ancora da gestire nel caso non ci sia/null boh --> varie comformazioni acqua calda
        dhw_points = df.where((df["Q_c[Wh/m3]"]!=0.0) & 
                            (df["Q_h[Wh/m3]"]!=0.0)).dropna()       
        dhw_mean_value = dhw_points.mean(axis=0)["Q_h[Wh/m3]"]
        df["Q_h[Wh/m3]"] = df["Q_h[Wh/m3]"]-dhw_mean_value
        data_cool = df.where(df["Q_c[Wh/m3]"]!=0.0).dropna()
        data_heat = df.where(df["Q_h[Wh/m3]"]!=0).dropna()

        # 1D energy signature ext temp
        model_cool_1D = sm.OLS(data_cool["Q_c[Wh/m3]"],
                            sm.add_constant(data_cool.deltaT))
        model_heat_1D = sm.OLS(data_heat["Q_h[Wh/m3]"],
                            sm.add_constant(data_heat.deltaT))

        res_cool_1D = model_cool_1D.fit()
        res_heat_1D = model_heat_1D.fit()

        X_cool = np.linspace(data_cool.deltaT.min(), data_cool.deltaT.max(), 2)
        Y_cool = res_cool_1D.params[0] + res_cool_1D.params[1] * X_cool

        X_heat = np.linspace(data_heat.deltaT.min(), data_heat.deltaT.max(), 2)
        Y_heat = res_heat_1D.params[0] + res_heat_1D.params[1] * X_heat 

        # 1D energy signature solar
        model_cool_1D_solar = sm.OLS(data_cool["Q_c[Wh/m3]"],
                            sm.add_constant(data_cool["Rad_global_e[W/m2]"]))
        model_heat_1D_solar = sm.OLS(data_heat["Q_h[Wh/m3]"],
                            sm.add_constant(data_heat["Rad_global_e[W/m2]"]))

        res_cool_1D_solar = model_cool_1D_solar.fit()
        res_heat_1D_solar = model_heat_1D_solar.fit()

        X_cool_solar = np.linspace(data_cool["Rad_global_e[W/m2]"].min(), data_cool["Rad_global_e[W/m2]"].max(), 2)
        Y_cool_solar = res_cool_1D_solar.params[0] + res_cool_1D_solar.params[1] * X_cool_solar

        X_heat_solar = np.linspace(data_heat["Rad_global_e[W/m2]"].min(), data_heat["Rad_global_e[W/m2]"].max(), 2)
        Y_heat_solar = res_heat_1D_solar.params[0] + res_heat_1D_solar.params[1] * X_heat_solar   

        # 2D energy signature
        model_cool_2D = sm.OLS(data_cool["Q_c[Wh/m3]"],
                            sm.add_constant(data_cool.loc[:,["deltaT",\
                                                            "Rad_global_e[W/m2]"]]))
        model_heat_2D = sm.OLS(data_heat["Q_h[Wh/m3]"],
                            sm.add_constant(data_heat.loc[:,["deltaT",\
                                                            "Rad_global_e[W/m2]"]]))

        res_cool_2D = model_cool_2D.fit()
        res_heat_2D = model_heat_2D.fit()

        xx1c, xx2c = np.meshgrid(np.linspace(data_cool.deltaT.min(),\
                                             data_cool.deltaT.max(),2), 
                                np.linspace(data_cool["Rad_global_e[W/m2]"].min(),\
                                            data_cool["Rad_global_e[W/m2]"].max(), 2))
        Z_cool = res_cool_2D.params[0] + res_cool_2D.params[1] * xx1c +\
                 res_cool_2D.params[2] * xx2c

        xx1h, xx2h = np.meshgrid(np.linspace(data_heat.deltaT.min(),\
                                             data_heat.deltaT.max(), 2), 
                                np.linspace(data_heat["Rad_global_e[W/m2]"].min(),\
                                            data_heat["Rad_global_e[W/m2]"].max(), 2))
        Z_heat = res_heat_2D.params[0] + res_heat_2D.params[1] * xx1h +\
                 res_heat_2D.params[2] * xx2h

        if self.graph:
            plt.rcParams.update({"font.size": 45})

            #%% energy signature 1d ext temp
            fig = plt.figure(figsize=(18, 15))#, constrained_layout=True)
            plt.plot(list(X_cool), list(Y_cool),'tab:cyan', lw=5, label='Cooling')
            plt.plot(list(X_heat),list(Y_heat),'tab:orange', lw=5, label='Heating')
            plt.title('Energy Signature 1D\nWeekly Data Aggregation')
            plt.xlabel(r'$T_{\mathrm{in}} - T_{\mathrm{ex}}$ [°C]')
            plt.grid()
            plt.ylabel(r'Energy Need $\left[\mathrm{W/m^3}\right]$')
            plt.legend()
            plt.savefig(self.plot_dir / "en_sig_1d_temp", fmt = ".png")
            plt.close(fig)

            #%% energy signature 1d solar
            fig = plt.figure(figsize=(18, 15))#, constrained_layout=True)
            plt.plot(list(X_cool_solar), list(Y_cool_solar),'tab:cyan', lw=5, label='Cooling')
            plt.plot(list(X_heat_solar),list(Y_heat_solar),'tab:orange', lw=5, label='Heating')
            plt.title('Energy Signature 1D\nWeekly Data Aggregation')
            plt.xlabel(r'Global solar radiation ' + r'$\left[\mathrm{Wh/m^2}\right]$')
            plt.grid()
            plt.ylabel(r'Energy Need $\left[\mathrm{W/m^3}\right]$')
            plt.legend()
            plt.savefig(self.plot_dir / "en_sig_1d_solar", fmt = ".png")
            plt.close(fig)

            #%% energy signature 2d
            fig = plt.figure(figsize=(18, 15))
            ax = fig.add_subplot(111, projection='3d')
                
            ax.plot_surface(np.array(list(xx1c[0]) + list(xx1c[1])).reshape(2,2),\
                            np.array(list(xx2c[0]) + list(xx2c[1])).reshape(2,2),\
                            np.array(list(Z_cool[0]) + list(Z_cool[1])).reshape(2,2), color='b', alpha=0.3, linewidth=0)
            ax.plot_surface(np.array(list(xx1h[0]) +  list(xx1h[1])).reshape(2,2),\
                            np.array(list(xx2h[0]) +  list(xx2h[1])).reshape(2,2),\
                            np.array(list(Z_heat[0]) + list(Z_heat[1])).reshape(2,2), color='orange', alpha=0.3, linewidth=0)
            ax.set_xlabel(r'$T_{\mathrm{in}} - T_{\mathrm{ex}}$ [°C]', labelpad=40)
            ax.set_ylabel('Global solar\nradiation   \n' + r'$\left[\mathrm{Wh/m^2}\right]$', labelpad=80)
            ax.set_zlabel('Energy Need\n' + r'$\left[\mathrm{W/m^3}\right]$', labelpad=40)
            ax.set_title('Energy Signature 2D\nWeekly Data Aggregation')
            ax.view_init(elev=34, azim=-35)
            # # rotate the axes and update
            ax.view_init(45, 55)
            plt.draw()

            surf_cool = mpatches.Patch(color='blue',label='Surface Cooling')
            surf_heat = mpatches.Patch(color='orange',label='Surface Heating')
            ax.legend(handles=[surf_cool,surf_heat], bbox_to_anchor=(0.5, 0.4, 0.5, 0.5))
            plt.savefig(self.plot_dir / "en_sig_2d", fmt="png")
            plt.close(fig)

        data = {
            "1D": {
                "cooling": {
                    "deltaT": list(X_cool),
                    "cooling": list(Y_cool),
                },
                "heating": {
                    "deltaT": list(X_heat),
                    "heating": list(Y_heat),
                },
            },
            "2D": {
                "cooling": {
                    "deltaT": list(xx1c[0]) + list(xx1c[1]),
                    "solarRadiation": list(xx2c[0]) + list(xx2c[1]),
                    "cooling": list(Z_cool[0]) + list(Z_cool[1]),
                },
                "heating": {
                    "deltaT": list(xx1h[0]) +  list(xx1h[1]),
                    "solarRadiation": list(xx2h[0]) + list(xx2h[1]),
                    "heating": list(Z_heat[0]) + list(Z_heat[1]),
                },
            },
        }
        return data

    def cidh(self, df, thresholds=[18, 20, 22, 24, 26, 28]):
        """Compute standardized CIDH (Cooling Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param df: dataframe containing at least "Date/Time" and "T_db_i[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 20, 22, 24, 26, 28]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()

        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = df["T_db_i[C]"]-th
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def cdh(self, df, thresholds=[18, 21, 24, 26, 28]):
        """Compute Cooling Degree Hours

        :param df: DataFrame containing at least "Date/Time" and "T_db_e[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param thresholds: temperature thresholds from which compute residuals,
            defaults to [18, 21, 24, 26, 28]
        :type thresholds: list, optional
        :return: Cooling degree hour
        :rtype: dict
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        
        my_dict = {}
        for th in thresholds:
            df["dist_"+str(th)] = df["T_db_e[C]"]-th
            sum_dist = df["dist_"+str(th)].where(df["dist_"+str(th)]>0).sum()
            my_dict["dist_"+str(th)] = sum_dist

        return my_dict

    def cdd(self, df):
        """Compute Cooling Degree Days.

        :param df: DataFrame containing at least "Date/Time" and "T_db_e[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :return: Cooling degree day
        :rtype: float
        """
        
        df = df.set_index("Date/Time")
        df = df.resample("1D").mean()
        # to return dict instead of float
        # my_dict = {}
        df["cdd"] = df["T_db_e[C]"]-21
        sum_dist = df["cdd"].where((df["T_db_e[C]"]-24)>=0).sum()
        # my_dict["cdd"] = sum_dist

        return sum_dist

    def cdd_res(self, df, tset=26, ef=0.5):
        """Compute Cooling Degree Days residuals.

        :param df: DataFrame containing at least "Date/Time" and "T_db_e[C]"
            columns
        :param tset: Setpoint temperature, defaults 26
        :type tset: int, optional
        :param ef: free parameter, defaults to 0.5
        :type ef: float, optional
        :return: Cooling degree days residuals
        :rtype: int
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        df = df.reset_index()
        cnt = 0
        sum_dist = 0
        
        while cnt + 24 < len(df):
            dist = 0
            for i in range(cnt, cnt + 24):
                delta_t = df["T_db_e[C]"][i] - tset
                if delta_t >= 0:
                    dist += delta_t
                else: 
                    dist += delta_t*ef
            if dist > 0:
                sum_dist += dist
            cnt += 24
        
        return sum_dist

    def ccp(self, df, delta_T=2):
        """Compute Climate Cooling Potential

        :param df: DataFrame containing at least "Date/Time" and "T_db_e[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param delta_T: Delta temperature, defaults to 2
        :type delta_T: int, optional
        :return: Climate Cooling Potential and daily Climate Cooling Potential
            in a dictionary
        :rtype: dict
        """
        hours = np.arange(0,24,1)
        t_base = 24.5+2.5*np.cos(2*np.pi*(hours-19)/24)
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        dist = [t_base[df.index[i].hour] - df["T_db_e[C]"][i] for i in range(0, len(df))]
        df["dist"] = dist
        my_dict = {}
        ccp = df["dist"].where(df["dist"]>=delta_T).sum()
        ccpd = ccp/int(len(df)/24)
        my_dict["ccp"] = ccp
        my_dict["ccpd"] = ccpd
        return my_dict 

    def qach(self, df, vol, ach=2.5, pair=1.2, cair=0.278, tset=26, delta_T=2):
        """Compute climate heat gain dissipation potential of
        ventilative cooling, Qach. 

        :param df: DataFrame containing at least "Date/Time" and "T_db_e[C]"
            columns
        :type df: class:`pandas.core.frame.DataFrame`
        :param vol: Volume of the building
        :type vol: float
        :param ach: Air changes per hour (ventilation), defaults to 2.5
        :type ach: float, optional
        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param tset: Setpoint temperature in °C, defaults to 26
        :type tset: int, optional
        :param delta_T: Delta temperature, defaults to 2 °C
        :type delta_T: int, optional
        :return: Qach
        :rtype: float
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        # my_dict={}
        df["dist"] = tset - df["T_db_e[C]"]
        sum_dist = df["dist"].where(df["dist"]>=delta_T).sum()
        qach = sum_dist*ach*pair*cair*vol/1000
        # my_dict["qach"] = qach
        
        return qach

    def n_h_kwh(self, df, th_list = [0,0.6,1], cop_cool = 3, cop_heat = 0.3):
        """Number of hours HVAC consumption is higher then given thresholds.

        :param df: Dataframe which must contain at least cooling and heating 
                consumption columns "Q_c[Wh/m2]" and "Q_h[Wh/m2]" plus
                "Date/Time" column.
        :type df: class:`pandas.core.frame.DataFrame`
        :param th_list: List of thresholds to be compared with consumption,
            defaults to [0,0.6,1]
        :type th_list: list, optional
        :param cop_cool: Cop value for cooling system, defaults to 3
        :type cop_cool: int, optional
        :param cop_heat: Cop value for heating system, defaults to 0.3
        :type cop_heat: float, optional
        :return: Dictionary containing No. hours above defined thresholds
                for heating and cooling.
        :rtype: dict
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").sum()
        
        my_dict = {}
        for t in th_list:
            my_dict[str(t) + "_cool"] = df["Q_c[Wh/m2]"].where((df["Q_c[Wh/m2]"])>t*cop_cool).count()
            my_dict[str(t) + "_heat"] = df["Q_h[Wh/m2]"].where((df["Q_h[Wh/m2]"])>t*cop_heat).count()
        
        return my_dict

    def co2(self,df,th_list=[600,900,1200,1500,1800,2100,2500]):
        """Compute number of hours with CO2 concentration above
        given thresholds and CO2 simple statistics.

        :param df: Dataframe which must contain at least CO2 concentration 
                outdoor and indoor columns "CO2_e[ppm]" and "CO2_i[ppm]" plus
                "Date/Time" column.
        :type df: class:`pandas.core.frame.DataFrame`
        :param th_list: thresholds from which compute number of hours with average CO2 above 
        :type th_list: list, optional
        :return: dictionary containing CO2 levels statistics
        :rtype: dict
        """
        df = df.set_index("Date/Time")
        df = df.resample("1H").mean()
        my_dict = {}
        for th in th_list:
            my_dict[str(th)] = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])>th).count()
        
        my_dict["max"] = df["CO2_i[ppm]"].max()
        my_dict["min"] = df["CO2_i[ppm]"].min()
        my_dict["mean"] = df["CO2_i[ppm]"].mean()
        # TODO check if it works
        my_dict["800+out"] = df["CO2_i[ppm]"].where((df["CO2_i[ppm]"])>800+df["CO2_e[ppm]"]).count()
        
        if self.graph:
            pass

        return my_dict

class EnergyPlusKPI(KPI):
    """Class which computes KPIs for EnergyPlus.
    It extends :class:`predyce.kpi.KPI` by preprocessing the eplusout.csv
    generated by EnergyPlus."""

    def __init__(self, df, idf, plot_dir, start_date=None, end_date=None, graph=False):
        """
        :param df: DataFrame of EnergyPlus outputs
        :type df: class:`pandas.core.frame.DataFrame`
        :param idf: IDF object
        :type idf: class:`predyce.IDF_class.IDF`
        :param plot_dir: Directory where to save plots
        :type plot_dir: str or Path
        :param graph: `True` to save plots, defaults to `False`
        :type graph: bool, optional
        """
        super().__init__(plot_dir, graph)
        self.df = df
        self.df.columns = [c.replace(':ON', '') for c in self.df.columns]
        self.building_name = idf.main_block
        self.area = idf.area
        self.volume = idf.volume
        self.idf = idf

        if start_date is not None or end_date is not None:
            dates = self.df["Date/Time"].apply(self.convert_to_datetime)

        if start_date is not None:
            start_date = dt.datetime.strptime(start_date + "-1970", "%d-%m-%Y")
            dates = dates[dates > start_date]

        if end_date is not None:
            end_date = dt.datetime.strptime(end_date + "-1970", "%d-%m-%Y")
            dates = dates[dates < end_date]

        if start_date is not None or end_date is not None:
            # TODO: Choose between "Site" or "Environment"
            variables = [c for c in self.df.columns if "site" in c.lower()]
            var_df = self.df.loc[:, ["Date/Time"] + variables]
            self.df = self.df.iloc[dates.index]
            self.df = pd.merge(self.df, var_df, how="right")


    def cdh(self, thresholds=[18, 21, 24, 26, 28]):
        """Prepare eplosout.csv to standardized cooling degree hours
        computation.

        :param thresholds: hresholds from which compute residuals, defaults to
            [18, 21, 24, 26, 28]
        :type thresholds: list, optional
        :return: Cooling degree hours with respect to each specified threshold
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df = df.loc[:,["Date/Time", "T_db_e[C]"]]
        return super().cdh(df,thresholds=thresholds)

    def n_h_kwh(self, th_list = [0,0.6,1], cop_cool = 3, cop_heat = 0.3):
        """Prepare eplosout.csv to standardized number of hours above
        consumption thresholds computation.

        :param th_list: List of thresholds to be compared with consumption,
            defaults to [0,0.6,1]
        :type th_list: list, optional
        :param cop_cool: Cop value for cooling system, defaults to 3
        :type cop_cool: int, optional
        :param cop_heat: Cop value for heating system, defaults to 0.3
        :type cop_heat: float, optional
        :return: Dictionary containing no. of hours above defined thresholds
                for heating and cooling.
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"})
        df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[Wh/m2]"})
        df["Q_c[Wh/m2]"] = df["Q_c[Wh/m2]"].apply(self.convert_to_kWh) 
        df["Q_h[Wh/m2]"] = df["Q_h[Wh/m2]"].apply(self.convert_to_kWh)                                    
        df = df.loc[:,["Date/Time", "Q_c[Wh/m2]", "Q_h[Wh/m2]"]]
        return super().n_h_kwh(df,th_list = th_list, cop_cool = cop_cool, cop_heat = cop_heat)

    def cdd(self):
        """Prepare eplosout.csv to standardized Cooling Degree Days computation.

        :return: Cooling degree day
        :rtype: float
        """
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df = df.loc[:,["Date/Time", "T_db_e[C]"]]
        return super().cdd(df)
 
    def cdd_res(self, tset=26, ef=0.5):
        """Prepare eplosout.csv to standardized Cooling Degree Days residuals computation.

        :param tset: Cooling system setpoint, defaults to 26
        :type tset: int, optional
        :param ef: Free parameter, defaults to 0.5
        :type ef: float, optional
        :return: Cooling Degree Days residuals
        :rtype: int
        """
        #TODO far si che il cooling setpoint non venga dato come parametro ma preso in automatico dalla schedule
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df = df.loc[:,["Date/Time", "T_db_e[C]"]]
        return super().cdd_res(df,tset=tset,ef=ef)  
        
    def ccp(self):
        """Prepare eplosout.csv to standardized climate cooling potential
        computation.

        :return: Dictionary containing CCP and CCPd
        :rtype: dict
        """
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df = df.loc[:,["Date/Time", "T_db_e[C]"]]
        delta_T = self.idf.mean_dt
        return super().ccp(df,delta_T=delta_T) #

    def qach(self, pair=1.2, cair=0.278, tset=26):
        """Prepare eplosout.csv to standardized climate heat gain dissipation
        potential of ventilative cooling, Qach. 

        :param pair: Air density in kg/m, defaults to 1.2
        :type pair: float, optional
        :param cair: Air specific heat capacity in W/kg°C, defaults
            to 0.278
        :type cair: float, optional
        :param tset: Setpoint temperature in °C, defaults to 26
        :type tset: int, optional
        :return: Qach
        :rtype: float
        """
        # TODO far si che anche il tset possa essere preso dalla cooling schedule
        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df = df.loc[:,["Date/Time", "T_db_e[C]"]]
        delta_T = self.idf.mean_dt
        ach = self.idf.mean_ach
        return super().qach(df, vol=self.volume, pair=pair, cair=cair,
                            tset=tset, delta_T=delta_T, ach=ach)

    def cidh(self, thresholds=[18, 20, 22, 24, 26, 28]):
        """Prepare eplusout.csv to standardized CIDH (Cooling Internal Degree
        Hours) computation and finally return sum of residuals from defined
        thresholds.

        :param thresholds: temperature thresholds from which compute residuals,
         defaults to [18, 20, 22, 24, 26, 28]
        :type thresholds: list, optional
        :return: dictionary containing sum of residuals from thresholds
        :rtype: dict
        """

        df= copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.loc[:,["Date/Time", "T_db_i[C]"]]
        return super().cidh(df, thresholds=thresholds) # Call original function

    def fictitious_cooling(self, dataframe_on, eu_norm='16798-1:2019',
                           alpha=0.8):
        """Return fictitious cooling computed as the amount of cooling
        consumption needed in a building without mechanic system in order to
        reach category I comfort of adaptive comfort model. It also prepares
        eplusout.csv to standard KPI computation.

        :param dataframe_on: dataframe containing at least "Date/Time" and 
            "Q_c[Wh/m2]" columns, of the same building with 
            HVAC installad and ACH ventilation set to 0.
        :type df: class:`pandas.core.frame.DataFrame`
        :param eu_norm: It can be set to '15251:2007' if old UE norm 
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alpha: float, optional
        :return: fictitious cooling total
        :rtype: float
        """
        df_off = copy.deepcopy(self.df)
        df_on = copy.deepcopy(dataframe_on)
        df_on.columns = [c.replace(':ON', '') for c in df_on.columns]

        df_off["Date/Time"] = df_off["Date/Time"].apply(self.convert_to_datetime)
        df_on["Date/Time"] = df_on["Date/Time"].apply(self.convert_to_datetime)
        df_on = df_on.rename(columns={"DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"})
        df_on["Q_c[Wh/m2]"] = df_on["Q_c[Wh/m2]"].apply(self.convert_to_kWh_m2,\
                                                        args=(self.idf.area, ))   
        df_on = df_on.loc[:,["Date/Time","Q_c[Wh/m2]"]]      
        df_off = self.mean_indoor_op_air_temp(df_off, self.building_name)
        df_off = df_off.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df_off = df_off.loc[:,["Date/Time", "T_op_i[C]","T_db_e[C]"]]
        return super().fictitious_cooling(df_off, df_on, eu_norm=eu_norm,\
                                         alpha=alpha)


    def adaptive_residuals(self, eu_norm='16798-1:2019', alpha=0.8):
        """Prepare eplusout.csv to standardized adaptive comfort model categories
        computation.

        :param eu_norm: EU normative to compute Adaptive Comfort Model
            thresholds. It can be '16798-1:2019' or '15251:2007', defaults to
            '16798-1:2019'.
        :type eu_norm: str, optional
        :param alpha: EU '15251:2007' free parameter ranging [0,1), defaults to
            0.8
        :type alpha: float, optional
        :return: Dictionary where '>3': average distance from cat 1 upper
            bound; '>0': average distance from central line of cat 1
        :rtype: dict
        """
        # TODO Add graph handling. 
        df_off = copy.deepcopy(self.df)     
        df_off["Date/Time"] = df_off["Date/Time"].apply(self.convert_to_datetime)
        df_off = self.mean_indoor_op_air_temp(df_off, self.building_name)
        df_off = df_off.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        df_off = df_off.loc[:,["Date/Time", "T_op_i[C]","T_db_e[C]"]]
        return super().adaptive_residuals(df_off, eu_norm=eu_norm, alpha=alpha)


    def pmv_ppd(self, vel=0.1, met=1.2, clo=0.7, wme=0,
                standard="ISO 7730-2006", filter_by_occupancy = 0):
        """Prepare eplosout.csv to standardized predicted mean vote and
        percentage of dissatisfied (PMV, PPD) computation. Then return hourly
        pmv and ppd.

        :param vel: relative air speed, default 0.1
        :type vel: float, optional
        :param met: metabolic rate, [met] default 1.2
        :type met: float, optional
        :param clo: clothing insulation, [clo] default 0.5
        :type clo: float, optional
        :param wme: external work, [met] default 0
        :type wme: float, optional
        :param standard: default "ISO 7730-2006"
        :type standard: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or not,
            default 0.
        :type filter_by_occupancy: int, optional
        :return: dictionary containing keys "pmv" (Predicted Mean Vote) and
            "ppd" (Predicted Percentage of Dissatisfied occupants)
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = self.indoor_mean_air_temp(df, self.building_name)
        df = self.indoor_mean_air_RH(df, self.building_name)
        df = self.indoor_mean_rad_temp(df, self.building_name)
        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            df = df.loc[:, ["Date/Time", "T_rad_i[C]",
                        "RH_i[%]",
                        "T_db_i[C]", "Occupancy"]]
        else:
            df = df.loc[:, ["Date/Time", "T_rad_i[C]",
                            "RH_i[%]",
                            "T_db_i[C]"]]
        return super().pmv_ppd(df, vel=vel, met=met, clo=clo, wme=wme,
                               standard=standard, filter_by_occupancy=filter_by_occupancy)  # Call original function

    def adaptive_comfort_model(self, eu_norm='16798-1:2019',\
                               alpha=0.8,filter_by_occupancy=0, when = {}):
        """Prepare eplosout.csv to standardized adaptive comfort 
        model computation. Then return number of hours in each comfort 
        category and POR considering cat II boundaries.

        :param eu_norm: It can be set to '15251:2007' if old UE norm
            computation is desired, defaults to '16798-1:2019'.
        :type eu_norm: str, optional
        :param filter_by_occupancy: It can be set 0 or 1, depending on wether
            activate occupancy filtering on thermal comfort KPIs computation or
            not, default 0.
        :type filter_by_occupancy: int, optional
        :param alhpa: With old UE norm '15251:2007 alpha is a free parameter in
            range [0,1), defaults to 0.8
        :type alhpa: float, optional
        :param when: dictionary with 'start' and 'end' keys and values in format
            'year/month/day hour:minutes:seconds'
        :type when: dict, optional
        :return: Number of hours in each comfort category.
        :rtype: dictionary
        """
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = self.mean_indoor_op_air_temp(df, self.building_name)
        df = df.rename(columns={"Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]"})
        if filter_by_occupancy:
            df = self.get_occupancy(df, self.building_name)
            df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_e[C]","Occupancy"]]
        else:
            df = df.loc[:,["Date/Time", "T_op_i[C]","T_db_e[C]"]]
        return super().adaptive_comfort_model(df, eu_norm=eu_norm,\
             alpha=alpha, filter_by_occupancy=filter_by_occupancy, when = when)

    def energy_signature(self):
        """Prepare eplosout.csv to standardized 1D and 2D energy signature
        computation. Then return points to build HVAC graphs.

        :return: Points allowing to build 1D and 2D graphs of energy signature.
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.set_index("Date/Time", drop=True)
        # NOTE: Do not remove last comma. It is needed in the tuple.
        df = df.rename(columns={"DistrictCooling:Facility [J](TimeStep)":"Q_c[Wh/m3]"})
        df["Q_c[Wh/m3]"] = df["Q_c[Wh/m3]"].apply(self.convert_to_W_m3,\
                                                  args=(self.volume, ))

        # NOTE: Do not remove last comma. It is needed in the tuple.
        df = df.rename(columns={"DistrictHeating:Facility [J](TimeStep)": "Q_h[Wh/m3]"})
        df["Q_h[Wh/m3]"] = df["Q_h[Wh/m3]"].apply(self.convert_to_W_m3,\
                                                  args=(self.volume, ))

        df = self.indoor_mean_air_temp(df, self.building_name)
        df = df.rename(columns={
            "Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)":"T_db_e[C]",
            "Environment:Site Direct Solar Radiation Rate per Area [W/m2](TimeStep)": "Rad_direct_e[W/m2]",
            "Environment:Site Diffuse Solar Radiation Rate per Area [W/m2](TimeStep)": "Rad_diffuse_e[W/m2]"
            })
        rad_global = epw_reader.retrieve_column(self.idf.epw, "global_horizontal_radiation", year=1970)
        df = df.join(rad_global, on="Date/Time")
        df = df.rename(columns={"global_horizontal_radiation": "Rad_global_e[W/m2]"})
        df = df.loc[:, ["T_db_i[C]", "T_db_e[C]",
                        "Rad_global_e[W/m2]","Q_c[Wh/m3]","Q_h[Wh/m3]"]]
        return super().energy_signature(df)

    def co2(self, th_list = [600,900,1200,1500,1800,2100,2500]):
        """Prepare eplosout.csv to standardized CO2 concentration
        computation. 
        :param th_list: thresholds from which compute number of hours with average CO2 above
        :type th_list: list, optional
        :return: dictionary containing CO2 information
        :rtype: dict
        """
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = self.indoor_mean_co2(df, self.building_name)
        schedule = self.idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"][0].Outdoor_Carbon_Dioxide_Schedule_Name
        df = df.rename(columns={schedule.upper() + ":Schedule Value [](TimeStep)":"CO2_e[ppm]"})
        df = df.loc[:, ["Date/Time","CO2_i[ppm]", "CO2_e[ppm]"]]
        return super().co2(df,th_list= th_list)

    def q_c(self, frequency="runperiod"):
        """Compute cooling net need Qc in kWh/m2.

        :param frequency: Frequency of retrieved data
        :type frequency: str
        :return: Cooling net need
        :rtype: float
        """
        frequency = frequency.lower()
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.set_index("Date/Time", drop=True)
        hourly = df.resample("1H").sum()
        hourly = hourly["DistrictCooling:Facility [J](TimeStep)"]
        # NOTE: Do not remove last comma. It is needed in the tuple.
        hourly = hourly.apply(
            self.convert_to_kWh_m2, args=((self.idf.area, ))
            )
        if self.graph:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(constrained_layout=False, figsize=(50, 25))
            ax.plot(
                hourly.index,
                hourly.values,
                label="District Cooling",
                color="tab:cyan",
                lw=5,
            )
            fig.autofmt_xdate()
            plt.legend()
            plt.grid()
            plt.savefig(self.plot_dir / "district_cooling.png")
            plt.close(fig)

            fig, ax = plt.subplots(constrained_layout=True, figsize=(50, 25))
            x = np.sort(hourly.values)
            y = np.arange (len(hourly.values)) / float (len(hourly.values))
            ax.plot(x, y, linewidth = 4)
            plt.title("CDF cooling needs")
            ax.set_xlabel("kWh/m2")
            plt.grid()
            plt.savefig(self.plot_dir / "cdf_cooling.png")
            plt.close(fig)
            # hourly.resample("1W").sum().to_csv(self.plot_dir / "q_c_weekly_data.csv")
        if frequency == 'weekly':
            return list(hourly.resample("1W").sum().values)
        elif frequency == 'runperiod':
            return self.convert_to_kWh_m2(
                df["DistrictCooling:Facility [J](RunPeriod)"].iloc[-1],
                self.idf.area,
            )

    def q_h(self, frequency="runperiod"):
        """Compute heating net need Qh in kWh/m2

        :param frequency: Frequency of retrieved data
        :type frequency: str
        :return: Heating net need
        :rtype: float
        """
        frequency = frequency.lower()
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.set_index("Date/Time", drop=True)
        hourly = df.resample("1H").sum()
        hourly = hourly["DistrictHeating:Facility [J](TimeStep)"]
        # NOTE: Do not remove last comma. It is needed in the tuple.
        hourly = hourly.apply(
            self.convert_to_kWh_m2, args=((self.idf.area, ))
            )
        if self.graph:
            plt.rcParams.update({"font.size": 45})
            fig, ax = plt.subplots(constrained_layout=False, figsize=(50, 25))
            ax.plot(
                hourly.index,
                hourly.values,
                label="District Heating",
                color="tab:red",
                lw=5,
            )
            fig.autofmt_xdate()
            plt.legend()
            plt.grid()
            plt.savefig(self.plot_dir / "district_heating.png")
            plt.close(fig)
            # hourly.resample("1W").sum().to_csv(self.plot_dir / "q_h_weekly_data.csv")

        if frequency == 'weekly':
            return list(hourly.resample("1W").sum().values)
        elif frequency == 'runperiod':
            return self.convert_to_kWh_m2(
                df["DistrictHeating:Facility [J](RunPeriod)"].iloc[-1],
                self.idf.area,
            )

    def interior_lights(self):
        df = copy.deepcopy(self.df)
        df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
        df = df.set_index("Date/Time", drop=True)
        hourly = df.resample("1H").sum()
        hourly = hourly["InteriorLights:Electricity [J](TimeStep)"]
        # NOTE: Do not remove last comma. It is needed in the tuple.
        hourly = hourly.apply(
            self.convert_to_kWh_m2, args=((self.idf.area, ))
            )

        return self.convert_to_kWh_m2(
            df["InteriorLights:Electricity [J](RunPeriod)"].iloc[-1],
            self.idf.area,
        )

    # def imat(self):
    #     df = copy.deepcopy(self.df)
    #     df["Date/Time"] = df["Date/Time"].apply(self.convert_to_datetime)
    #     df = df.set_index("Date/Time", drop=True)
    #     return self.indoor_mean_air_temp()

    @staticmethod
    def convert_to_datetime(date_str):
        """It converts EnergyPlus "Date/Time" column to a timestamp referred to
        1970 as a sample year.

        :param date_str: EnergyPlus "Date/Time"
        :type date_str: str
        :return: timestamp
        :rtype: class:`pandas._libs.tslibs.timestamps.Timestamp`
        """
        if date_str[0] == " ":
            date_str = date_str[1:]
        date_str ="1970/" + date_str
        date = date_str.split(" ")[0]
        time = date_str.split(" ")[2]
        hour = time.split(":")[0]
        if hour != '24':
            return pd.to_datetime(date_str, format='%Y/%m/%d  %H:%M:%S')

        else:
            time = time.replace('24','00')
        return pd.to_datetime(date+'  '+time, format='%Y/%m/%d  %H:%M:%S') + \
            dt.timedelta(days=1)

    @staticmethod
    def get_occupancy(df, building_name, filter_by = ""):
        """Get occupancy values from eplusout.csv in boolean (0/1) form for each
        simulated timestep.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new `Occupancy` column
        :rtype: class:`pandas.core.frame.DataFrame`

        .. warning:: **Output:Variable, \*, Zone People Occupant Count,
            Timestep** is required in IDF.

        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone People Occupant Count [](TimeStep)" not in col or building_name not in col or filter_by not in col:
                df_new = df_new.drop(col, axis=1)
    
        df["Occupancy"] = df_new.mean(axis=1)
        df.loc[df["Occupancy"] != 0, "Occupancy"] = 1
        return df

    @staticmethod
    def mean_indoor_op_air_temp(df, building_name):
        """Compute indoor mean operative temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_op_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Operative Temperature [C](TimeStep)" not in col or building_name not in col:
                df_new = df_new.drop(col, axis=1)
    
        df["T_op_i[C]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_air_temp(df, building_name):
        """Compute indoor mean air temperature considering all thermal zones of
        the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_db_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Mean Air Temperature [C](TimeStep)" not in col or building_name not in col:
                df_new = df_new.drop(col, axis=1)

        df["T_db_i[C]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_air_RH(df, building_name):
        """Compute indoor mean air humidity considering all thermal zones of
        the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "RH_i[%]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Air Relative Humidity [%](TimeStep)" not in col or building_name not in col:
                df_new = df_new.drop(col, axis=1)
                
        df["RH_i[%]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_rad_temp(df, building_name):
        """Compute indoor mean radiant temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "T_rad_i[C]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Mean Radiant Temperature [C](TimeStep)" not in col or building_name not in col:
                df_new = df_new.drop(col, axis=1)
                
        df["T_rad_i[C]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def indoor_mean_co2(df,building_name):
        """Compute indoor mean radiant temperature considering all thermal
        zones of the main building block.

        :param df: eplusout.csv
        :type df: class:`pandas.core.frame.DataFrame`
        :return: DataFrame with new "CO2[ppm]" column
        :rtype: class:`pandas.core.frame.DataFrame`
        """
        building_name = building_name.upper()
        df_new = df.copy()
        for col in df_new.columns:
            if "Zone Air CO2 Concentration [ppm](TimeStep)" not in col or building_name not in col:
                df_new = df_new.drop(col, axis=1)
                
        df["CO2_i[ppm]"] = df_new.mean(axis=1)
        return df

    @staticmethod
    def convert_to_W_m3(value, volume):
        """Convert energy consumption expressed in J to W/m3.

        :param value: consumption value in J
        :type value: float
        :param volume: Volume
        :type volume: int or float
        :return: consumption value in W/m3
        :rtype: float
        """
        # TODO: change 600 in 60*timestep...
        return value/(600*volume)

    @staticmethod
    def convert_to_W_m2(value, area):
        """Convert energy consumption expressed in J to W/m3.

        :param value: consumption value in J
        :type value: float
        :param volume: Volume
        :type volume: int or float
        :return: consumption value in W/m3
        :rtype: float
        """
        # TODO: change 600 in 60*timestep...
        return value/(600*area)

    @staticmethod
    def convert_to_kWh_m2(value, area):
        """Convert energy consumption expressed in J to kWh/m2.

        :param value: consumption value in J
        :type value: float
        :param area: Area
        :type area: int or float
        :return: consumption value in kWh/m2
        :rtype: float
        """ 
        return value/(3.6*(10**6)*area)

    @staticmethod
    def convert_to_kWh(value):
        """Convert energy consumption expressed in J to kWh

        :param value: consumption value in J
        :type value: float
        :return: consumption value in kWh
        :rtype: float
        """ 
        return value/(3.6*(10**6))

