# -*- coding: utf-8 -*-
from predyce.IDF_class import IDF
import os, sys, inspect

currentdir = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe()))
)
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from pathlib import Path
from predyce import kpi
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import copy
import numpy as np


def mbe(data_true, data_predicted):
    return np.nanmean(data_predicted - data_true)


def rmse(data_true, data_predicted):
    return np.sqrt(np.nanmean((data_predicted - data_true) ** 2))


def rmse_mbe(data_true, data_predicted):
    return np.sqrt(
        rmse(data_true, data_predicted) ** 2
        + mbe(data_true, data_predicted) ** 2
    )


def calibration_signature(
    data_true, data_predicted, x_axis, plot_dir, graph=True
):
    plt.rc('font',**{'family':'serif','serif':['Palatino']})
    plt.rc('text', usetex=True)
    plt.rcParams.update({"font.size": 45})
    num = (data_true - data_predicted).dropna()
    cal_sig = 100 * num / data_true.max()
    if graph:
        fig, ax = plt.subplots(constrained_layout=True, figsize=(18, 15))
        # ax.plot(cal_sig, label=data_true.columns[0], lw=3)
        ax.plot(
            x_axis, cal_sig, "o", label=data_true.columns[0], markersize=10
        )
        ax.set_title("Calibration Signature")
        ax.set_xlabel("Outside Air Temperature [°C]")
        ax.set_ylabel("Percent")
        ax.set_ylim([-15, 15])
        plt.axhline(y=0, color="r", linestyle="-")
        plt.grid()
        plot_dir = Path(plot_dir)
        plt.savefig(plot_dir / "calibration_signature.png")
        plt.close(fig)


def comparison(data_true, data_predicted, plot_dir):
    plt.rc('font',**{'family':'serif','serif':['Palatino']})
    plt.rc('text', usetex=True)
    where = (data_true - data_predicted).dropna().index
    plt.rcParams.update({"font.size": 45})

    # First plot.
    fig, ax = plt.subplots(constrained_layout=True, figsize=(18, 18))
    ax.plot(
        data_predicted.loc[where],
        data_true.loc[where],
        marker="o",
        ls="",
        color="indigo",
        markersize=10,
    )
    ax.set_title("Measured vs Simulated Indoor Drybulb Temperatures")#\nAQ June")
    plt.axis('square')
    ax.set_xlabel(
        r"Simulated $T_{\mathrm{db}}^\mathrm{i}$ [°C]"
    )
    ax.set_ylabel(
        r"Measured $T_{\mathrm{db}}^\mathrm{i}$ [°C]"
    )
    plt.grid()
    plot_dir = Path(plot_dir)
    x = np.linspace(*ax.get_xlim())
    ax.plot(x, x, 'r')
    plt.savefig(plot_dir / "predicted_vs_true.png")
    plt.close(fig)

    labels = ['-'.join((str(d.day), str(d.month))) for d in data_predicted.loc[where].index]

    # Second plot: Data vs Time
    fig, ax = plt.subplots(constrained_layout=False, figsize=(40, 20))
    ax.plot(
        data_predicted.loc[where].index + pd.DateOffset(year=2021),
        data_predicted.loc[where],
        color="tab:cyan",
        label="Simulated",
        lw=5,
    )
    ax.plot(
        data_true.loc[where].index + pd.DateOffset(year=2021),
        data_true.loc[where],
        color="tab:blue",
        label="Measured",
        lw=5,
    )
    ax.set_title("Indoor Drybulb Temperature; Measured and Simulated")#\nAQ June")
    ax.set_xlabel("Datetime")
    ax.set_ylabel(r"$T_{\mathrm{db}}^\mathrm{i}$ [°C]")
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plot_dir = Path(plot_dir)
    plt.savefig(plot_dir / "predicted_vs_true_time.png")
    plt.close(fig)

class EnergyPlusCalibration:
    def __init__(
        self, df, idf, plot_dir, data_true_dir, graph=False, time_bin="1H"
    ):
        self.df = df
        self.df.columns = [c.replace(":ON", "") for c in self.df.columns]
        self.building_name = idf.main_block
        self.area = idf.area
        self.volume = idf.volume
        self.idf = idf
        self.plot_dir = plot_dir
        self.data_true_dir = str(Path(data_true_dir))
        self.graph = graph
        self.time_bin = time_bin

    def retrieve_data(self, col_name, filter_by=[]):
        # Retrieve simulated data.
        df_sim = copy.deepcopy(self.df)
        df_sim["Date/Time"] = df_sim["Date/Time"].apply(
            kpi.EnergyPlusKPI.convert_to_datetime
        )
        df_sim = df_sim.set_index("Date/Time")
        df_sim = df_sim.resample(self.time_bin).mean()
    
        # Remove the first seven days from simulated data.
        first_date = df_sim.index[0]
        df_sim = df_sim.loc[df_sim.index > first_date + dt.timedelta(days=6)]

        x_axis = df_sim.rename(
            columns={
                "Environment:Site Outdoor Air Drybulb Temperature [C](TimeStep)": "T_db_e[C]"
            }
        ).loc[:, "T_db_e[C]"]
        x_axis = x_axis.to_frame()

        if col_name == "T_db_i":
            if filter_by == []:
                df_sim = kpi.EnergyPlusKPI.indoor_mean_air_temp(
                    df_sim, self.building_name
                )
                df_sim = df_sim.loc[:, ["T_db_i[C]"]]
                data_sim = df_sim.loc[:, ["T_db_i[C]"]]
            else:
                df_new = df_sim.copy()
                for col in df_new.columns:
                    if (
                        "Zone Mean Air Temperature [C](TimeStep)" not in col
                        or self.building_name.upper() not in col
                    ):
                        df_new = df_new.drop(col, axis=1)
                df_final = pd.DataFrame()
                for el in filter_by:
                    for col in df_new.columns:
                        if el in col:
                            df_final[col] = df_new[col]
                df_sim["T_db_i[C]"] = df_final.mean(axis=1)
                data_sim = df_sim.loc[:, ["T_db_i[C]"]]

        elif col_name == "Q_c":
            df_sim = df_sim.rename(
                columns={
                    "DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"
                }
            )
            df_sim["Q_c[Wh/m2]"] = df_sim["Q_c[Wh/m2]"].apply(
                kpi.EnergyPlusKPI.convert_to_W_m2, args=(self.volume,)
            )
            data_sim = df_sim.loc[:, ["Q_c[Wh/m2]"]]

        elif col_name == "Q_h":
            df_sim = df_sim.rename(
                columns={
                    "DistrictCooling:Facility [J](TimeStep)": "Q_c[Wh/m2]"
                }
            )
            df_sim["Q_c[Wh/m2]"] = df_sim["Q_c[Wh/m2]"].apply(
                kpi.EnergyPlusKPI.convert_to_W_m2, args=(self.volume,)
            )
            data_sim = df_sim.loc[:, ["Q_c[Wh/m2]"]]

        else:
            raise Exception("Invalid col_name")

        # retrieve measured data
        df_true = pd.read_csv(
            self.data_true_dir, index_col=0, sep=";", parse_dates=True
        )

        df_true = df_true.resample(self.time_bin).mean()

        df_new = df_true.copy()

        for col in df_new.columns:
            # TODO: Gestire meglio questo controllo.
            if (
                col_name not in col
            ):  # or self.building_name.upper() not in col:
                df_new = df_new.drop(col, axis=1)

        df_final = df_new.copy()
        if filter_by != []:
            df_final = pd.DataFrame()
            for el in filter_by:
                for col in df_new.columns:
                    if el in col:
                        df_final[col] = df_new[col]

        data_true = df_final.mean(axis=1).to_frame()
        data_true.columns = ["T_db_i[C]"]

        return (data_true, data_sim, x_axis)
