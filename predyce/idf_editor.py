#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to edit IDF file"""
from geomeppy import recipes
from numpy import sin, cos, deg2rad
import numpy as np
import random
import pandas as pd
from predyce.database_manager import Database
from pathlib import Path
import os
import datetime
import uuid

P = Path(os.path.dirname(__file__))
PATH = P / "database.json"
CAVITY_AIR_RESISTANCE = {
    "thickness": [0, 0.005, 0.007, 0.010, 0.015, 0.025, 0.050, 0.100, 0.300],
    "upwards": [0.0, 0.11, 0.13, 0.15, 0.16, 0.16, 0.16, 0.16, 0.16],
    "horizontal": [0.0, 0.11, 0.13, 0.15, 0.17, 0.18, 0.18, 0.18, 0.18],
    "downwards": [0.0, 0.11, 0.13, 0.15, 0.17, 0.18, 0.21, 0.22, 0.23],
}


def set_fields(obj, data=None, **fields):
    """Set value for a specific field in an IDF object.
    A dictionary with all pairs of key: value can be passed. In this case all
    keys are converted by replacing spaces with underscores.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param data: dictionary where each key corresponds to a pararameter to be
        changed in the IDF and each value corresponds to the new value.
    :type data: dict, optional
    :param fields: Same as `data` parameter but fields are passed as keyword
        arguments.

    :Example:

    .. code-block:: python

        set_fields(material, data={"Conductivity": 0.3})
        set_fields(material, Conductivity=0.3)

    """
    if isinstance(data, dict):
        data = convert_keys(data)
        for k in data.keys():
            try:
                setattr(obj, k, data[k])
            except Exception:
                print("Field %s does not exist" % k)

    for key, value in fields.items():
        try:
            setattr(obj, key, value)
        except Exception:
            print("Field %s does not exist" % key)


def set_object_params(idf, obj_str, data=None, **fields):
    """Set parameters on IDF objects given a string representing the object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param obj_str: String representing the object; the typology and the name
        are separated by a comma. For example "material,Timber Flooring"
        will edit the idfobjects["MATERIAL"] whose name is Timber Flooring;
        a string like "material" will edit all materials regardless of name.
    :type obj_str: str
    :param data: dictionary where each key corresponds to a pararameter to be
        changed in the IDF and each value corresponds to the new value.
    :type data: dict, optional
    :param fields: Same as `data` parameter but fields are passed as keyword
        arguments, e.g. Conductivity=0.3 which is equivalent to
        {"Conductivity": 0.3} for `data` field.

    :Example:

    .. code-block:: python

        set_object_params(idf, "material,Timber Flooring", data={"Conductivity": 0.3})
        set_object_params(idf, "material,Timber Flooring", Conductivity=0.3)
    """
    typology = obj_str.split(",")[0]
    try:
        name = obj_str.split(",")[1]
    except:
        name = None

    if name is not None:
        for el in idf.idfobjects[typology.upper()]:
            if el.Name == name:
                set_fields(el, data, **fields)
    else:
        for el in idf.idfobjects[typology.upper()]:
            set_fields(el, data, **fields)


def get_layer(obj, name, exact=False):
    """Get layer of a material given the name.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param name: Name of the layer or first/last
    :type name: str
    :return: Layer key
    :rtype: str
    :param exact: If the compared strings have to perfectly match, defaults to
        `False`
    :type exact: bool, optional
    """
    if name == "first":
        return obj.objls[2]

    elif name == "last":
        for el in reversed(obj.objls):
            if getattr(obj, el) != "":
                return el

    else:
        for el in obj.objls:
            if exact:
                if name.lower() == getattr(obj, el).lower():
                    layer = el
                    return layer
            else:
                if name.lower() in getattr(obj, el).lower():
                    layer = el
                    return layer


def insert_layer(obj, layer, name, below=False):
    """Insert a new layer above or below the given layer.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param layer: Key of the level above which the new level is inserted
    :type layer: str
    :param name: Name of the new layer
    :type name: str
    :param below: `True` if new layer have to be inserted belove the given layer
    :type below: bool, defaults to `False`
    """
    layer_pos = obj.objls.index(layer)

    for cnt, key in enumerate(obj.objls):
        value = getattr(obj, key)
        if value == "":
            max_key = cnt
            break

    if below == False:
        for a in range(layer_pos, max_key)[::-1]:
            setattr(obj, obj.objls[a + 1], getattr(obj, obj.objls[a]))

        setattr(obj, obj.objls[a], name)
    else:
        for a in range(layer_pos + 1, max_key)[::-1]:
            setattr(obj, obj.objls[a + 1], getattr(obj, obj.objls[a]))
        setattr(obj, obj.objls[layer_pos + 1], name)


def remove_layer(obj, layer_name, exact=False):
    """Remove a layer given the name.

    :param obj: geomeppy object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param layer_name: Name of the layer which needs to be removed
    :type layer_name: str
    :param exact: If the compared strings have to perfectly match, defaults to
        `False`
    :type exact: bool, optional
    """
    layer = get_layer(obj, layer_name, exact=exact)
    layer_pos = obj.objls.index(layer)

    for cnt, key in enumerate(obj.objls):
        value = getattr(obj, key)
        if value == "":
            max_key = cnt
            break

    setattr(obj, obj.objls[layer_pos], "")
    for a in range(layer_pos, max_key):
        setattr(obj, obj.objls[a], getattr(obj, obj.objls[a + 1]))

    obj.obj = obj.obj[:-1]  # Remove last empty field


def set_outdoor_co2(idf, co2_schedule=""):
    """Activate outdoor CO2 contaminant calculation according to given schedule.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param co2_schedule: Schedule which has to be assigned. It follows the rules of
        Schedule:Compact objects of EnergyPlus.
    :type co2_schedule: dict
    """
    # Find or create
    if len(idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"]) > 0:
        co2_obj = idf.idfobjects["ZONEAIRCONTAMINANTBALANCE"][0]
    else:
        co2_obj = idf.newidfobject("ZONEAIRCONTAMINANTBALANCE")

    # Remove () in schedule name for output readibility
    co2_schedule["Name"] = (
        co2_schedule["Name"].replace("(", "_").replace(")", "_")
    )

    # Set fields
    co2_obj_properties = {
        "Carbon Dioxide Concentration": "Yes",
        "Outdoor Carbon Dioxide Schedule Name": co2_schedule["Name"],
        "Generic Contaminant Concentration": "Yes",
        "Outdoor Generic Contaminant Schedule Name": "Off 24/7",
    }
    set_fields(co2_obj, co2_obj_properties)

    # Add schedule
    sc = idf.newidfobject("Schedule:Compact".upper())
    set_fields(sc, data=co2_schedule)
    # Add needed outputs #TODO maybe better to add from kpi
    set_outputs(
        idf, ["Schedule Value"], co2_schedule["Name"], frequency="Timestep"
    )
    set_outputs(idf, ["Zone Air CO2 Concentration"], "*", frequency="Timestep")


def change_co2_prod_rate(idf, filter_by="", multiplier=0, value=0):
    """Change zone CO2 generation rate.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param multiplier: multiply current CO2 generation rate
    :type multiplier: float, optional
    :param value: sobstitute current CO2 generation rate
    :type value: float, optional
    """
    # Filter by zone name
    people_obj_list = [
        p
        for p in idf.idfobjects["PEOPLE"]
        if filter_by.lower() in p.Name.lower()
    ]

    # Set CO2 geeration rate
    for obj in people_obj_list:
        if multiplier:
            obj.Carbon_Dioxide_Generation_Rate = (
                obj.Carbon_Dioxide_Generation_Rate * multiplier
            )
        elif value:
            obj.Carbon_Dioxide_Generation_Rate = value


def add_internal_mass(idf, filter_by="", area=0, relative=False):
    """Add internal mass to the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param area: Area of the internal mass, defaults to 0
    :type area: int, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    # create internal mass material
    new_mat = idf.newidfobject("MATERIAL")
    rand = random.randint(0, 1000)
    mat_properties = {
        "Name": "internal mass material - " + str(rand),
        "Roughness": "Rough",
        "Thickness": 0.012,
        "Conductivity": 1.4,
        "Density": 2100,
        "Specific Heat": 840,
        "Thermal Absorptance": 0.9,
        "Solar Absorptance": 0.6,
        "Visible Absorptance": 0.6,
    }
    set_fields(new_mat, mat_properties)

    # create internal mass cons and cons_rev
    new_cons = idf.newidfobject("CONSTRUCTION")
    new_cons_rev = idf.newidfobject("CONSTRUCTION")
    cons_properties = {
        "Name": "Internal Mass Construction - " + str(rand),
        "Outside Layer": new_mat.Name,
    }
    cons_properties_rev = {
        "Name": "Internal Mass Construction - " + str(rand) + "_Rev",
        "Outside Layer": new_mat.Name,
    }
    set_fields(new_cons, cons_properties)
    set_fields(new_cons_rev, cons_properties_rev)

    # create an internal mass per zone according to filter_by
    zones = idf.idfobjects["ZONE"]
    where = [z for z in zones if filter_by.lower() in z.Name.lower()]
    for i in range(0, len(where)):
        mass = idf.newidfobject("INTERNALMASS")
        if relative:
            final_area = area * where[i].Floor_Area
        mass_properties = {
            "Name": where[i].Name + " internal mass - " + str(rand),
            "Construction Name": new_cons.Name,
            "Zone Name": where[i].Name,
            "Surface Area": round(final_area, 2),
        }
        set_fields(mass, mass_properties)


def set_simplified_windows(idf, win_type="Simplified window system"):
    """Sobstitute all Windows and GlazedDoor objects in an IDF with a simpler
    object characterized by only 3 fields: U-factor, SHGC and visible
    transmittance.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param win_type: Construction name, defaults to "Simplified window system"
    :type win_type: str, optional
    """

    if win_type == None:
        pass  # leggi default

    # prendi l'elemento construction dal database
    d = Database(PATH)
    d.load_file()
    win = d.get_object("CONSTRUCTION", win_type)
    new_win = idf.newidfobject("CONSTRUCTION")
    set_fields(new_win, win)
    win_layer = new_win.obj[2]
    layer_obj = d.get_object("WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM", win_layer)
    new_mat = idf.newidfobject("WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM")
    set_fields(new_mat, layer_obj)

    # cambiare il nome del pacchetto finestra usato in tutti gli oggetti finestra
    # solo al type window o anche ad altri tipi va riassegnato?
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for f in fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            if len(f.Shading_Control_Name) > 0:
                # If the window has a shading control, the shaded version of
                # the construction has to be analysed and modified in order to
                # make it match the new window construction.
                for sc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
                    if sc.Name == f.Shading_Control_Name:
                        for shad_con in idf.idfobjects["CONSTRUCTION"]:
                            if (
                                shad_con.Name
                                == sc.Construction_with_Shading_Name
                            ):
                                # At this point the construction object with
                                # shading has been found. Only the shading
                                # materials have to be kept.
                                for el in shad_con.obj[2:]:
                                    keep = 0
                                    for wms in idf.idfobjects[
                                        "WINDOWMATERIAL:SHADE"
                                    ]:
                                        if el == wms.Name:
                                            keep = 1
                                    if not keep:
                                        remove_layer(shad_con, el, True)

                                # Copy new window layer to the shaded version
                                # of the construction to make them match.
                                # TODO: Controllare se lo shading deve essere
                                #       interno o esterno e inserire i livelli
                                #       finestra nell'ordine corretto.
                                for layer in new_win.obj[2:]:
                                    pos = get_layer(shad_con, "last")
                                    insert_layer(shad_con, pos, layer, True)

            f.Construction_Name = win_type


def change_windows_system(idf, win_type=None):
    """Change all Windows and GlazedDoors system with a different
    construction object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param win_type: Construction name, defaults to None
    :type win_type: str, optional
    """
    # TODO: could be done according to orientation passing a map like in wwr
    if win_type == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    win = d.get_object("CONSTRUCTION", win_type)

    # Add Construction to IDF
    new_win = idf.newidfobject("CONSTRUCTION")
    set_fields(new_win, win)

    # Add all materials of the window to the IDF if they are not present.
    # Materials are taken from the database.
    win_layers = new_win.obj[2:]
    win_materials = list(idf.idfobjects["WINDOWMATERIAL:GAS"]) + list(
        idf.idfobjects["WINDOWMATERIAL:GLAZING"]
    )
    for el in win_layers:
        found = 0
        for obj in win_materials:
            if obj.Name == el:
                found = 1
                break

        if not found:
            for el_type in ["WINDOWMATERIAL:GLAZING", "WINDOWMATERIAL:GAS"]:
                if d.get_object(el_type, el) != -1:
                    break

            el_obj = d.get_object(el_type, el)
            new_el = idf.newidfobject(el_type)
            win_materials.append(new_el)
            set_fields(new_el, el_obj)

    # Update Construction name in all windows object.
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for f in fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            # Check if the window does not overlook another room.
            if len(f.Outside_Boundary_Condition_Object) == 0:
                if len(f.Shading_Control_Name) > 0:
                    # If the window has a shading control, the shaded version of
                    # the construction has to be analysed and modified in order to
                    # make it match the new window construction.
                    for sc in idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
                        if sc.Name == f.Shading_Control_Name:
                            for shad_con in idf.idfobjects["CONSTRUCTION"]:
                                if (
                                    shad_con.Name
                                    == sc.Construction_with_Shading_Name
                                ):
                                    # At this point the construction object with
                                    # shading has been found. Only the shading
                                    # materials have to be kept.
                                    for el in shad_con.obj[2:]:
                                        keep = 0
                                        for wms in idf.idfobjects[
                                            "WINDOWMATERIAL:SHADE"
                                        ]:
                                            if el == wms.Name:
                                                keep = 1
                                        if not keep:
                                            remove_layer(shad_con, el, True)

                                    # Copy new window layer to the shaded version
                                    # of the construction to make them match.
                                    # TODO: Controllare se lo shading deve essere
                                    #       interno o esterno e inserire i livelli
                                    #       finestra nell'ordine corretto.
                                    for layer in new_win.obj[2:]:
                                        pos = get_layer(shad_con, "last")
                                        insert_layer(
                                            shad_con, pos, layer, True
                                        )

                f.Construction_Name = win_type


def change_shgc(idf, value):
    """Set an SHGC (Solar Heat Gain Coefficient) value to all windows of type
    SimpleGlazingSystem.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New SHGC value
    :type value: float
    """
    for f in idf.idfobjects["WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM"]:
        set_fields(f, {"Solar Heat Gain Coefficient": value})


def change_ufactor_windows(idf, value):
    """Set an U-Factor value to all windows of type SimpleGlazingSystem.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New U-Factor value
    :type value: float
    """
    for f in idf.idfobjects["WINDOWMATERIAL:SIMPLEGLAZINGSYSTEM"]:
        set_fields(f, {"UFactor": value})


def change_wwr(idf, wwr_value=0.2, wwr_map_value={}):
    """Change wwr value for all windows in an IDF object or specify in
    wwr_map_value dictionary wwr corresponding to specific windows orientation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param wwr_value: wwr for all windows, if not specified it is 20%
    :type wwr_value: float in range [0,1]
    :param wwr_map_value: dictionary where each key corresponds to a
        orientation and value to wwr.
    :type wwr_map_value: dict, {int (0,90,180,270): float (in range [0,1])}
    """
    # if wwr_value == None and wwr_map_value == None:
    #     idf.set_wwr()
    # elif wwr_value != None and wwr_map_value == None:
    #     idf.set_wwr(wwr=wwr_value)
    # elif wwr_value == None and wwr_map_value != None:
    #     idf.set_wwr(wwr_map=wwr_map_value)
    # else:
    #     idf.set(wwr=wwr_value, wwr_map=wwr_map_value)
    idf.set_wwr(wwr=wwr_value, wwr_map=wwr_map_value)


def add_ceiling_insulation_tilted_roof(
    idf, ins_data=None, filter_by="", **fields
):
    """Add insulation layer on tilted roof environment non occupied ceiling.
    Area and volume reductions of roof object are neglected.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_data)

    # Find which BuildingSurface:Detailed are ceilings/roof floor
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Ceiling"  # Filter only ceilings
        and filter_by.lower() in b.Zone_Name.lower()
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)
    for key, value in fields.items():
        if key != "ins_data":
            setattr(new_mat, key, value)

    for w in cons_list:
        for c in cons:
            # Add as last layer
            if c.Name == w:
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

            # Add as first layer (reversed construction)
            if c.Name == w + "_Rev":
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)


def add_roof_insulation_tilted_roof(idf, ins_data=None):
    """Add insulation layer on tilted roof environment non occupied as internal
    layer under tilted part. Area and volume reductions on roof object are
    neglected.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: str, optional
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_data)

    # Find which BuildingSurface:Detailed represent tilted roof
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Roof"  # Filter only tilted roof
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # Add as last layer (reversed construction)
            if c.Name == w + "_Rev":
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

            # Add as first layer
            if c.Name == w:
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)


def add_external_insulation_walls(idf, ins_data=None, filter_by="", **fields):
    """Add external insulation layer and plaster layer on external walls.
    Since layers are added externally internal building area and volume are
    not impacted.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction objects list (insulation material and
        plaster), defaults to None
    :type ins_data: list of two string elements, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments

    :Example:

    .. code-block:: python

        add_external_insulation_walls(
            idf,
            ins_data=[
                [
                    "extruded polystyrene panel XPS 35 kg/m3 15 mm",
                    "plaster lime and gypsum 15 mm",
                ]
            ],
            filter_by="kitchen",
            Conductivity=0.4,
        )

    .. note:: *ins_data* parameter takes only names; the respective entire
        objects are included in the JSON database.
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    obj_list = [d.get_object("MATERIAL", name) for name in ins_data]

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed
        and filter_by.lower() in b.Zone_Name.lower()
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    for cnt, mat in enumerate(obj_list):
        # Add insulation material in Material list
        new_mat = idf.newidfobject("MATERIAL")
        set_fields(new_mat, mat)

        # Set more fields.
        if cnt == 0:
            for key, value in fields.items():
                if key != "ins_data":
                    setattr(new_mat, key, value)

        # if thickness is not None:
        #     set_fields(new_mat, Thickness=thickness)
        # if conductivity is not None:
        #     set_fields(new_mat, Conductivity=conductivity)

        for w in cons_list:
            for c in cons:
                # Add as last layer
                if c.Name == w:
                    last_layer = get_layer(c, "first")
                    insert_layer(c, last_layer, mat["Name"], below=False)

                # Add as first layer (reversed construction)
                if c.Name == w + "_Rev":
                    first_layer = get_layer(c, "last")
                    insert_layer(c, first_layer, mat["Name"], below=True)


def change_ufactor_walls(idf, ufactor, relative=False):
    """Change Ufactor of walls to desired value or increment it by a
    specified percentage.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value or incremental percentage if
        `relative` field is set to `True`
    :type ufactor: float
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ufactor(idf, ufactor, "Walls", relative)


def change_specific_heat_material(idf, material_name, value, relative=False):
    # TODO: Si può generalizzare a obj e parameter e non solo material e
    #       specific heat.
    """Change specific heat of a material.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param material_name: Name of the material
    :type material_name: str
    :param value: New value for specific heat.
    :type value: int
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    for mat in idf.idfobjects["MATERIAL"]:
        if mat.Name == material_name:
            if relative:
                mat.Specific_Heat += mat.Specific_Heat * value
            else:
                mat.Specific_Heat = value


def change_ufactor_roofs(idf, ufactor, relative=False):
    """Change Ufactor of roofs to desired value or increment it by a
    specified percentage.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value or incremental percentage if
        `relative` field is set to `True`
    :type ufactor: float
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ufactor(idf, ufactor, "Roof", relative)


def change_ufactor(idf, ufactor, where="Walls", relative=False):
    """Change Ufactor of specified Construction objects to desired value acting
    on thickness of most insulating layer in the construction object.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ufactor: desired final ufactor value
    :type ufactor: float
    :param where: where to compute Ufactor, could be "Pavements", "Walls",
        "Ceilings","Roof" or the name of a specific Construction object
    :type where: string, defaults to "Walls"
    """
    if where not in ["Pavements", "Walls", "Ceilings", "Roof"]:
        raise Exception(
            "Specified element must be chosen among Pavements, Walls, Ceiling and Roof"
        )

    Re = 0.04
    cons_list = []

    if where.lower() == "walls":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Wall"  # Filter only walls
            and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
        ]
        Ri = 0.13

    elif where.lower() == "floors":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Floor"  # Filter only pavaments
        ]
        Ri = 0.1

    elif where.lower() == "ceilings":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Ceiling"  # Filter only ceilings
        ]
        Ri = 0.1

    elif where.lower() == "roof":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Roof"  # Filter only roof
        ]
        Ri = 0.1

    else:
        try:
            # TODO forse where potrebbe essere una lista di materiali?
            names_list = []
            for cons in idf.idfobjects["CONSTRUCTION"]:
                if cons.Name == where:
                    names_list.append(cons.Name)
                    break
            for bsd in idf.idfobjects["BUILDINGSURFACE:DETAILED"]:
                if bsd.Construction_Name == where:
                    if bsd.tilt == 90:
                        Ri = 0.13
                    else:
                        Ri = 0.1
        except:
            print("Not present construction object")

    names_list = list(dict.fromkeys(names_list))
    for name in names_list:
        for cons in idf.idfobjects["CONSTRUCTION"]:
            if cons.Name == name:
                cons_list.append(cons)

    for cnt, the_list in enumerate(cons_list):
        Rl = []
        lower_lambda_mat = ""
        lower_lambda = 500
        mat_to_change = ""
        mats = list(the_list.obj[2:])
        # Find most insulating material in the construction layers
        for el in mats:
            for materials in idf.idfobjects["MATERIAL"]:
                if (
                    materials.Name == el
                    and materials.Conductivity < lower_lambda
                ):
                    lower_lambda = materials.Conductivity
                    lower_lambda_mat = materials.Name
                    mat_to_change = materials
                    break

        mats.remove(lower_lambda_mat)
        # Compute constant Ufactor part
        for el in mats:
            found = 0
            for materials in idf.idfobjects["MATERIAL"]:
                if materials.Name == el:
                    Rl.append(materials.Thickness / materials.Conductivity)
                    found = 1
                    break

            if not found:
                for materials in idf.idfobjects["MATERIAL:NOMASS"]:
                    if materials.Name == el:
                        Rl.append(materials.Thermal_Resistance)
                        break

        # create material copy not to impact on other construction objects
        new_mat = idf.copyidfobject(mat_to_change)
        new_mat.Name = mat_to_change.Name + "_._" + str(cnt)
        layer = get_layer(the_list, mat_to_change.Name)
        setattr(the_list, layer, new_mat.Name)

        if relative:
            old_value = compute_ufactor(idf, the_list.Name)
            ufactor = old_value + old_value * ufactor

        # new thickness needed in mat_to_change to obtained desired Ufactor
        Rn = sum(Rl)
        Rt = Ri + Re + Rn
        x = max((1 / ufactor - Rt) * lower_lambda, 0.001)
        if x == 0.001:
            lower_lambda = max(x * (ufactor - Rt), 0.001)
        set_fields(new_mat, {"Thickness": x, "Conductivity": lower_lambda})


def compute_ufactor(idf, where="Walls"):
    """Compute Ufactor of specified opaque Construction objects.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param where: where to compute Ufactor, could be "Pavaments", "Walls",
        "Ceilings","Roof" or the name of a specific Construction object
    :type where: string, defaults to "Walls"
    """

    U_value_opaque = []
    Re = 0.04
    cons_list = []

    if where == "Walls":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Wall"  # Filter only walls
            and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
        ]
        Ri = 0.13

    elif where == "Floors":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Floor"  # Filter only pavaments
        ]
        Ri = 0.1

    elif where == "Ceilings":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Ceiling"  # Filter only ceilings
        ]
        Ri = 0.1

    elif where == "Roof":
        bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
        names_list = [
            b.Construction_Name
            for b in bsd
            if b.Surface_Type == "Roof"  # Filter only roof
        ]
        Ri = 0.1

    else:
        try:
            # TODO forse where potrebbe essere una lista di materiali?
            names_list = []
            for cons in idf.idfobjects["CONSTRUCTION"]:
                if cons.Name == where:
                    names_list.append(cons.Name)
                    break
            for bsd in idf.idfobjects["BUILDINGSURFACE:DETAILED"]:
                if bsd.Construction_Name == where:
                    if bsd.tilt == 90:
                        Ri = 0.13
                    else:
                        Ri = 0.1
        except:
            print("Not present construction object")

    names_list = list(dict.fromkeys(names_list))
    for name in names_list:
        for cons in idf.idfobjects["CONSTRUCTION"]:
            if cons.Name == name:
                cons_list.append(cons)

    for the_list in cons_list:

        Rl = []
        for el in the_list.obj[2:]:
            found = 0
            for materials in idf.idfobjects["MATERIAL"]:
                if materials.Name == el:
                    Rl.append(materials.Thickness / materials.Conductivity)
                    found = 1
                    break

            if not found:
                for materials in idf.idfobjects["MATERIAL:NOMASS"]:
                    if materials.Name == el:
                        Rl.append(materials.Thermal_Resistance)
                        break

        Rn = sum(Rl)
        Rt = Ri + Re + Rn
        U_value_opaque.append(1 / Rt)

    return np.mean(U_value_opaque)


def add_internal_insulation_walls(idf, ins_data=None):
    """Add internal insulation layer and plaster layer on external walls.

    Since layers are added internally building area and volume are minimally
    reduced. However since information about external perimeter are not
    available in IDF file, the impact of these reductions is not considered.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction objects list (insulation material and
        plaster), defaults to None
    :type ins_data: list of two string elements, optional
    """
    if ins_data == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    obj_list = [d.get_object("MATERIAL", name) for name in ins_data]

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    for mat in obj_list:
        # Add insulation material in Material list
        new_mat = idf.newidfobject("MATERIAL")
        set_fields(new_mat, mat)

        for w in cons_list:
            for c in cons:
                # Add as last layer (reversed construction)
                if c.Name == w + "_Rev":
                    last_layer = get_layer(c, "first")
                    insert_layer(c, last_layer, mat["Name"], below=False)

                # Add as first layer
                if c.Name == w:
                    first_layer = get_layer(c, "last")
                    insert_layer(c, first_layer, mat["Name"], below=True)


def add_cavity_insulation_walls(idf, ins_data=None):
    """Add insulation layer in external walls cavity.

    Information about cavity thickness are not available in IDF file but are
    retrieved based on air resistance values. However, cavity thickness could
    be underestimated and insulation thickness could be increased acting on
    material object fields.

    Based on known air resistance values in cavities, cavity thickness is
    retrieved. If insulation material thickness is thinner than available
    space, air resistance is consequently reduced, otherwise cavity is removed
    and completely sobstituted by insulation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_data: Construction name, defaults to None
    :type ins_data: string, optional
    """

    air_r = pd.DataFrame(CAVITY_AIR_RESISTANCE)
    air_r = air_r.set_index("thickness")

    if ins_data == None:
        # TODO: Take default element
        pass

    # leggi da database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_data)

    # Find which BuildingSurface:Detailed walls are exposed
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Wall"  # Filter only walls
        and b.Outside_Boundary_Condition == "Outdoors"  # Only exposed wall
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates
    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # Add as last layer
            if c.Name == w:
                lista = [l for l in c.obj]
                for el in lista:
                    for nomass in idf.idfobjects["MATERIAL:NOMASS"]:
                        if nomass.Name == el:
                            # Convert air R to air thickness
                            air_thickness = air_r[
                                air_r["horizontal"]
                                == nomass.Thermal_Resistance
                            ].index[
                                0
                            ]  # Thickness values corresponding to R

                            # Insert material layer near cavity layer.
                            air_lay = get_layer(c, nomass.Name)
                            insert_layer(c, air_lay, mat["Name"], below=False)

                            # Compute new air thickness (difference)
                            new_air_thickness = (
                                air_thickness - mat["Thickness"]
                            )

                            # Find which air R is the best for new thickness
                            absolute_difference_function = (
                                lambda list_value: abs(
                                    list_value - new_air_thickness
                                )
                            )
                            new_air_thickness = min(
                                list(air_r.index.values),
                                key=absolute_difference_function,
                            )
                            new_air_r = air_r.loc[
                                new_air_thickness, "horizontal"
                            ]
                            nomass.Thermal_Resistance = new_air_r

                            if new_air_thickness <= 0:
                                setattr(
                                    new_mat,
                                    "Thickness",
                                    air_thickness,
                                )
                                remove_layer(c, "RVAL")  # Remove cavity

            # Add as first layer (reversed construction)
            if c.Name == w + "_Rev":
                lista = [l for l in c.obj]
                for el in lista:
                    for nomass in idf.idfobjects["MATERIAL:NOMASS"]:
                        if nomass.Name == el:
                            # Convert air R to air thickness
                            air_thickness = air_r[
                                air_r["horizontal"]
                                == nomass.Thermal_Resistance
                            ].index[
                                0
                            ]  # Thickness values corresponding to R

                            # Insert material layer near cavity layer.
                            air_lay = get_layer(c, nomass.Name)
                            insert_layer(c, air_lay, mat["Name"], below=True)

                            # Compute new air thickness (difference)
                            new_air_thickness = (
                                air_thickness - mat["Thickness"]
                            )

                            # Find which air R is the best for new thickness
                            absolute_difference_function = (
                                lambda list_value: abs(
                                    list_value - new_air_thickness
                                )
                            )
                            new_air_thickness = min(
                                list(air_r.index.values),
                                key=absolute_difference_function,
                            )
                            new_air_r = air_r.loc[
                                new_air_thickness, "horizontal"
                            ]
                            nomass.Thermal_Resistance = new_air_r

                            if new_air_thickness <= 0:
                                setattr(
                                    new_mat,
                                    "Thickness",
                                    air_thickness,
                                )
                                remove_layer(c, "RVAL")  # Remove cavity


def add_insulation_flat_roof(idf, ins_name=None, sheathing_name=None):
    """Add insulation to flat roof, under the sheating layer.

    If no insulation is provided, a default one would be used; if no sheathing
    layer is provided, the function will try to put the insulation under the
    fibreboard if present or under the asphalt.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the roof,
        defaults to None
    :type ins_name: str, optional
    :param sheathing_name: Name of the sheathing layer under which the
        insulation has to be added, defaults to None
    :type sheathing_name: str, optional
    """
    if ins_name == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_name)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Roof"  # Filter only walls
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    # Try to insert insulation under fibreboard layer.
    try:
        sheathing_name = "fibreboard"
        for w in cons_list:
            for c in cons:
                # Add as a layer above fibreboard
                if c.Name == w:
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=True)

                # Add as a layer below fibreboard
                if c.Name == w + "_Rev":
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=False)

    # Insert insulation under asphalt if there is no fibreboard layer.
    except Exception:
        sheathing_name = "asphalt"
        for w in cons_list:
            for c in cons:
                # Add as a layer above asphalt
                if c.Name == w:
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=True)

                # Add as a layer below asphalt
                if c.Name == w + "_Rev":
                    screed_layer = get_layer(c, sheathing_name)
                    insert_layer(c, screed_layer, mat["Name"], below=False)


def add_external_insulation_floor(idf, ins=None):
    """Add insulation on the external layer of all floors in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the floor,
        defaults to None
    :type ins_name: str, optional
    """
    if ins == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Floor"  # Filter only walls
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # Add as last layer
            if c.Name == w:
                last_layer = get_layer(c, "first")
                insert_layer(c, last_layer, mat["Name"], below=False)

            # Add as first layer (reversed construction)
            if c.Name == w + "_Rev":
                first_layer = get_layer(c, "last")
                insert_layer(c, first_layer, mat["Name"], below=True)


def add_internal_insulation_floor(idf, ins_name=None, screed_name="screed"):
    """Add insulation under the screed layer of all floors in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ins_name: Name of the insulation which has to be added to the floor,
        defaults to None
    :type ins_name: str, optional
    :param screed_name: Name of the screed layer used in the floor of the
        building, defaults to "screed"
    :type screed_name: str, optional
    """
    if ins_name == None:
        # TODO: Take default element
        pass

    # Read from database
    d = Database(PATH)
    d.load_file()
    mat = d.get_object("MATERIAL", ins_name)

    # Find BuildingSurface:Detailed floors
    bsd = idf.idfobjects["BUILDINGSURFACE:DETAILED"]
    cons_list = [
        b.Construction_Name
        for b in bsd
        if b.Surface_Type == "Floor"  # Filter only walls
    ]
    cons_list = list(dict.fromkeys(cons_list))  # Remove duplicates

    # Add insulation layer in Construction objects
    cons = idf.idfobjects["CONSTRUCTION"]

    # Add insulation material in Material list
    new_mat = idf.newidfobject("MATERIAL")
    set_fields(new_mat, mat)

    for w in cons_list:
        for c in cons:
            # TODO: What to do if there is no screed layer?
            # Add as a layer above the screed
            if c.Name == w:
                screed_layer = get_layer(c, screed_name)
                insert_layer(c, screed_layer, mat["Name"], below=False)

            # Add as a layer below the screed
            if c.Name == w + "_Rev":
                screed_layer = get_layer(c, screed_name)
                insert_layer(c, screed_layer, mat["Name"], below=True)


def change_internal_heat_gain(idf, value, filter_by="", relative=False):
    """Change internal heat gain.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New value for field "Power per Zone Floor Area" or a
        percentage if `relative` parameter is set to `True`
    :type value: float
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    # TODO: Gestire altri tipi di equipment.
    for oe in idf.idfobjects["OTHEREQUIPMENT"]:
        if filter_by in oe.Name:
            if relative:
                old_value = oe.Power_per_Zone_Floor_Area
                oe.Power_per_Zone_Floor_Area = old_value + old_value * value
            else:
                oe.Power_per_Zone_Floor_Area = value


def change_infiltration(idf, ach, filter_by="", relative=False):
    """Change infiltration ACH for all zones of a specific block of the
    building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ach: New ACH (air changes per hour) value or dictionary of ACH
        values for each zone.
    :type ach: int or dict
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    change_ach(idf, ach, "infiltration", filter_by, relative)


def change_ach(idf, ach, ach_type="ventilation", filter_by="", relative=False):
    """Change ACH for all zones of a specific block of the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param ach: New ACH (air changes per hour) value or dictionary of ACH
        values for each zone.
    :type ach: int or dict
    :param ach_type: ACH type, between `Infiltration` and `Ventilation`
    :type ach_type: str
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    zones = idf.idfobjects["ZONE"]
    if ach_type.lower() == "infiltration":
        z_dfr = idf.idfobjects["ZoneInfiltration:DesignFlowRate".upper()]
    elif ach_type.lower() == "ventilation":
        z_dfr = idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]
    else:
        print("Error")

    if isinstance(ach, dict):
        for zi in z_dfr:
            for z in zones:
                if z.Name in zi.Zone_or_ZoneList_Name and z.Name in ach.keys():
                    zi.Design_Flow_Rate = compute_dfr(z, ach[z.Name])

    else:
        for zi in z_dfr:
            # TODO: Maybe a lower() should be inserted here.
            if filter_by in zi.Zone_or_ZoneList_Name:
                for z in zones:
                    if z.Name == zi.Zone_or_ZoneList_Name:
                        if relative:
                            old_value = zi.Design_Flow_Rate
                            zi.Design_Flow_Rate = old_value + old_value * ach
                        else:
                            zi.Design_Flow_Rate = compute_dfr(z, ach)

    # TODO: Rivedere filter_by in get_mean_ach, forse necessario usare il nome
    #       del blocco contenuto nell'oggetto IDF.
    idf.mean_ach = get_mean_ach(idf, filter_by)


def set_ach_schedule(
    idf, schedule, ach_type="ventilation", filter_by="", **fields
):
    """Set a schedule for ACH ventilation

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param schedule: Schedule which has to be assigned. It follows the rules of
        Schedule:Compact objects of EnergyPlus.
    :type schedule: dict
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param fields: Additional fields to be set, passed as keyword arguments
    """
    if ach_type == "ventilation":
        zv = idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]
    elif ach_type == "infiltration":
        zv = idf.idfobjects["ZoneInfiltration:DesignFlowRate".upper()]

    for zvdfr in zv:
        if filter_by in zvdfr.Zone_or_ZoneList_Name:
            zvdfr.Schedule_Name = schedule["Name"]

            # Set additional fields passed as parameters.
            for k, v in fields.items():
                setattr(zvdfr, k, v)

    sc = idf.newidfobject("Schedule:Compact".upper())
    set_fields(sc, data=schedule)


def compute_dfr(zone, ach):
    """Compute infiltration value given a zone and its ACH.

    :param zone: geomeppy object
    :type zone: class 'geomeppy.patches.EpBunch'
    :param ach: Air changes per hour
    :type ach: int
    :return: Design Flow Rate value
    :rtype: float
    """

    dfr = ach * zone.Volume / 3600
    try:
        if dfr < 0:
            raise Exception("Cannot set ACH lower than 0.")
    except Exception as msg:
        print(msg)
        dfr = 0

    return dfr


def get_ach(zone, dfr):
    """Return ACH from Design Flow Rate

    :param zone: Zone object
    :type zone: [type]
    :param dfr: Design Flow Rate Value
    :type dfr: float
    :return: ACH value for the zone
    :rtype: float
    """
    return dfr * 3600 / zone.Volume


def get_mean_ach(idf, filter_by=""):
    """Compute mean ACH value over all zones of the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :return: Mean ACH value
    :rtype: float
    """
    ach_values = []
    for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if filter_by.lower() in zvdfr.Name.lower():
            for z in idf.idfobjects["ZONE"]:
                if zvdfr.Zone_or_ZoneList_Name == z.Name:
                    ach_values.append(get_ach(z, zvdfr.Design_Flow_Rate))
    return round(np.mean(ach_values), 2)


def get_mean_dt(idf, filter_by=""):
    """Compute mean Delta Temperature over all Zone Ventilations.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :return: Mean Delta Temperature
    :rtype: float
    """
    dt_values = []
    for zvdfr in idf.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if filter_by.lower() in zvdfr.Name.lower():
            dt_values.append(zvdfr.Delta_Temperature)
    return round(np.mean(dt_values), 2)


def change_zone_ach(zone, zone_inf, ach):  # Deprecated
    """Change ACH in a zone by modifying the Design Flow Rate in the
    ZoneInfiltration:DesignFlowRate object.

    :param zone: geomeppy object
    :type zone: class 'geomeppy.patches.EpBunch'
    :param zone_inf: Collection of IDF objects
    :type zone_inf: class 'eppy.idf_msequence.Idf_MSequence'
    """
    zone_name = zone.Name
    for z in zone_inf:
        if z.Zone_or_ZoneList_Name == zone_name:
            z.Design_Flow_Rate = compute_dfr(zone, ach)


def filter_object(obj, key, value):
    """Return object given a field value from a list of objects.

    :param obj: Collection of IDF objects
    :type obj: class 'eppy.idf_msequence.Idf_MSequence'
    :param key: Field name
    :type key: str
    :param value: Field value
    :type value: int or str
    :raises Exception: [description]
    :return: Object from the list
    :rtype: class 'geomeppy.patches.EpBunch'
    """
    # print(key, value)
    retrieved = [o for o in obj if value in getattr(o, key)]
    return retrieved


def convert_keys(data):
    """Convert dictionary keys by replacing spaces with underscores.

    :param data: Dictionary
    :type data: dict
    :return: Dictionary with new keys
    :rtype: dict
    """
    return {x.replace(" ", "_"): y for x, y in data.items()}


def change_setpoint(idf, heat=None, cool=None):
    """Change heating and cooling setpoints in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heat: New heating setpoint value, defaults to None
    :type heat: int, optional
    :param cool: New cooling setpoint value, defaults to None
    :type cool: int, optional
    """
    # Get Schedule:Compact names for heating and cooling setpoint schedules.
    therm = idf.idfobjects["THERMOSTATSETPOINT:DUALSETPOINT"]
    for t in therm:
        heat_sched = getattr(t, "Heating_Setpoint_Temperature_Schedule_Name")
        cool_sched = getattr(t, "Cooling_Setpoint_Temperature_Schedule_Name")

    # Find Schedule:Compact with right names and change setpoints of them.
    schedules = idf.idfobjects["SCHEDULE:COMPACT"]
    for s in schedules:
        if heat_sched in s.Name and heat is not None:
            _change_setpoint(s, heat, which="heating")

        if cool_sched in s.Name and cool is not None:
            _change_setpoint(s, cool, which="cooling")


def _change_setpoint(obj, value, which):
    """Change setpoint of a schedule.

    :param obj: Schedule:Compact object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param value: New setpoint value
    :type value: float
    :param which: Which system, 'heating' or 'cooling'
    :type which: str
    """
    if which == "heating":
        number = 0
        found = 0
        for key in obj.objls:
            if found:
                new_value = int(getattr(obj, key))
                if new_value > number:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if int(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

    elif which == "cooling":
        number = 100
        found = 0
        for key in obj.objls:
            if found:
                new_value = int(getattr(obj, key))
                if new_value < number:
                    number = new_value
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1

        found = 0
        for key in obj.objls:
            if found:
                if int(getattr(obj, key)) == number:
                    setattr(obj, key, value)
                found = 0
            if "Until" in str(getattr(obj, key)):
                found = 1


def add_scheduled_nat_vent(
    idf,
    filter_by="",
    schedule_obj=None,
    ach=None,
    min_ind_temp=None,
    max_ind_temp=None,
    delta_temp=None,
    params={},
):
    """Add scheduled natural ventilation to desired zones.

    Calculation method is supposed to be the IDD default one, Flow/zone.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule_name: Name of the Schedule:Compact construction object to
        be added in IDF, defaults to None
    :type schedule_name: str, optional
    :param ach: Value of air changes per hour, defaults to None
    :type ach: float, optional
    :param min_ind_temp: Minimum indoor temperature setpoint, defaults to None
    :type min_ind_temp: float, optional
    :param max_ind_temp: Maximum indoor temperature setpoint, defaults to None
    :type max_ind_temp: float, optional
    :param delta_temp: Delta temperature for activation, defaults to None
    :type delta_temp: float, optional
    :param params: Additional scheduled natural ventilation parameters for the
        ZoneVentilation:DesignFlowRate object, defaults to {}
    :type params: dict, optional
    """
    if schedule_obj == None:
        # TODO: Take default element
        pass

    # Read from database.
    d = Database(PATH)
    d.load_file()
    sched_nat_vent_obj = d.get_object("SCHEDULE:COMPACT", schedule_obj)

    new_obj = idf.newidfobject("SCHEDULE:COMPACT")
    set_fields(new_obj, sched_nat_vent_obj)

    zones = idf.idfobjects["ZONE"]
    where = [z for z in zones if filter_by in z.Name]
    for zone in where:
        vent_obj = idf.newidfobject("ZoneVentilation:DesignFlowRate".upper())
        ach = ach
        set_fields(
            vent_obj,
            {
                "Name": zone.Name + " Nat Vent",
                "Zone_or_ZoneList_Name": zone.Name,
                "Schedule_Name": schedule_obj,
                "Design_Flow_Rate": compute_dfr(zone, ach),
                "Minimum_Indoor_Temperature": min_ind_temp,
                "Maximum_Outdoor_Temperature": max_ind_temp,
                "Delta_Temperature": delta_temp,
            },
        )
        # Set additional fields
        set_fields(vent_obj, **params)


def add_mechanical_ventilation(idf, filter_by="", schedule=None):
    """Add mechanical ventilation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param schedule: [description], defaults to None
    :type schedule: dict or str, optional
    :raises Warning: If the name of the given schedule is already present in the
        model. In such case, the old schedule is preserved
    :raises TypeError: If schedule type is not `dict` or `str`
    """
    # Add Schedule:Compact object to the IDF if needed.
    found = 0
    if isinstance(schedule, dict):
        try:
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name.lower() == schedule["Name"]:
                    found = 1
                    raise Warning
        except Warning:
            print(
                "Schedule ",
                schedule["Name"],
                " already exists in the IDF.\
                  Given schedule may not be applied.",
            )
        if not found:
            new_sc = idf.newidfobject("SCHEDULE:COMPACT")
            set_fields(new_sc, data=schedule)

    # Update zones
    for z in idf.idfobjects["ZONE"]:
        if filter_by.lower() in z.Name.lower():
            dsoa = idf.newidfobject("DESIGNSPECIFICATION:OUTDOORAIR")
            set_fields(
                dsoa,
                data={
                    "Name": z.Name,
                },
            )
            if isinstance(schedule, dict):
                schedule_name = schedule["Name"]

            elif isinstance(schedule, str):
                schedule_name = schedule

            else:
                raise TypeError("Schedule must be a dict or str")

            set_fields(dsoa, data={"Outdoor Air Schedule Name": schedule_name})

            # Edit HVAC system.
            # TODO: Deal with already existing supplied air.
            for zhec in idf.idfobjects[
                "ZoneHVAC:EquipmentConnections".upper()
            ]:
                if zhec.Zone_Name.lower() == z.Name.lower():
                    inlet = zhec.Zone_Air_Inlet_Node_or_NodeList_Name
                    break  # Found

            for nl in idf.idfobjects["NodeList".upper()]:
                if inlet.lower() == nl.Name.lower():
                    node = nl.Node_1_Name
                    break  # Found

            for zh in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
                if zh.Zone_Supply_Air_Node_Name.lower() == node:
                    zh.Design_Specification_Outdoor_Air_Object_Name = (
                        schedule_name
                    )
                    break


def add_overhangs_simple(idf, extension=1, tilt=90, shift=0.04):
    """Add simple overhang.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension of the overhang, defaults to 1
    :type extension: int, optional
    :param tilt: Tilt of the overhang, defaults to 90
    :type tilt: int, optional
    :param shift: Shift of the overhang, defaults to 0.04
    :type shift: float, optional
    """
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:
        if w.Surface_Type == "Window" or w.Surface_Type == "GlazedDoor":
            # Check if the window does not overlook another room.
            if len(w.Outside_Boundary_Condition_Object) == 0:
                el = idf.newidfobject("SHADING:OVERHANG")
                data = {
                    "Name": str(round(random.random() * 100000)),
                    "Window or Door Name": w.Name,
                    "Left extension from WindowDoor Width": shift,
                    "Right extension from WindowDoor Width": shift,
                    "Height above Window or Door": shift,
                    "Tilt Angle from WindowDoor": tilt,
                    "Depth": extension,
                }
                set_fields(el, data)


def add_overhangs_projection_simple(
    idf, extension_ratio=1, tilt=90, shift=0.04
):
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:
        el = idf.newidfobject("SHADING:OVERHANG:PROJECTION")
        data = {
            "Name": str(round(random.random() * 100000)),
            "Window or Door Name": w.Name,
            "Height above Window or Door": shift,
            "Tilt Angle from WindowDoor": tilt,
            "Left extension from WindowDoor Width": shift,
            "Right extension from WindowDoor Width": shift,
            "Depth as Fraction of WindowDoor Height": extension_ratio,
        }
        set_fields(el, data)


def add_overhangs_complex(
    idf, depth=float, tilt=90, orientation=None, transmittance_schedule=""
):
    # Taken from BESOS (University of Victoria)
    # Potrebbe non funzionare se le finestre sono definite con coordinate
    # diverse.
    """Set the overhang shading on all external windows by creating a
    SHADING:ZONE:DETAILED object which can be viewed using geomeppy's
    view_model() function.

    Note:
    Depth must be greater than 0

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param depth: The depth in meters of the overhang.
    :type depth: float
    :param tilt: Tilt Angle from Window/Door {deg}.
    :type tilt: int
    :param orientation: One of "north", "east", "south", "west". Walls within
        45 degrees will be affected.
    :type orientation: int
    :param transmittance_schedule: Transmittance Schedule Name, defaults to ""
    :type transmittance_schedule: str, optional
    """
    try:
        ggr = idf.idfobjects["GLOBALGEOMETRYRULES"][0]
    except IndexError:
        ggr = None
    # Orientation to degrees.
    orientations = {
        "north": 0.0,
        "east": 90.0,
        "south": 180.0,
        "west": 270.0,
        None: None,
    }
    degrees = orientations.get(orientation, None)
    external_walls = filter(
        lambda x: x.Outside_Boundary_Condition.lower() == "outdoors",
        idf.getsurfaces("wall"),
    )
    external_walls = list(
        filter(
            lambda x: recipes._has_correct_orientation(x, degrees),
            external_walls,
        )
    )
    windows = idf.getsubsurfaces("window")
    for wall in external_walls:

        for window in windows:

            if window.Building_Surface_Name == wall.Name:
                coords = [window.coords[3]]
                x, y, _ = window.coords[0]
                coords.append(
                    (
                        x
                        + depth
                        * sin(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        y
                        + depth
                        * cos(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        window.coords[3][2] - depth * cos(deg2rad(tilt)),
                    )
                )
                x, y, _ = window.coords[1]
                coords.append(
                    (
                        x
                        + depth
                        * sin(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        y
                        + depth
                        * cos(deg2rad(window.azimuth))
                        * sin(deg2rad(tilt)),
                        window.coords[2][2] - depth * cos(deg2rad(tilt)),
                    )
                )
                coords.append(window.coords[2])
                Shade = idf.newidfobject(
                    "SHADING:ZONE:DETAILED",
                    Name="%s - Overhang" % window.Name,
                    Transmittance_Schedule_Name=transmittance_schedule,
                    Base_Surface_Name=window.Building_Surface_Name,
                )
                Shade.setcoords(coords, ggr)


def add_overhangs_detailed(idf, extension=1, shift=0.04):
    """Add shading to all windows in the IDF.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension in meter for the shading, defaults to 1
    :type extension: int, optional
    """
    windows = [w for w in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]]
    for w in windows:
        _add_one_shading(idf, w, extension, shift)


def _add_one_shading(idf, window, extension, shift):
    """Add shading over a single window.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param window: Fenestrationsurface:Detailed object
    :type obj: class 'geomeppy.patches.EpBunch'
    :param extension: Extension in meter for the shading, defaults to 1
    :type extension: int, optional
    """
    # TODO: make sure that the global order of shadings is correct
    # TODO: make sure that the vertex order for each shading is correct
    name_list = [n.Name for n in idf.idfobjects["SHADING:BUILDING:DETAILED"]]
    name = "1"
    available = 0

    if len(name_list) > 0:
        while not available:
            name = str(int(name) + 1)
            if name not in name_list:
                available = 1
    shad = idf.newidfobject("SHADING:BUILDING:DETAILED")

    if max(
        window.Vertex_1_Xcoordinate,
        window.Vertex_2_Xcoordinate,
        window.Vertex_3_Xcoordinate,
        window.Vertex_4_Xcoordinate,
    ) == min(
        window.Vertex_1_Xcoordinate,
        window.Vertex_2_Xcoordinate,
        window.Vertex_3_Xcoordinate,
        window.Vertex_4_Xcoordinate,
    ):
        if window.Vertex_1_Xcoordinate > 0:
            x1 = window.Vertex_1_Xcoordinate
            x2 = window.Vertex_1_Xcoordinate + extension
        else:
            x1 = window.Vertex_1_Xcoordinate
            x2 = window.Vertex_1_Xcoordinate - extension

        y1 = (
            min(
                window.Vertex_1_Ycoordinate,
                window.Vertex_2_Ycoordinate,
                window.Vertex_3_Ycoordinate,
                window.Vertex_4_Ycoordinate,
            )
            - shift
        )
        y2 = (
            max(
                window.Vertex_1_Ycoordinate,
                window.Vertex_2_Ycoordinate,
                window.Vertex_3_Ycoordinate,
                window.Vertex_4_Ycoordinate,
            )
            + shift
        )

    elif max(
        window.Vertex_1_Ycoordinate,
        window.Vertex_2_Ycoordinate,
        window.Vertex_3_Ycoordinate,
        window.Vertex_4_Ycoordinate,
    ) == min(
        window.Vertex_1_Ycoordinate,
        window.Vertex_2_Ycoordinate,
        window.Vertex_3_Ycoordinate,
        window.Vertex_4_Ycoordinate,
    ):
        if window.Vertex_1_Ycoordinate > 0:
            y1 = window.Vertex_1_Ycoordinate
            y2 = window.Vertex_1_Ycoordinate + extension
        else:
            y1 = window.Vertex_1_Ycoordinate
            y2 = window.Vertex_1_Ycoordinate - extension

        x1 = (
            min(
                window.Vertex_1_Xcoordinate,
                window.Vertex_2_Xcoordinate,
                window.Vertex_3_Xcoordinate,
                window.Vertex_4_Xcoordinate,
            )
            - shift
        )
        x2 = (
            max(
                window.Vertex_1_Xcoordinate,
                window.Vertex_2_Xcoordinate,
                window.Vertex_3_Xcoordinate,
                window.Vertex_4_Xcoordinate,
            )
            + shift
        )

    z = (
        max(
            window.Vertex_1_Zcoordinate,
            window.Vertex_2_Zcoordinate,
            window.Vertex_3_Zcoordinate,
            window.Vertex_4_Zcoordinate,
        )
        + shift
    )

    data = {
        "Name": name,
        "Number of Vertices": "4",
        "Vertex 1 Xcoordinate": round(x1, 10),
        "Vertex 1 Ycoordinate": round(y2, 10),
        "Vertex 1 Zcoordinate": round(z, 10),
        "Vertex 2 Xcoordinate": round(x2, 10),
        "Vertex 2 Ycoordinate": round(y2, 10),
        "Vertex 2 Zcoordinate": round(z, 10),
        "Vertex 3 Xcoordinate": round(x2, 10),
        "Vertex 3 Ycoordinate": round(y1, 10),
        "Vertex 3 Zcoordinate": round(z, 10),
        "Vertex 4 Xcoordinate": round(x1, 10),
        "Vertex 4 Ycoordinate": round(y1, 10),
        "Vertex 4 Zcoordinate": round(z, 10),
    }

    set_fields(shad, data=data)


def remove_shading(
    idf,
    which=[
        "Shading:Building:Detailed",
        "Shading:Zone:Detailed",
        "Shading:Overhang",
    ],
):
    """Remove all or specified shading overhangs from the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param which: Specify which overhangs objects have to be removed, defaults
        to ["Shading:Building:Detailed", "Shading:Zone:Detailed",
        "Shading:Overhang"]
    :type which: str or list, optional
    """
    if isinstance(which, list):
        for s in which:
            shadings = idf.idfobjects[s.upper()]
            del shadings[:]

    if isinstance(which, str):
        shadings = idf.idfobjects[which.upper()]
        del shadings[:]


def add_fin_simple(idf, extension=1, shift=0.04):
    """Add simple fin.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param extension: Extension of the fin, defaults to 1
    :type extension: int, optional
    :param shift: Shift of the fin from the window edges, defaults to 0.04
    :type shift: float, optional
    """
    windows = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for w in windows:
        el = idf.newidfobject("SHADING:FIN")
        data = {
            "Name": str(round(random.random() * 100000)),
            "Window or Door Name": w.Name,
            "Left Extension from WindowDoor": shift,
            "Left Distance Above Top of Window": shift,
            "Left Distance Below Bottom of Window": shift,
            "Left Depth": extension,
            "Right Extension from WindowDoor": shift,
            "Right Distance Above Top of Window": shift,
            "Right Distance Below Bottom of Window": shift,
            "Right Depth": extension,
        }
        set_fields(el, data)


def add_blind(
    idf,
    blind_data,
    type="interior",
    filter_by="",
    filter_by_orientation=None,
    schedule=None,
):
    # TODO Allow shading control strategy customization.
    """Add interior or exterior blind to all windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param blind_data: WINDOWMATERIAL:BLIND" JSON data
    :type blind_data: dict
    :param type: Type of blind (interior or exterior), defaults to "interior"
    :type type: str, optional
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param filter_by_orientation: Specify the orientation of the windows to
        which the blind has to be added. Possible values are 0, 45, 90, 135,
        180, 225, 270, 315, 360 (the windows orientation in the building are
        converted to the nearest of these values for simplicity), defaults to
        None
    :type filter_by_orientation: int, optional
    :param schedule: If specified blinds will follow the given schedule,
        otherwise they are supposed to be always on, defaults to None
    :type schedule: dict, optional

    :Example:

    .. code-block:: python

        add_blind(idf,
                  blind_data={
                      "Name": "ext_blind",
                      "Slat Width": 0.025,
                      "Slat Separation": 0.01875,
                      "Slat Thickness": 0.001,
                      "Slat Conductivity": 0.9,
                      "Front Side Slat Beam Solar Reflectance": 0.8,
                      "Back Side Slat Beam Solar Reflectance": 0.8,
                      "Front Side Slat Diffuse Solar Reflectance": 0.8,
                      "Back Side Slat Diffuse Solar Reflectance": 0.8,
                      "Slat Beam Visible Transmittance": 0,
                      "Front Side Slat Beam Visible Reflectance": 0.8,
                      "Back Side Slat Beam Visible Reflectance": 0.8,
                      "Front Side Slat Diffuse Visible Reflectance": 0.8,
                      "Back Side Slat Diffuse Visible Reflectance": 0.8,
                      "Blind to Glass Distance": 0.015,
                      "Blind Bottom Opening Multiplier": 0.5
                  },
                  type="exterior",
                  filter_by="main_block"
                  )

    """
    # Possible values for orientation (azimuth) of windows.
    possible_values = [0, 45, 90, 135, 180, 225, 270, 315, 360]
    shaded = "_shaded_" + str(uuid.uuid4()).replace("-", "")[0:4]
    control = "_control_" + str(uuid.uuid4()).replace("-", "")[0:4]
    # Add shaded construction object by copying window constructions.
    construction_list = []
    for f in idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]:
        o = possible_values[
            min(
                range(len(possible_values)),
                key=lambda i: abs(possible_values[i] - f.azimuth),
            )
        ]
        if o == 360:
            o = 0
        if filter_by_orientation == 360:
            filter_by_orientation = 0
        # Check if:
        #   - the window type is Window or GlazedDoor
        #   - the window does not overlook another room
        #   - the zone name is included in filter_by
        if (
            (f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor")
            and len(f.Outside_Boundary_Condition_Object) == 0
            and filter_by.lower() in f.Name.lower()
        ):
            if filter_by_orientation == None or o == filter_by_orientation:
                construction_list.append(f.Construction_Name)

                # Change Shading_Control_Name according to ShadingControl name.
                setattr(
                    f,
                    "Shading_Control_Name",
                    f.Construction_Name + shaded + control,
                )

    construction_list = list(set(construction_list))  # Remove duplicates

    # Transform construction names into real construction objects.
    construction_list = [
        c
        for c in idf.idfobjects["CONSTRUCTION"]
        if c.Name in construction_list
    ]

    # Create WindowMaterial:Blind object.
    try:
        for wmb in idf.idfobjects["WINDOWMATERIAL:BLIND"]:
            if wmb.Name == blind_data["Name"]:
                raise ValueError(
                    "Blind material with specified name is already present in the model"
                )
    except ValueError as e:
        print(e)
        print("Assigning a new name to the material")
        blind_data["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]
    blind = idf.newidfobject("WINDOWMATERIAL:BLIND")
    set_fields(blind, blind_data)

    for con in construction_list:
        new_con = idf.copyidfobject(con)
        con_name = getattr(con, "Name")
        con_name += shaded
        setattr(new_con, "Name", con_name)

        # Interior: add layer as last (inner) layer.
        if type == "interior":
            shad_type = "InteriorBlind"
            last_layer = get_layer(new_con, "last")
            insert_layer(new_con, last_layer, blind_data["Name"], below=True)

        # Exterior: add layer as first (outer) layer.
        elif type == "exterior":
            shad_type = "ExteriorBlind"
            first_layer = get_layer(new_con, "first")
            insert_layer(new_con, first_layer, blind_data["Name"], below=False)

        # Create Windowproperty:ShadingControl object.
        wpsc = idf.newidfobject("WINDOWPROPERTY:SHADINGCONTROL")
        data = {
            "Name": con_name + control,
            "Shading Type": shad_type,  # InteriorBlind/ExteriorBlind
            "Construction with Shading Name": con_name,  # Automatically assigned
            "Shading Control Type": "AlwaysOn",
        }
        set_fields(wpsc, data=data)

    # If schedule is provided, create a new schedule object and assign it
    # to the shading control.
    if schedule is not None:
        try:
            for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name == schedule["Name"]:
                    raise ValueError(
                        "Schedule with specified name is already present in the model"
                    )
        except ValueError as e:
            print(e)
            schedule["Name"] += str(uuid.uuid4()).replace("-", "")[0:6]

        sc = idf.newidfobject("SCHEDULE:COMPACT")
        set_fields(sc, schedule)
        set_fields(
            wpsc,
            data={
                "Schedule Name": schedule["Name"],
                "Shading Control Type": "OnIfScheduleAllows",
                "Shading Control Is Scheduled": "Yes",
            },
        )


def set_shading_control(idf, name, data=None):
    fsd = idf.idfobjects["FENESTRATIONSURFACE:DETAILED"]
    for f in fsd:
        if f.Surface_Type == "Window" or f.Surface_Type == "GlazedDoor":
            # Check if the window does not overlook another room.
            if len(f.Outside_Boundary_Condition_Object) == 0:
                f.Shading_Control_Name = name
    if data is not None:
        sc = idf.newidfobject("WINDOWPROPERTY:SHADINGCONTROL")
        set_fields(sc, data)


def set_block_beam_solar(idf):
    """Set BlockBeamSolar to the shading control.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    """
    wps = idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]
    wps_data = {
        "Type_of_Slat_Angle_Control_for_Blinds": "BlockBeamSolar",
        "Slat_Angle_Schedule_Name": "",
    }
    for w in wps:
        set_fields(w, data=wps_data)


def add_slat_angle_control(idf, angle, setpoint):
    """Set shading control to fixed angle.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param angle: Angle of shading to be applied
    :type angle: float
    :param setpoint: The setpoint for activating window shading
    :type setpoint: float
    """
    for s in idf.idfobjects["SCHEDULE:COMPACT"]:
        if getattr(s, "Name") == "Slat angle control":
            del s

    # Create Schedule:Compact object
    sac = idf.newidfobject("SCHEDULE:COMPACT")
    data = {
        "Name": "Slat angle control",
        "Schedule Type Limits Name": "Any Number",
        "Field 1": "Through: 12/3",
        "Field 2": "For: AllDays",
        "Field 3": "Until: 24:00",
        "Field 4": str(angle),
    }
    set_fields(sac, data=data)

    # Set fields in WindowProperty:ShadingControl object
    wps = idf.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]
    wps_data = {
        "Shading Control Type": "OnIfHighHorizontalSolar",
        "Setpoint": float(setpoint),
        "Type of Slat Angle Control for Blinds": "ScheduledSlatAngle",
        "Slat Angle Schedule Name": "Slat angle control",
        "Setpoint 2": 0.0,  # 0 because of OnIfHighHorizontalSolar type
    }
    for w in wps:
        set_fields(w, data=wps_data)


def change_opening_factor(idf, value):
    """Change opening factor.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New opening factor value
    :type value: float or str
    """
    amcd = idf.idfobjects["AIRFLOWNETWORK:MULTIZONE:COMPONENT:DETAILEDOPENING"]
    for a in amcd:
        num = getattr(a, "Number_of_Sets_of_Opening_Factor_Data")
        for n in range(1, num + 1):
            data = {
                "Height Factor for Opening Factor " + str(n): value,
                "Start Height Factor for Opening Factor "
                + str(n): round(1 - value, 2),
            }
            set_fields(a, data)


def change_supply_air_temperature(idf, which, value):
    if which == "heating":
        for z in idf.idfobjects["SIZING:ZONE"]:
            set_fields(
                z, {"Zone Heating Design Supply Air Temperature": value}
            )
        for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
            set_fields(i, {"Maximum Heating Supply Air Temperature": value})
    elif which == "cooling":
        for z in idf.idfobjects["SIZING:ZONE"]:
            set_fields(
                z, {"Zone Cooling Design Supply Air Temperature": value}
            )
        for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
            set_fields(i, {"Minimum Cooling Supply Air Temperature": value})


def change_heating_supply_air_humidity_ratio(idf, value):
    # TODO: Gestire insieme anche cooling (?)
    """Change heating supply air humidity ratio in the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New Heating Design Supply Air Humidity Ratio value
    :type value: float
    """
    for z in idf.idfobjects["SIZING:ZONE"]:
        set_fields(z, {"Zone Heating Design Supply Air Humidity Ratio": value})
    for i in idf.idfobjects["ZONEHVAC:IDEALLOADSAIRSYSTEM"]:
        set_fields(i, {"Maximum Heating Supply Air Humidity Ratio": value})


def change_hvac_limits(idf, heating=None, cooling=None):
    """Change HVAC heating and cooling limits.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heating: New heating limit, defaults to None
    :type heating: str, optional
    :param cooling: New cooling limit, defaults to None
    :type cooling: str, optional
    :raises Exception: [description]
    :raises Exception: [description]
    """
    limit_list = [
        "NoLimit",
        "LimitFlowRate",
        "LimitCapacity",
        "LimitFlowRateAndCapacity",
    ]
    try:
        if heating not in limit_list:
            raise Exception
        if cooling not in limit_list:
            raise Exception
        for hvac in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem"]:
            if heating is not None:
                set_fields(hvac, {"Heating Limit": heating})
            if cooling is not None:
                set_fields(hvac, {"Cooling Limit": cooling})
    except Exception:
        print("Limit type is not valid")


def change_dhw_temperature(idf, supply=None, mains=None):
    """Change DHW Supply and DHW Mains temperatures.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param supply: Value of DHW supply temperature, defaults to None
    :type supply: int or str, optional
    :param mains: Value of DHW mains temperature, defaults to None
    :type mains: int or str, optional
    """
    schedules = idf.idfobjects["SCHEDULE:COMPACT"]
    dhw = idf.idfobjects["WATERUSE:EQUIPMENT"]

    # Read DHW schedule names in the WaterUse:Equipment object
    for d in dhw:
        dhw_supply = getattr(d, "Target_Temperature_Schedule_Name")
        dhw_mains = getattr(d, "Cold_Water_Supply_Temperature_Schedule_Name")

        # For each Schedule:Compact, search DHW Supply and DHW Mains
        for s in schedules:
            if dhw_supply in s.Name and supply is not None:
                found = 0
                for key in s.objls:
                    if found == 1:
                        setattr(s, key, str(supply))
                        found = 0
                    try:
                        if "Until" in getattr(s, key):
                            found = 1
                    except Exception:
                        pass

            if dhw_mains in s.Name and mains is not None:
                found = 0
                for key in s.objls:
                    if found == 1:
                        setattr(s, key, str(mains))
                        found = 0
                    try:
                        if "Until" in getattr(s, key):
                            found = 1
                    except Exception:
                        pass


def change_occupancy(idf, value, filter_by="", relative=False):
    """Change People per Zone Floor Area field with a new value.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New value for the field
    :type value: float
    :param filter_by: Filter zone by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    :param relative: Specify if new value will be asssigned as a percentage
        increment from the old value, defaults to `False`
    :type relative: bool, optional
    """
    for p in idf.idfobjects["PEOPLE"]:
        if relative:
            old_value = getattr(p, "People_per_Zone_Floor_Area")
            new_value = old_value + old_value * value
            setattr(p, "People_per_Zone_Floor_Area", new_value)
        else:
            if filter_by in p.Name:
                setattr(p, "People_per_Zone_Floor_Area", value)


def get_area(idf, name):
    """Compute and return area of the building by filtering zone names.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param name: Name of the zones
    :type name: str
    :return: Area
    :rtype: float
    """
    home_area = 0
    for zone in idf.idfobjects["ZONE"]:
        if name.lower() in zone.Name.lower():
            home_area += zone.Floor_Area
    return home_area


def get_volume(idf, name):
    """Compute and return volume of the building by filtering zone names.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param name: Name of the zones
    :type name: str
    :return: Volume
    :rtype: float
    """
    home_volume = 0
    for zone in idf.idfobjects["ZONE"]:
        if name.lower() in zone.Name.lower():
            home_volume += zone.Volume
    return home_volume


def set_epw(idf, epw_file):
    """Set EPW file in the IDF

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param epw_file: EPW filename
    :type epw_file: path or str
    """
    idf.epw = epw_file


def get_hvac_elements(idf):
    """Get all HVAC elements that are present the building.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :return: List of HVAC objects
    :rtype: list
    """
    obj_list = [
        "Sizing:Zone",
        "DesignSpecification:ZoneAirDistribution",
        "ZoneControl:Thermostat",
        "ThermostatSetpoint:DualSetpoint",
        "ZoneHVAC:EquipmentConnections",
        "ZoneHVAC:EquipmentList",
        "ZoneHVAC:IdealLoadsAirSystem",
        "NodeList",
    ]
    hvac_ojects = [idf.idfobjects[el.upper()] for el in obj_list]
    hvac_ojects = [o for el in hvac_ojects for o in el]
    return hvac_ojects


def add_hvac(idf, heating_schedule=None, cooling_schedule=None, filter_by=""):
    """Add HVAC to a building without HVAC.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param heating_schedule: Heating schedule, defaults to None
    :type heating_schedule: dict, optional
    :param cooling_schedule: Cooling schedule, defaults to None
    :type cooling_schedule: dict, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to ""
    :type filter_by: str, optional
    """
    if len(get_hvac_elements(idf)) > 0:
        raise Exception("The building already has an HVAC system.")

    zones = [z for z in idf.idfobjects["ZONE"] if filter_by in z.Name]
    for z in zones:
        add_zone_hvac(
            idf,
            z,
            heating_schedule=heating_schedule,
            cooling_schedule=cooling_schedule,
        )


def add_zone_hvac(idf, zone, heating_schedule=None, cooling_schedule=None):
    """Add HVAC to a zone without HVAC.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param zone: [description]
    :type zone: [type]
    :param heating_schedule: Heating schedule, defaults to None
    :type heating_schedule: dict, optional
    :param cooling_schedule: Cooling schedule, defaults to None
    :type cooling_schedule: dict, optional
    """
    # Validation non needed maybe.
    # if heating_schedule == None and cooling_schedule == None:
    #     raise Exception("At least one schedule must be provided")

    # Heating.
    if heating_schedule is not None:
        heat_is_on = 1
        heating_schedule["Name"] = zone.Name + " Heating Setpoint Schedule"
    else:
        heat_is_on = 0
        heating_schedule = {
            "Name": zone.Name + " Heating Setpoint Schedule",
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": -50,
        }
    hsc = idf.newidfobject("SCHEDULE:COMPACT")
    set_fields(hsc, data=heating_schedule)

    # Create availability heating schedule from setpoint schedule.
    havail = idf.newidfobject("SCHEDULE:COMPACT")
    heat_avail = {
        k: 0 if v == -50 else 1 if isinstance(v, int) else v
        for k, v in heating_schedule.items()
    }
    heat_avail["Name"] = zone.Name + " Heating Availability Sch"
    set_fields(havail, data=heat_avail)

    # Cooling.
    if cooling_schedule is not None:
        cool_is_on = 1
        cooling_schedule["Name"] = zone.Name + " Cooling SP Sch"
    else:
        cool_is_on = 0
        cooling_schedule = {
            "Name": zone.Name + " Heating Setpoint Schedule",
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": 100,
        }
    csc = idf.newidfobject("SCHEDULE:COMPACT")
    set_fields(csc, data=cooling_schedule)

    # Create availability cooling schedule from setpoint schedule.
    cavail = idf.newidfobject("SCHEDULE:COMPACT")
    cool_avail = {
        k: 0 if v == 100 else 1 if isinstance(v, int) else v
        for k, v in cooling_schedule.items()
    }
    cool_avail["Name"] = zone.Name + " Cooling Availability Sch"
    set_fields(cavail, data=cool_avail)

    # Sizing:Zone
    sz = idf.newidfobject("SIZING:ZONE")
    set_fields(
        sz,
        data={
            "Zone or ZoneList Name": zone.Name,
            "Zone Cooling Design Supply Air Temperature": 12,
            "Zone Cooling Design Supply Air Temperature Difference": 0,
            "Zone Heating Design Supply Air Temperature": 35,
            "Zone Heating Design Supply Air Temperature Difference": 0,
            "Zone Cooling Design Supply Air Humidity Ratio": 0.0077,  # Default
            "Zone Heating Design Supply Air Humidity Ratio": 0.0156,  # Default
            "Zone Heating Sizing Factor": 1.25,  # Recommended value
            "Zone Cooling Sizing Factor": 1.15,  # Recommended value
            "Cooling Design Air Flow Rate": 0,
            "Cooling Minimum Air Flow per Zone Floor Area": 0.000762,  # Default
            "Cooling Minimum Air Flow": 0,
            "Cooling Minimum Air Flow Fraction": 0.2,
            "Heating Maximum Air Flow per Zone Floor Area": 0.002032,  # Default
            "Heating Maximum Air Flow": 0.1415762,  # Default
            "Heating Maximum Air Flow Fraction": 0.3,  # Default
            "Design Specification Zone Air Distribution Object Name": zone.Name,
        },
    )

    # DesignSpecification:ZoneAirDistribution
    dszad = idf.newidfobject("DesignSpecification:ZoneAirDistribution".upper())
    set_fields(dszad, data={"Name": zone.Name})

    # ZoneControl:Thermostat
    zct = idf.newidfobject("ZoneControl:Thermostat".upper())
    set_fields(
        zct,
        data={
            "Name": zone.Name + " Thermostat",
            "Zone or ZoneList Name": zone.Name,
            "Control Type Schedule Name": "Zone Control Type Sched",  # Check
            "Control 1 Object Type": "ThermostatSetpoint:DualSetpoint",
            "Control 1 Name": "Dual Setpoint - Zone " + zone.Name,
        },
    )

    # ThermostatSetpoint:DualSetpoint
    tssd = idf.newidfobject("ThermostatSetpoint:DualSetpoint".upper())
    set_fields(
        tssd,
        data={
            "Name": "Dual Setpoint - Zone " + zone.Name,
            "Heating Setpoint Temperature Schedule Name": heating_schedule[
                "Name"
            ],
            "Cooling Setpoint Temperature Schedule Name": cooling_schedule[
                "Name"
            ],
        },
    )

    # ZoneHVAC:EquipmentConnections
    eq = idf.newidfobject("ZoneHVAC:EquipmentConnections".upper())
    set_fields(
        eq,
        data={
            "Zone Name": zone.Name,
            "Zone Conditioning Equipment List Name": zone.Name + " Equipment",
            "Zone Air Inlet Node or NodeList Name": zone.Name + " Inlets",
            "Zone Air Node Name": "Node " + zone.Name + " Zone",
            "Zone Return Air Node or NodeList Name": "Node "
            + zone.Name
            + " Out",
        },
    )

    # ZoneHVAC:EquipmentList
    el = idf.newidfobject("ZoneHVAC:EquipmentList".upper())
    set_fields(
        el,
        data={
            "Name": zone.Name + " Equipment",
            "Zone Equipment 1 Object Type": "ZoneHVAC:IdealLoadsAirSystem",
            "Zone Equipment 1 Name": zone.Name + " Ideal Loads Air",
            "Zone Equipment 1 Cooling Sequence": 1,
            "Zone Equipment 1 Heating or NoLoad Sequence": 1,
        },
    )

    # ZoneHVAC:IdealLoadsAirSystem
    il = idf.newidfobject("ZoneHVAC:IdealLoadsAirSystem".upper())
    set_fields(
        il,
        data={
            "Name": zone.Name + " Ideal Loads Air",
            "Zone Supply Air Node Name": "Node " + zone.Name + " In",
            "Maximum Heating Supply Air Temperature": 35,
            "Minimum Cooling Supply Air Temperature": 12,
            "Heating Availability Schedule Name": zone.Name
            + " Heating Availability Sch",
            "Cooling Availability Schedule Name": zone.Name
            + " Cooling Availability Sch",
            # "Design Specification Outdoor Air Object Name": zone.Name,
        },
    )
    if heat_is_on:  # Only if heating is active.
        set_fields(
            il,
            data={
                "Heating Limit": "LimitFlowRateAndCapacity",
                "Maximum Heating Air Flow Rate": "Autosize",
                "Maximum Sensible Heating Capacity": "Autosize",
            },
        )
    else:  # If heating is not active, set flow rate to 0.
        set_fields(
            il, data={"Heating Limit": "", "Maximum Heating Air Flow Rate": 0}
        )

    if cool_is_on:  # Only if cooling is active.
        set_fields(
            il,
            data={
                "Cooling Limit": "LimitFlowRateAndCapacity",
                "Maximum Cooling Air Flow Rate": "Autosize",
                "Maximum Total Cooling Capacity": "Autosize",
            },
        )
    else:  # If cooling is not active, set flow rate to 0.
        set_fields(
            il, data={"Cooling Limit": "", "Maximum Cooling Air Flow Rate": 0}
        )

    # NodeList
    nl = idf.newidfobject("NodeList".upper())
    set_fields(
        nl,
        data={
            "Name": zone.Name + " Inlets",
            "Node 1 Name": "Node " + zone.Name + " In",
        },
    )


def activate_cooling(
    idf,
    cooling_schedule,
    cool_sch_name="Cooling SP Sch",
    cool_avail_name="Cooling Availability Sch",
    filter_by="",
):
    """Activate cooling on the building. If the building does not have an HVAC
    system, the entire system is added.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param cooling_schedule: New schedule for the cooling system.
    :type cooling_schedule: dict
    :param cool_sch_name: Name of the cooling setpoint schedule compact which
        has to be replaced, defaults to "Cooling SP Sch"
    :type cool_sch_name: str, optional
    :param cool_avail_name: Name of the cooling availability schedule compact
        which has to be replaced, defaults to "Cooling Availability Sch"
    :type cool_avail_name: str, optional
    :param filter_by: Filter zones by name. Can be the block name or specific
        zone name, defaults to "", ignored if ach is a `dict`
    :type filter_by: str, optional
    """
    # Check if the building already has an HVAC system, otherwise add it.
    if len(get_hvac_elements(idf)) == 0:
        add_hvac(idf, filter_by=filter_by)

    # Activate cooling in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        if filter_by.lower() in ilas.Zone_Supply_Air_Node_Name.lower():
            set_fields(
                ilas,
                data={
                    "Cooling Limit": "LimitFlowRateAndCapacity",
                    "Maximum Cooling Air Flow Rate": "Autosize",
                    "Maximum Total Cooling Capacity": "Autosize",
                },
            )
    cool_avail = {
        k: 0 if v == 100 else 1 if isinstance(v, int) else v
        for k, v in cooling_schedule.items()
    }
    # Update cooling schedules.
    # FIXME: If the already existing cooling schedule has more fields than
    #        the new one, it causes problems. See possible fix below.
    del cooling_schedule["Name"]  # To prevent name overwriting.
    del cool_avail["Name"]  # To prevent name overwriting.
    for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
        if cool_sch_name.lower() in sc.Name.lower():
            set_fields(sc, data=cooling_schedule)
        if cool_avail_name.lower() in sc.Name.lower():
            set_fields(sc, data=cool_avail)
    # TODO: Test this fix:
    # for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
    #     if cool_sch_name.lower() in sc.Name.lower():
    #         cooling_schedule.update({"Name": sc.Name})
    #         idf.removeidfobject(sc)
    #         sc = idf.newidfobject("SCHEDULE:COMPACT")
    #         set_fields(sc, data=cooling_schedule)
    #     elif cool_avail_name.lower() in sc.Name.lower():
    #         cool_avail.update({"Name": sc.Name})
    #         idf.removeidfobject(sc)
    #         sc = idf.newidfobject("SCHEDULE:COMPACT")
    #         set_fields(sc, data=cool_avail)


def deactivate_cooling(
    idf,
    cool_sch_name="Cooling SP Sch",
    cool_avail_name="Cooling Availability Sch",
    temp=None,
):
    # TODO: Aggiungere filter_by per agire su singole zone.
    """Deactivate cooling on already existing HVAC system.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param cool_sch_name: Name of the cooling setpoint schedule compact which
        has to be replaced, defaults to "Cooling SP Sch"
    :type cool_sch_name: str, optional
    :param cool_avail_name: Name of the cooling availability schedule compact
        which has to be replaced, defaults to "Cooling Availability Sch"
    :type cool_avail_name: str, optional
    :param temp: New value for maximum outdoor temperature. If None, no changes
        are applied to such parameter, defaults to None
    :type temp: int or float, optional
    """
    # Deactivate cooling in ZoneHVAC:IdealLoadsAirSystem.
    for ilas in idf.idfobjects["ZoneHVAC:IdealLoadsAirSystem".upper()]:
        set_fields(
            ilas,
            data={
                "Cooling Limit": "",
                "Maximum Cooling Air Flow Rate": 0,
                "Maximum Total Cooling Capacity": "",
            },
        )
        cooling_schedule = {
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until: 24:00",
            "Field_4": 100,
        }
        # NOTE: Can be removed
        # empty_fields = {"Field_" + str(x): "" for x in range(5, 100)}
        # cooling_schedule.update(empty_fields)

        cool_avail = {
            k: 0 if v == 100 else 1 if isinstance(v, int) else v
            for k, v in cooling_schedule.items()
        }
        # Update cooling schedules.
        for sc in idf.idfobjects["SCHEDULE:COMPACT"]:
            if cool_sch_name.lower() in sc.Name.lower():
                cooling_schedule.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=cooling_schedule)
            elif cool_avail_name.lower() in sc.Name.lower():
                cool_avail.update({"Name": sc.Name})
                idf.removeidfobject(sc)
                sc = idf.newidfobject("SCHEDULE:COMPACT")
                set_fields(sc, data=cool_avail)

        if temp is not None:
            for dfr in idf.idfobjects["ZONEVENTILATION:DESIGNFLOWRATE"]:
                set_fields(dfr, {"Maximum Outdoor Temperature": temp})


def change_window_opening(idf, value):
    """Change window opening factor in a building with controlled natural
    ventilation already active.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New percentage value, for example 0.3 or 30 will set the
        opening to 30 %
    :type value: int or float
    :raises ValueError: If specified value is not between 0 and 100.
    """
    if (value <= 0) or (value > 100):
        raise ValueError("Opening value must be between 0 and 100")
    if value > 1:
        value /= 100
    for do in idf.idfobjects[
        "AirflowNetwork:MultiZone:Component:DetailedOpening".upper()
    ]:
        data = {}
        num = do["Number of Sets of Opening Factor Data".replace(" ", "_")]
        for n in range(1, num + 1):
            entry = {
                "Height Factor for Opening Factor X".replace(
                    "X", str(n)
                ): value,
                "Start Height Factor for Opening Factor X".replace(
                    "X", str(n)
                ): round(1 - value, 2),
            }
            data.update(entry)
        set_fields(do, data=data)


def change_cp(idf, data):
    """Change Cp coefficients.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param data: Dictionary where keys are wind angles and values are Cp
        coefficients
    :type data: dict
    """
    # TODO: Implement a way to reset all angles and coefficients if necessary.
    angles = idf.idfobjects[
        "AirflowNetwork:MultiZone:WindPressureCoefficientArray".upper()
    ][0]
    for k, v in data.items():
        if float(k) not in angles.obj:
            angles.obj.append(float(k))

        for ang in angles.objls:
            if getattr(angles, ang) == float(k):
                number = ang[-1]
                for wpcv in idf.idfobjects[
                    "AirflowNetwork:MultiZone:WindPressureCoefficientValues".upper()
                ]:
                    set_fields(
                        wpcv,
                        data={"Wind Pressure Coefficient Value " + number: v},
                    )


def change_cd(idf, value):
    """Change window discharge coefficient in a building with controlled
    natural ventilation already active.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param value: New percentage value, for example 0.3 or 30 will set the
        opening to 30 %
    :type value: int or float
    :raises ValueError: If specified value is not between 0 and 100.
    """
    if (value <= 0) or (value > 100):
        raise ValueError("Cd value must be between 0 and 100")
    if value > 1:
        value /= 100
    for do in idf.idfobjects[
        "AirflowNetwork:MultiZone:Component:DetailedOpening".upper()
    ]:
        data = {}
        num = do["Number of Sets of Opening Factor Data".replace(" ", "_")]
        for n in range(1, num + 1):
            entry = {
                "Discharge Coefficient for Opening Factor X".replace(
                    "X", str(n)
                ): value
            }
            data.update(entry)
        set_fields(do, data=data)


def change_runperiod(idf, start, end, fmt="%d-%m"):
    """Change simulation RunPeriod according to provided dates.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param start: Start date
    :type start: str
    :param end: End date
    :type end: str
    :param fmt: Format of the date, defaults to "%d-%m"
    :type fmt: str, optional
    """
    start = datetime.datetime.strptime(start, fmt)
    end = datetime.datetime.strptime(end, fmt)
    begin_m, begin_d = start.month, start.day
    end_m, end_d = end.month, end.day
    data = {
        "Begin Month": begin_m,
        "Begin Day of Month": begin_d,
        "End Month": end_m,
        "End Day of Month": end_d,
    }
    set_fields(idf.idfobjects["RUNPERIOD"][0], data=data)


def set_daylight_saving(
    idf,
    status="Yes",
    start="Last Sunday in March",
    end="Last Sunday in October",
):
    """Activate daylight saving time in the IDF.
    Start date and end date can also be provided.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param status: 'Yes' or 'No', defaults to 'Yes'
    :type status: str, optional
    :param start: Start date, defaults to "Last Sunday in March"
    :type start: str, optional
    :param end: End date, defaults to "Last Sunday in October"
    :type end: str, optional
    """
    set_fields(
        idf.idfobjects["RunPeriodControl:DaylightSavingTime".upper()][0],
        data={"Start Date": start, "End Date": end},
    )
    set_fields(
        idf.idfobjects["RUNPERIOD"][0],
        data={"Use Weather File Daylight Saving Period": status},
    )


def set_outputs(
    idf,
    variable_list,
    key_value="*",
    frequency="Timestep",
    schedule_name="On",
    reset=False,
):
    """Edit EnergyPlus outputs.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param variable_list: List of output variables to add
    :type variable_list: list
    :param key_value: Key value for added variables, defaults to "*"
    :type key_value: str, optional
    :param frequency: Reporting frequency for added variables, defaults to
        "Timestep"
    :type frequency: str, optional
    :param schedule_name: [Description], defaults to "On"
    :type schedule_name: str, optional
    :param reset: If set to `True`, all existing output variables are deleted,
        defaults to False
    :type reset: bool, optional

    :Example:

    .. code-block:: python

        set_outputs(idf,
                    ["Zone Mean Air Temperature",
                    "Site Outdoor Air Drybulb Temperature",
                    "Zone Operative Temperature"],
                    key_value="Block1",
                    reset=True)

    """
    # Reset all variable outputs.
    if reset:
        del idf.idfobjects["OUTPUT:VARIABLE"][:]
        del idf.idfobjects["OUTPUT:METER"][:]
        del idf.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]

    # Add output variables:
    for var in variable_list:
        if var is not None:
            ov = idf.newidfobject("OUTPUT:VARIABLE")
            set_fields(
                ov,
                data={
                    "Key Value": key_value,
                    "Variable Name": var,
                    "Reporting Frequency": frequency,
                },
            )
            if schedule_name is not None:
                set_fields(ov, Schedule_Name=schedule_name)

    # Add frequency for DistrictHeating and DistrictCooling.
    found = 0
    for eif in idf.idfobjects["Output:EnvironmentalImpactFactors".upper()]:
        if eif.Reporting_Frequency == frequency:
            found = 1
    if not found:
        new_eif = idf.newidfobject("Output:EnvironmentalImpactFactors".upper())
        set_fields(new_eif, Reporting_Frequency=frequency)


def set_ems_constants(idf, x, y, z):
    sr = idf.newidfobject("EnergyManagementSystem:Subroutine".upper())
    set_fields(
        sr,
        data={
            "Name": "Set_Globals",
            "Program Line 1": "SET X = %s" % x,
            "Program Line 2": "SET Y = %s" % y,
            "Program Line 3": "SET Z = %s" % z,
        },
    )


def edit_csv_schedule(
    idf,
    csv,
    hours,
    columns,
    value,
    sep=";",
    index_col=0,
    save=True,
    fix_paths=True,
):
    """Edit a CSV representing a Schedule:File object of EnergyPlus.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param csv: CSV file
    :type csv: str or class:`pandas.core.frame.DataFrame`
    :param hours: Hours of CSV file to be changed
    :type hours: int or list
    :param columns: Columns of the CSV file to be changed
    :type columns: list
    :param value: Value to be assigned
    :type value: float
    :param sep: Separator of the CSV file, defaults to ";"
    :type sep: str, optional
    :param index_col: Index column number, defaults to 0
    :type index_col: int, optional
    :param save: Save modified CSV to file, defaults to True
    :type save: bool, optional
    :param fix_paths: Convert CSV file paths in IDF to point at the new file,
        defaults to True
    :type fix_paths: bool, optional
    :raises Exception: [description]
    :raises NotImplementedError: [description]
    :raises Exception: [description]
    :return: DataFrame of the CSV file
    :rtype: class:`predyce.IDF_class.IDF`
    """
    if isinstance(csv, str):
        df = pd.read_csv(csv, sep=sep, index_col=index_col)
    elif isinstance(csv, pd.core.frame.DataFrame):
        df = csv
    if len(df) == 8760:
        df.set_index(
            pd.date_range(
                start="01-JAN-2021 00:00", end="31-DEC-2021 23:50", freq="H"
            ),
            drop=True,
            inplace=True,
        )
    elif len(df) == 8784:
        df.set_index(
            pd.date_range(
                start="01-JAN-2020 00:00", end="31-DEC-2020 23:50", freq="H"
            ),
            drop=True,
            inplace=True,
        )
    else:
        raise Exception(
            "CSV file for Schedule:File must contain 8760 or 8784 rows"
        )

    if isinstance(hours, np.float64):
        hours = int(hours)

    if isinstance(hours, (int, list)):
        df.loc[df.index[hours], columns] = value
    elif isinstance(hours, str):
        raise NotImplementedError("Parsing datetime is not implemented yet.")
    else:
        raise Exception("Error in editing CSV file: incompatible hour type.")

    if isinstance(csv, str) and save == True:
        df.to_csv(Path(csv).name, sep=";")

    if fix_paths == True:
        for sf in idf.idfobjects["SCHEDULE:FILE"]:
            if Path(csv).name in sf.File_Name:
                sf.File_Name = os.path.abspath(Path(csv).name)

    return df
