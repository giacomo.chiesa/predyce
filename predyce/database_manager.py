#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to manage a JSON database"""
import json as js


class Database:
    """Class to manage JSON database."""

    def __init__(self, path):
        """
        :param path: Database path
        :type path: Path or str
        """
        self.database = {}
        self.path = path
        self.text = ""

    def load_file(self):
        """Load database in local memory.

        :param path: Database path
        :type path: str
        """
        f = open(self.path, "r")
        self.text = f.read()
        self.database = js.loads(self.text)
        f.close()
        return

    def get_object(self, obj_type, obj_name):
        """Retrieve database object.

        :param obj_type: Database object key
        :type obj_type: str
        :param obj_name: Database object name
        :type obj_name: str
        """
        found = 0
        obj_gen = (obj for obj in self.database[obj_type])
        while not found:
            try:
                current_obj = next(obj_gen)
                if current_obj["Name"] == obj_name:
                    obj_value = current_obj
                    found = 1
            except StopIteration:
                break

        if found:
            return obj_value
        else:
            return -1

    def post_object(self, obj_type):
        """Add database object. Not yet implemented.

        :param obj_type: Database object key
        :type obj_type: str
        """
        pass

    def put_object(self, obj_type, obj_name):
        """Update database object. Not yet implemented.

        :param obj_type: Database object key
        :type obj_type: str
        :param obj_name: Database object name
        :type obj_name: str
        """
        pass

    def delete_object(self, obj_type, obj_name):
        """Delete database object. Not yet implemented.

        :param obj_type: Database object key
        :type obj_type: str
        :param obj_name: Database object name
        :type obj_name: str
        """
        pass


class WebDatabase:
    """Not yet implemented."""

    exposed = True

    def __init__(self):
        self.database = Database("database.json")

    def GET(self, *uri, **params):
        """Not yet implemented."""
        self.database.load_file()
        return

    def POST(self, *uri, **params):
        """Not yet implemented."""
        self.database.load_file()
        return

    def PUT(self, *uri, **params):
        """Not yet implemented."""
        self.database.load_file()
        return

    def DELETE(self):
        """Not yet implemented."""
        self.database.load_file()
        return
