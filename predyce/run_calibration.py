#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from predyce.IDF_class import IDF
from pathlib import Path
from predyce import idf_editor
from predyce.runner import Runner
import json
import matplotlib.pyplot as plt
import yaml
import numpy as np
import argparse

plt.rcParams.update({"font.size": 15})

P = Path(__file__).parent.absolute()

parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help = "Checkpoint data (default: None)", default = None)
parser.add_argument("-c", "--checkpoint-interval", help = "Checkpoint interval (default: 128)", default = 128, type=int)
parser.add_argument ("-d", "--output-directory", help = "Output directory path (default: /predyce-out)", default = P / "predyce-out")
parser.add_argument("-i", "--idd", help='IDD version')
parser.add_argument ("-f", "--input-file", help = "Input data file path (default: in.json in executable directory" , default= P / "in.json")
parser.add_argument("-j", "--jobs", help = "Multi-process with N processes (default: auto)", type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action = "store_false", default = True)
parser.add_argument("-m", "--measured-data-directory", help = "Measured data directory path (default: None)", default = None)
parser.add_argument("-p", "--plot-directory", help = "Plot directory path (default: /predyce-plots)", default = P / "predyce-plots")
parser.add_argument("-t", "--temp-directory", help = "Temp directory path (default: /predyce-temp)", default = P / "predyce-temp")
parser.add_argument("-w", "--weather", help = "Weather file path (default: "" and must be in case specified in input json file -f)", default = "")
parser.add_argument("model", help = "Building model path (IDF format only accepted)")

args = parser.parse_args()

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

INPUT = args.input_file
fname1 = args.model
# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if args.weather.endswith(".epw"):
    EPW = weather
    EPW_DIR = weather.parent
else:
    EPW = None
    EPW_DIR = weather
idf1 = IDF(fname1, EPW)

CHECKPOINT_DATA = args.checkpoint_data

def main():
    # Load input file.

    with open(INPUT, "r") as f:
        input = json.loads(f.read())

    input["actions"] = {}
    for k, v in input["calibration_actions"].items():
        try:
            args_cal = v["args"]
        except Exception:
            args_cal = {}
        # if k == "change_ufactor_roofs":
        #     r = v["range"]
        #     s = v["step"]
        #     # try_values = np.arange(-r, r, s)
        #     # SB & AQ & MC
        #     try_values = [0]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"ufactor": try_values, **args_cal}

        # if k == "change_ufactor_walls":
        #     r = v["range"]
        #     s = v["step"]
        #     # try_values = np.arange(-r, r, s)
        #     # SB
        #     # try_values = [0.15]
        #     # MC
        #     try_values = [-0.15]
        #     # AQ
        #     # try_values = [0]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"ufactor": try_values, **args_cal}

        if k == "change_ach":
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            # SB
            # try_values = [-0.5]
            #MC
            try_values = [0.15]
            # AQ
            # try_values = [0.25]
            args_cal = {k: [v] for k, v in args_cal.items()}
            input["actions"][k] = {"ach": try_values, **args_cal}

        # if k == "change_infiltration":
        #     r = v["range"]
        #     s = v["step"]
        #     try_values = np.arange(-r, r, s)
        #     # SB
        #     # try_values = [0.2]
        #     # MC
        #     try_values = [0.1]
        #     # AQ
        #     # try_values = [0.25]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"ach": try_values, **args_cal}

        # if k == "change_internal_heat_gain":
        #     r = v["range"]
        #     s = v["step"]
        #     try_values = np.arange(-r, r, s)
        #     # SB
        #     # try_values = [5]
        #     # MC
        #     try_values = [0]
        #     # AQ
        #     # try_values = [-0.35]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"value": try_values, **args_cal}

        # # if k == "change_occupancy":
        # #     try_values = [-0.05, 0.05]
        # #     args_cal = {k: [v] for k, v in args_cal.items()}
        # #     input["actions"][k] = {"value": try_values, **args_cal}

        # if k == "change_ufactor_windows":
        #     idf_editor.set_simplified_windows(idf1)
        #     value = v["value"]
        #     r = v["range"]
        #     s = v["step"]
        #     # try_values = np.arange(value-value*r, value+value*r, value*s)
        #     # SB
        #     # try_values = [1.3846]
        #     # AQ
        #     # try_values = [2.7692]
        #     # MC
        #     try_values = [2.4725]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"value": try_values, **args_cal}

        # if k == "change_shgc":
        #     # idf_editor.set_simplified_windows(idf1)
        #     value = v["value"]
        #     r = v["range"]
        #     s = v["step"]
        #     # try_values = np.arange(value-value*r, value+value*r, value*s)
        #     # SB
        #     # try_values = [0.8625]
        #     # AQ
        #     # try_values = [0.759]
        #     # MC
        #     try_values = [0.8279]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"value": try_values, **args_cal}

        # # if k == "change_specific_heat_material":
        # #     r = v["range"]
        # #     s = v["step"]
        # #     # try_values = np.arange(-r, r, s)
        # #     try_values = [-0.2]
        # #     args_cal = {k: [v] for k, v in args_cal.items()}
        # #     input["actions"][k] = {"value": try_values, **args_cal}

        # if k == "add_internal_mass":
        #     # SB
        #     # try_values = [10.5]
        #     # AQ
        #     # try_values = [8.5]
        #     # MC
        #     try_values = [2]
        #     args_cal = {k: [v] for k, v in args_cal.items()}
        #     input["actions"][k] = {"area": try_values, **args_cal}                    

    # Create Runner and run simulations.
    config = {"plot_dir": args.plot_directory,
              "temp_dir": args.temp_directory,
              "out_dir": args.output_directory,
              "num_of_cpus": args.jobs,
              "epw_dir": EPW_DIR,
              "data_true_dir": args.measured_data_directory}

    runner = Runner(
        idf1,
        input,
        **config,
        checkpoint_interval=args.checkpoint_interval,
        checkpoint_data=CHECKPOINT_DATA,
        graph=True
    )

    runner.create_dataframe(input, args.original)
    print(runner.df)
    runner.run()
    runner.print_summary()


if __name__ == "__main__":
    main()
