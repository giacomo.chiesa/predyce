#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Functions used to run EnergyPlus simulations"""
from predyce import idf_editor, calibration
import pandas as pd
import numpy as np
from pathlib import Path
import copy
import os
import uuid
from itertools import product
from multiprocessing import Pool, cpu_count
from predyce.kpi import EnergyPlusKPI
import math
import json
import time
from datetime import datetime, timedelta
from pyepw.epw import EPW
import gc


def make_eplaunch_options(
    idf, readvars=True, expandobjects=True, verbose="q", output_directory="Out"
):
    """Make options for run, so that it runs like EPLaunch on Windows.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param readvars: Run ReadVarsESO after simulation to generate the CSV file
        of the output, defaults to `True`
    :type readvars: bool, optional
    :param expandobjects: Run ExpandObjects prior to simulation, defaults to
        `True`
    :type expandobjects: bool, optional
    :param verbose: Set verbosity of runtime messages, defaults to 'q'
    :type verbose: str, optional
    :param out_dir: Output directory of EnergyPlus.
    :type out_dir: path or str
    :return: Options for EnergyPlus
    :rtype: dict
    """
    options = {
        "readvars": readvars,
        "expandobjects": expandobjects,
        "verbose": verbose,
        "output_directory": output_directory,
    }
    return options


class Runner:
    """Class which run EnergyPlus simulations."""

    def __init__(
        self,
        idf,
        input_file,
        temp_dir,
        out_dir,
        epw_dir,
        plot_dir,
        aggregations=None,
        data_true_dir=None,
        num_of_cpus="auto",
        checkpoint_interval=100,
        checkpoint_data=None,
        graph=False,
    ):
        """
        :param idf: IDF object
        :type idf: class:`predyce.IDF_class.IDF`
        :param input_file: Input file with actions and outputs.
        :type input_file: dict
        :param temp_dir: Output directory of EnergyPlus simulationns.
        :type temp_dir: path or str
        :param out_dir: Output directory.
        :type out_dir: path or str
        :param num_of_cpus: Number of processes to be used. If set to `auto`, a
            number of processes equal to number of CPUs will be used, defaults
            to auto
        :type num_of_cpus: str or int, optional
        :param checkpoint_interval: [Description], defaults to 100
        :type checkpoint_interval: int, optional
        :param checkpoint_data: Checkpoint dataframe, defaults to None
        :type checkpoint_data: class:`pandas.core.frame.DataFrame`, optional
        """
        self.idf = copy.deepcopy(idf)
        self.input_file = input_file
        self.outputs = None
        self.temp_dir = Path(temp_dir)
        self.out_dir = Path(out_dir)
        self.plot_dir = Path(plot_dir)

        try:
            self.output_list = input_file["outputs"]
        except KeyError:
            self.output_list = {}

        try:
            self.kpi_list = input_file["kpi"]
        except KeyError:
            self.kpi_list = {}

        try:
            self.cal_list = input_file["calibration"]
        except KeyError:
            self.cal_list = {}

        try:
            self.calibration_objectives = input_file["calibration_objectives"]
        except KeyError:
            self.calibration_objectives = {}

        self.data_true_dir = data_true_dir
        self.num_cpu = cpu_count()
        if num_of_cpus == 0:
            self.num_processes = self.num_cpu
        else:
            self.num_processes = num_of_cpus
        self.epw_dir = epw_dir
        self.checkpoint_interval = checkpoint_interval
        idf.epw_dir = Path(epw_dir)
        self.checkpoint_data = checkpoint_data
        self.graph = graph
        self.preliminary_actions()
        idf.set_block(input_file["building_name"])
        try:
            self.aggregations = input_file["aggregations"]
        except KeyError:
            self.aggregations = None
        try:
            self.start_date = input_file["start_date"]
        except KeyError:
            self.start_date = None
        try:
            self.end_date = input_file["end_date"]
        except KeyError:
            self.end_date = None

    def preliminary_actions(self):
        """Execute preliminary actions on the building before running the
        multiple simulations.
        """
        try:
            prel_acts = self.input_file["preliminary_actions"]
            if prel_acts:
                print("Preliminary actions...")
        except KeyError:
            prel_acts = {}

        for act, values in prel_acts.items():
            print(act, values)
            call_method(self.idf, act, values)

    def create_dataframe(self, input_file, include_original=False):
        """Create datafame of all permutations

        :param input_file: Input file with actions and outputs.
        :type input_file: dict
        :param include_original: Whether to include the original building in
            the list of simulations, defaults to `False`
        :type include_original: bool, optional
        """
        # If no action is specified, add a dummy action.
        if "actions" not in input_file:
            input_file["actions"] = {"no_action": {}}
        if not input_file["actions"]:
            input_file["actions"] = {"no_action": {}}

        # If a directory was specified in the EPW field.
        if not hasattr(self.idf, "epw"):
            if "epw" not in input_file["actions"]:
            # if not input_file["actions"]["epw"]:
                input_file["actions"]["epw"] = {"epw_file": ["dir"]}

        keys, values = zip(*input_file["actions"].items())
        all_data = []
        for k, v in zip(keys, values):
            new_keys = []
            new_values = []
            for field, field_values in v.items():
                new_key = ".".join((k, field))
                new_keys.append(new_key)
                if k == "epw":
                    for epw_file in field_values:
                        if epw_file == "dir":
                            new_values.append(os.listdir(self.epw_dir))
                        else:
                            new_values.append([epw_file])
                else:
                    new_values.append(field_values)
            permutations_dicts = [
                dict(zip(new_keys, v)) for v in product(*new_values)
            ]
            df = pd.DataFrame()
            for el in permutations_dicts:
                df = df.append(el, ignore_index=True)
            if include_original:
                if "epw" not in k:
                    df = df.append(pd.Series(), ignore_index=True)
            all_data.append(df)

        nums = [list(range(len(d))) for d in all_data]
        perms = product(*nums)
        col_list = []
        for d in all_data:
            for col in d.columns:
                col_list.append(col)

        df = pd.DataFrame(columns=col_list)
        perms = product(*nums)
        for idx, tup in enumerate(perms):
            for i, pos in enumerate(tup):
                new_data = all_data[i].loc[pos]
                df.loc[idx, new_data.index] = new_data

        self.df = df
        if self.outputs == None:
            self.outputs = copy.deepcopy(self.df)

        try:
            os.makedirs(self.plot_dir)
        except FileExistsError:
            pass

        with open(self.plot_dir / "summary.html", "w") as fo:
            self.df.to_html(fo)

    def run(self):
        """Run every row of the DataFrame as a simulation."""
        if self.graph:
            try:
                os.makedirs(self.plot_dir)
            except FileExistsError:
                pass

        pool = Pool(self.num_processes)
        # result_list = []
        db = Database(out_dir=self.out_dir, interval=self.checkpoint_interval)
        if self.checkpoint_data is not None:
            db.add_data(self.checkpoint_data)

        def log_result(result):
            db.add_data(result.to_frame().T)
            db.checkpoint()
            # result_list.append(result.to_frame().T)

        self.start = time.time()
        if self.num_processes == 1:  # To make it work with Jupyter Notebook
            for i in range(len(self.df)):
                data_res = run(
                    self.df.loc[i],
                    self.idf,
                    self.temp_dir,
                    self.output_list,
                    self.kpi_list,
                    self.cal_list,
                    self.plot_dir,
                    self.data_true_dir,
                    self.aggregations,
                    len(self.df),
                    self.graph,
                    self.start_date,
                    self.end_date
                )
                db.add_data(data_res)

        else:
            sims = range(len(self.df))
            if self.checkpoint_data is not None:
                sims = [i for i in sims if i not in self.checkpoint_data.index]
                print("Resuming checkpoint...")

            for i in sims:
                pool.apply_async(
                    run,
                    args=(
                        self.df.loc[i],
                        self.idf,
                        self.temp_dir,
                        self.output_list,
                        self.kpi_list,
                        self.cal_list,
                        self.plot_dir,
                        self.data_true_dir,
                        self.aggregations,
                        len(self.df),
                        self.graph,
                        self.start_date,
                        self.end_date
                    ),
                    callback=log_result,
                )

            pool.close()
            pool.join()

            try:
                os.makedirs(self.out_dir)
            except FileExistsError:
                pass

        self.end = time.time()
        self.results = db.df

        # NOTE: Use this to remove columns from final output.
        # db.clean_data(["epw.epw_file"])

        db.save_data(self.out_dir / "data_res.csv")

        with open(self.plot_dir / "summary_res.html", "w") as fo:
            self.results.to_html(fo)

    def print_summary(self, filename="summary.txt"):
        """Print summary

        :param filename: Filename of saved summary, defaults to "summary.txt"
        :type filename: str, optional
        """
        lines = [
            "Number of simulations: %d" % (len(self.outputs)),
            "Number of processes allocated in the pool: %d"
            % self.num_processes,
            "Number of CPUs: %d" % self.num_cpu,
            "Started at: %s"
            % datetime.utcfromtimestamp(self.start).strftime(
                "%Y-%m-%d %H:%M:%S"
            ),
            "Finished at: %s"
            % datetime.utcfromtimestamp(self.end).strftime(
                "%Y-%m-%d %H:%M:%S"
            ),
            "Elapsed time: %s" % str(timedelta(seconds=self.end - self.start)),
        ]
        with open(self.out_dir / filename, "w") as fo:
            fo.writelines([l + "\n" for l in lines])

        for l in lines:
            print(l)
        print(self.results)


def call_method(building, function, values):
    """Call functions from idf_editor.

    :param building: IDF object
    :type building: class:`predyce.IDF_class.IDF`
    :param function: Name of the function which has to be called.
    :type function: str
    :param values: Dictionary where each key is a field and each value is its
        value.
    :type values: dict
    :return: Modified IDF object
    :rtype: class:`predyce.IDF_class.IDF`
    """
    for _, v in values.items():
        try:
            if math.isnan(v):
                return building
        except Exception:
            pass
    if function == "no_action":
        pass
    if function == "add_external_insulation_walls":
        idf_editor.add_external_insulation_walls(building, **values)
    elif function == "add_internal_insulation_walls":
        idf_editor.add_internal_insulation_walls(building, **values)
    elif function == "add_roof_insulation_tilted_roof":
        idf_editor.add_roof_insulation_tilted_roof(building, **values)
    elif function == "add_ceiling_insulation_tilted_roof":
        idf_editor.add_ceiling_insulation_tilted_roof(building, **values)
    elif function == "add_insulation_flat_roof":
        idf_editor.add_insulation_flat_roof(building, **values)
    elif function == "change_ufactor":
        idf_editor.change_ufactor(building, **values)
    elif function == "change_ufactor_walls":
        idf_editor.change_ufactor_walls(building, **values)
    elif function == "change_ufactor_roofs":
        idf_editor.change_ufactor_roofs(building, **values)
    elif function == "change_windows_system":
        idf_editor.change_windows_system(building, **values)
    elif function == "epw":
        building.epw = building.epw_dir / values["epw_file"]
    elif function == "change_occupancy":
        idf_editor.change_occupancy(building, **values)
    elif function == "add_overhang_simple":
        idf_editor.add_overhangs_simple(building, **values)
    elif function == "add_fin_simple":
        idf_editor.add_fin_simple(building, **values)
    elif function == "change_ach":
        idf_editor.change_ach(building, **values)
    elif function == "change_infiltration":
        idf_editor.change_infiltration(building, **values)
    elif function == "add_blind":
        idf_editor.add_blind(building, **values)
    elif function == "add_scheduled_nat_vent":
        idf_editor.add_scheduled_nat_vent(building, **values)
    elif function == "activate_cooling":
        idf_editor.activate_cooling(building, **values)
    elif function == "deactivate_cooling":
        idf_editor.deactivate_cooling(building, **values)
    elif function == "change_runperiod":
        idf_editor.change_runperiod(building, **values)
    elif function == "change_window_opening":
        idf_editor.change_window_opening(building, **values)
    elif function == "add_hvac":
        idf_editor.add_hvac(building, **values)
    elif function == "set_daylight_saving":
        idf_editor.set_daylight_saving(building, **values)
    elif function == "set_ems_constants":
        idf_editor.set_ems_constants(building, **values)
    elif function == "set_simplified_windows":
        idf_editor.set_simplified_windows(building, **values)
    elif function == "change_shgc":
        idf_editor.change_shgc(building, **values)
    elif function == "change_ufactor_windows":
        idf_editor.change_ufactor_windows(building, **values)
    elif function == "change_internal_heat_gain":
        idf_editor.change_internal_heat_gain(building, **values)
    elif function == "change_specific_heat_material":
        idf_editor.change_specific_heat_material(building, **values)
    elif function == "add_overhangs_simple":
        idf_editor.add_overhangs_simple(building, **values)
    elif function == "set_shading_control":
        idf_editor.set_shading_control(building, **values)
    elif function == "set_object_params":
        idf_editor.set_object_params(building, **values)
    elif function == "add_internal_mass":
        idf_editor.add_internal_mass(building, **values)
    elif function == "set_ach_schedule":
        idf_editor.set_ach_schedule(building, **values)
    elif function == "set_outputs":
        idf_editor.set_outputs(building, **values)
    elif "edit_csv_schedule" in function:
        idf_editor.edit_csv_schedule(building, **values)
    elif "set_outdoor_co2" in function:
        idf_editor.set_outdoor_co2(building, **values)
    elif "change_co2_prod_rate" in function:
        idf_editor.change_co2_prod_rate(building, **values)
    else:
        print("WARNING: no edit")
        raise Exception
    return building


def call_kpi(function):
    """Not implemented yet."""
    pass


def run(
    df,
    idf,
    temp_dir,
    output_list,
    kpi_list,
    cal_list,
    plot_dir,
    data_true_dir,
    aggregations,
    num_total="?",
    graph=False,
    start_date=None,
    end_date=None
):
    """Create building, run EnergyPlus and return its output.

    :param df: DataFrame of simulations.
    :type df: class:`pandas.core.frame.DataFrame`
    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param temp_dir: Output directory of EnergyPlus simulationns.
    :type temp_dir: path
    :param output_list: EnergyPlus outputs which have to be saved into the
        results dataframe.
    :type output_list: list
    :param kpi_list: KPI which have to be calculated and saved into the results
        dataframe.
    :type kpi_list: list
    :param cal_list: [Description]
    :type cal_list: [Description]
    :param plot_dir: Path to directory where plots are saved.
    :type plot_dir: Path or str
    :param data_true_dir: [Description]
    :type data_true_dir: [Description]
    :param num_total: Total number of simulations, only for print purpose,
        defaults to "?"
    :type num_total: int, optional
    :param graph: Whether to save plots, defaults to `False`
    :type graph: bool, optional
    :raises Exception: If editing action has no effect on the building
    :return: Dataframe with added outputs.
    :rtype: class:`pandas.core.frame.DataFrame`
    """
    # Create and cd into a temporary directory.
    try:
        os.makedirs(temp_dir)
    except FileExistsError:
        pass
    temp_dir = generate_temp_folder(temp_dir, 25)
    os.mkdir(temp_dir)
    os.chdir(temp_dir)

    plot_idx = str(df.name)  # Name of the simulation

    # Create building to be simulated.
    transformation = df
    building = create_building(idf, transformation)

    # TODO: Gestire meglio.
    # epwobj = EPW()
    # epwobj.read(building.epw)
    # location = {
    #     "city": epwobj.location.city,
    #     "longitude": epwobj.location.longitude,
    #     "latitude": epwobj.location.latitude,
    # }
    # df["location"] = json.dumps(location, cls=NpEncoder)

    try:
        out = run_building(building, df, temp_dir, output_list, num_total)
    except KeyboardInterrupt:
        print("Forced interruption, perform cleaning...")
        remove_files(temp_dir)
        print("Done")
        quit()  # Terminate program
    except Exception as e:
        print(e)

    # TODO: Teoricamente da rimuovere se non abbiamo output standard.
    for o in output_list:
        if "RunPeriod" in o:
            df[o] = out[o].iloc[-1]
        elif ("Hourly" in o) or ("Monthly" in o) or ("Daily" in o):
            df[o] = out[o]
        else:
            pass

    real_name = copy.deepcopy(building.main_block)  # A backup
    if aggregations is not None:
        for func, agg in aggregations.items():
            for a in agg:
                building.main_block = a
                ekpi = EnergyPlusKPI(out, building, plot_dir / plot_idx, start_date, end_date, graph=graph)
                params = kpi_list[func]
                if func == "pmv_ppd":
                    kpi = ekpi.pmv_ppd(**params)
                elif func == "adaptive_comfort_model":
                    kpi = ekpi.adaptive_comfort_model(**params)

                if isinstance(kpi, dict):
                    df[func + "_" + a] = json.dumps(kpi, cls=NpEncoder)
                else:
                    df[func + "_" + a] = kpi

            building.set_block(real_name)

    # TODO: Make a separate function which calls KPI functions.
    ekpi = EnergyPlusKPI(out, building, plot_dir / plot_idx, start_date, end_date, graph=graph)
    for func, params in kpi_list.items():
        if func == "pmv_ppd":
            kpi = ekpi.pmv_ppd(**params)
        elif func == "adaptive_comfort_model":
            kpi = ekpi.adaptive_comfort_model(**params)
        elif func == "energy_signature":
            kpi = ekpi.energy_signature(**params)
        elif func == "cidh":
            kpi = ekpi.cidh(**params)
        elif func == "adaptive_residuals":
            kpi = ekpi.adaptive_residuals(**params)
        elif func == "cdd":
            kpi = ekpi.cdd(**params)
        elif func == "cdd_res":
            kpi = ekpi.cdd_res(**params)
        elif func == "cdh":
            kpi = ekpi.cdh(**params)
        elif func == "qach":
            kpi = ekpi.qach(**params)
        elif func == "ccp":
            kpi = ekpi.ccp(**params)
        elif "q_c" in func.lower():
            kpi = ekpi.q_c(**params)
        elif "q_h" in func.lower():
            kpi = ekpi.q_h(**params)
        elif func.lower() == "interior_lights":
            kpi = ekpi.interior_lights(**params)
        elif func.lower() == "n_h_kwh":
            kpi = ekpi.n_h_kwh(**params)
        elif func.lower() == "co2":
            kpi = ekpi.co2(**params)
        elif func == "fictitious_cooling":
            # TODO gestire due casi: casa con già hvac ma senza cooling, casa senza niente
            idf_editor.activate_cooling(building, **params)
            idf_editor.change_ach(building, 0, "ventilation")
            out2 = run_building(
                building,
                transformation,
                temp_dir / ".kpi",
                output_list,
            )
            kpi = 0
            kpi = ekpi.fictitious_cooling(out2)
        else:
            raise Exception("KPI not found")

        if isinstance(kpi, dict):
            df[func] = json.dumps(kpi, cls=NpEncoder)
        else:
            df[func] = kpi

    # Calibration.
    if len(cal_list) > 0:
        # TODO: Make a separate function which calls calibration functions.
        epc = calibration.EnergyPlusCalibration(
            out, building, plot_dir / plot_idx, data_true_dir
        )
        # TODO: Replace col_name calls and support multiple objectives.
        (x, y, ax) = epc.retrieve_data(col_name="T_db_i")
        x.index = x.index.map(lambda t: t.replace(year=1970))
        y.index = y.index.map(lambda t: t.replace(year=1970))
        ax.index = ax.index.map(lambda t: t.replace(year=1970))

        for func, params in cal_list.items():
            if func == "rmse":
                met = calibration.rmse(x, y)
            if func == "mbe":
                met = calibration.mbe(x, y)
            if func == "rmse_mbe":
                met = calibration.rmse_mbe(x, y)
            if func == "calibration_signature":
                met = calibration.calibration_signature(
                    x, y, ax, plot_dir / plot_idx
                )
            if func == "comparison":
                met = calibration.comparison(x, y, plot_dir / plot_idx)
            try:
                df[func] = met
            except Exception:
                df[func] = float("nan")

    remove_files(temp_dir)  # Remove e+ simulation files
    return df


def create_building(idf, transformation):
    """Apply edits on building before running EnergyPlus simulation.

    :param idf: IDF object
    :type idf: class:`predyce.IDF_class.IDF`
    :param transformation: Dictionary of transformations. Each key corresponds
        to an action. Each value is a dictionary where each key is a field and
        each value is its value.
    :type transformation: dict
    :return: Modified IDF object
    :rtype: class:`predyce.IDF_class.IDF`
    """
    building = copy.deepcopy(idf)
    new_trans = {}
    for k, v in transformation.items():
        methods = k.split(".")[0]
        field = k.split(".")[1]
        if methods not in new_trans:
            data = {methods: {field: v}}
            new_trans.update(data)
        else:
            data = {field: v}
            new_trans[methods].update(data)
    transformation = new_trans

    for k, v in transformation.items():
        building = call_method(building, k, v)

    return building


def run_building(building, df, out_dir, output_list, num_total="?"):
    """Run EnergyPlus simulation for the specified building.

    :param building: IDF object
    :type building: class:`predyce.IDF_class.IDF`
    :param df: DataFrame where each row represent a simulation.
    :type df: class:`pandas.core.frame.DataFrame`
    :param out_dir: Output directory of EnergyPlus.
    :type out_dir: path or str
    :param output_list: EnergyPlus outputs which has to be saved into the
        dataframe.
    :type output_list: list
    :param num_total: Total number of simulations, only for print purpose,
        defaults to "?"
    :type num_total: int, optional
    :return: Output dataframe corresponding to the original dataframe with
        added outputs.
    :rtype: class:`pandas.core.frame.DataFrame`
    """
    print("Running... %s/%s" % (df.name, num_total))
    # print(out_dir, "- Running... %s/%s" % (df.name, num_total))
    out_file = out_dir / "eplusout.csv"
    options = make_eplaunch_options(building, output_directory=out_dir)
    building.run(**options)
    out = pd.read_csv(out_file)
    return out


def generate_temp_folder(directory, length=25):
    """Generate temp folder name.

    :param directory: Initial directory
    :type directory: path
    :param length: Number of characters of the alphanumeric part of the name,
        defaults to 25
    :type length: int, optional
    :return: Temporary directory
    :rtype: path
    """
    random_string = "_" + str(uuid.uuid4()).replace("-", "")[0:length]
    temp_dir = directory / random_string
    return temp_dir


def remove_files(dir_path):
    """Remove `mtr`, `htm`, `edd`, `eio`, `eso`, `edd`, `shd`, `dxf`, `mtd`,
    `rdd`, `tab`, `mdd`, `bnd`, `err`, `audit`, `rvaudit`, `end`, `dbg`, `csv`
    files from specified folder. This function can be utilised to clean
    EnergyPlus simulation files.

    :param dir_path: Path of directory
    :type dir_path: Path or str

    .. warning:: The eplusout.csv file which contains simulation results is
        also deleted.
    """
    dir_path = Path(dir_path)
    extensions = (
        ".mtr",
        ".htm",
        ".edd",
        ".eio",
        ".eso",
        ".edd",
        ".shd",
        ".dxf",
        ".mtd",
        ".rdd",
        ".tab",
        ".mdd",
        ".bnd",
        ".err",
        ".audit",
        ".rvaudit",
        ".end",
        ".dbg",
        ".csv",
    )
    files = os.listdir(dir_path)
    for item in files:
        if item.endswith(extensions):
            try:
                os.remove(dir_path / item)
            except OSError as e:
                print("Error: %s : %s" % (dir_path / item, e.strerror))
    try:
        os.chdir("..")
        os.rmdir(dir_path)
    except OSError as e:
        print("Error: %s : %s" % (dir_path, e.strerror))


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


class Database:
    """Class which stores results."""

    def __init__(self, out_dir, interval):
        """
        :param out_dir: Output directory
        :type out_dir: Path or str
        :param interval: Checkpoint interval
        :type interval: int
        """
        self.df = pd.DataFrame()
        self.out_dir = out_dir
        self.cnt = 0
        self.interval = interval

    def add_data(self, data):
        """Add data to the database

        :param data: DataFrame which contains simulation results.
        :type data: class:`pandas.core.frame.DataFrame`
        """
        self.df = self.df.append(data, ignore_index=False)
        self.cnt += 1

    def checkpoint(self):
        """Save a checkpoint as CSV file if the number of data in the database
        is a multiple of the checkpoint interval.
        """
        if self.cnt % self.interval == 0:
            gc.collect(2)
            self.save_data(
                self.out_dir
                / "checkpoint-{}.csv".format(
                    datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                )
            )

    def clean_data(self, columns):
        """Delete some columns from the database.

        :param columns: List of columns
        :type columns: list of str
        """
        for c in columns:
            del self.df[c]

    def save_data(self, filename, sep=";"):
        """Save database to CSV file.

        :param filename: Filename
        :type filename: str
        :param sep: Separator, defaults to ";"
        :type sep: str, optional
        """
        self.df.to_csv(filename, sep=sep)
