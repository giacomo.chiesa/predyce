# coding: utf-8
from predyce.IDF_class import IDF
from predyce.idf_editor import set_fields
from predyce.runner import Runner, idf_editor
from pathlib import Path
import pandas as pd
import json
import yaml
import argparse
import tempfile
import datetime
from kpi import EnergyPlusKPI
import os
import uuid
import copy
import matplotlib.pyplot as plt

MAINBLOCK = "kpi"
DOW = {
    7: "Sunday",
    0: "Monday",
    1: "Tuesday",
    2: "Wednesday",
    3: "Thursday",
    4: "Friday",
    5: "Saturday"
    }

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()


class MyDateTime(datetime.datetime):
    def __new__(cls, *args, **kwargs):
        return datetime.datetime.__new__(cls, *args, **kwargs)

    def minutes_of_year(self):
        return self.seconds_of_year() // 60

    def hours_of_year(self):
        return self.minutes_of_year() // 60

    def seconds_of_year(self):
        dt0 = datetime.datetime(self.year, 1, 1, tzinfo=self.tzinfo)
        delta = self-dt0
        return int(delta.total_seconds())

# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

# Argument parameters which override the ones in configuration file.
parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data absolute path (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=128, type=int)
parser.add_argument("-d", "--output-directory", help="Output directory absolute path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument("-f", "--input-file", help="Input data file absolute path (default: in.json in executable directory" , default=P / "in.json")
parser.add_argument("-j", "--jobs", help= "Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data", help="Measured data file absolute path (default: None)", default="")
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=Path(tempfile.gettempdir()) / "predyce")
parser.add_argument("-w", "--weather", help= "Weather file absolute path (default: "" and must be in case specified in input JSON file)", default=CONFIG["epw"])
parser.add_argument("model", help= "Building model path (IDF format only accepted)")

args = parser.parse_args()

# IDF file.
fname1 = args.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
INPUT = args.input_file

# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if weather.is_file():
    EPW = weather
    EPW_DIR = weather.parent
elif weather.is_dir():
    EPW = None
    EPW_DIR = weather
else:
    raise IOError("Error in the EPW weather file")

idf1 = IDF(fname1, EPW)
idf1.set_block(MAINBLOCK)


def set_step_outputs(idf):
    idf.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 6
    idf_editor.set_outputs(idf, ["Zone Ventilation Air Change Rate",
    "Zone Infiltration Air Change Rate"], "*", "hourly", reset=True)


def main():
    # Load input file.
    with open(INPUT, "r") as f:
        input = json.loads(f.read())

    config = {
        "plot_dir": args.plot_directory,
        "temp_dir": args.temp_directory,
        "out_dir": args.output_directory,
        "num_of_cpus": args.jobs,
        "epw_dir": EPW_DIR,
        "data_true_dir": args.measured_data,
    }

    # Get current day an month
    day = str(datetime.datetime.today().day)
    month = str(datetime.datetime.today().month)
    year = datetime.datetime.today().year
    dow = datetime.datetime(year, 1, 1).weekday()
    dow = DOW[dow]

    DAY = MyDateTime.strptime("02/08", "%d/%m")  # To test
    print("24h forecast for day: ", day)
    print("24h forecast for day: ", DAY)

    try:
        os.makedirs(Path(args.temp_directory) / "idf_steps")
    except FileExistsError:
        pass

    step0 = copy.deepcopy(idf1)
    set_step_outputs(step0)
    step0.save(Path(args.temp_directory) / "idf_steps" / "step_0.idf")

    # Find ACH schedule values.
    idf_editor.change_runperiod(idf1, "01-01", "31-12", fmt="%d-%m")
    idf1.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 1
    schedule_names = []
    for zvdfr in idf1.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if idf1.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower():
            for sc in idf1.idfobjects["SCHEDULE:COMPACT"]:
                if sc.Name == zvdfr.Schedule_Name:
                    # Make a copy of the schedule and save its name in a list.
                    new_sc = idf1.copyidfobject(sc)
                    new_sc.Name += ("_" + zvdfr.Zone_or_ZoneList_Name)
                    new_sc.Name = new_sc.Name.replace(" ", "_").replace("(", "_").replace(")", "_")
                    zvdfr.Schedule_Name = new_sc.Name
                    zvdfr.Schedule_Name = zvdfr.Schedule_Name.replace(" ", "_").replace("(", "_").replace(")", "_")
                    schedule_names.append(zvdfr.Schedule_Name)
                    break  # NOTE: Important!

    # Delete useless outputs that slow down the simulation.
    idf_editor.set_outputs(idf1, [None], None, reset=True)

    # Add new schedules in the output variables to create a file that will
    # be used in new Schedule:File schedules later.
    for schedule in schedule_names:
        idf_editor.set_outputs(idf1, ["Schedule Value"], schedule, frequency="Hourly", reset=False)  # O forse "Hourly"

    # Delete useless outputs that slow down the simulation.
    del idf1.idfobjects["Output:EnvironmentalImpactFactors".upper()][:]
    del idf1.idfobjects["Output:Meter".upper()][:]

    # Match first weekday of the year.
    # idf_editor.set_fields(idf1.idfobjects["RUNPERIOD"][0], Day_of_Week_for_Start_Day=dow)  # NOTE: De-comment me

    try:
        os.makedirs(Path(args.temp_directory) / "schedule_runs")
    except FileExistsError:
        pass

    step1 = copy.deepcopy(idf1)
    set_step_outputs(step1)
    step1.save(Path(args.temp_directory) / "idf_steps" / "step_1.idf")
    idf1.save(Path(args.temp_directory) / "schedule_runs" / "test_schedule.idf")
    # idf_editor.set_outputs(idf1, [None], None, frequency="Hourly")
    print("Exporting ventilation ACH schedules to CSV file...")
    idf1.run(output_directory=Path(args.temp_directory) / "schedule_runs", readvars=True, verbose="q")

    # Read eplusout.csv and use it as a CSV for Schedule:File
    res = pd.read_csv(Path(args.temp_directory) / "schedule_runs" / "eplusout.csv")
    res.index = res["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
    del res["Date/Time"]
    res.to_csv(Path(args.temp_directory) / "schedule_runs" / "ach_schedules.csv", sep=";")
    res.to_csv(Path(args.temp_directory) / "schedule_runs" / "ach_schedules_original.csv", sep=";")
    res = res.resample("1H").mean()
 
    ach_file = pd.read_csv(
        Path(args.temp_directory) / "schedule_runs" / "ach_schedules.csv",
        sep=";",
        index_col=0,
    )
    columns = [c.replace("ON) ", "ON)") for c in list(ach_file.columns)]

    for schedule in schedule_names:
        for zvdfr in idf1.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
            if idf1.main_block.lower() in zvdfr.Zone_or_ZoneList_Name.lower() and zvdfr.Schedule_Name == schedule:
                # zvdfr.Schedule_Name += ("_" + zvdfr.Zone_or_ZoneList_Name)
                # Create Schedule:File
                for sc in idf1.idfobjects["SCHEDULE:COMPACT"]:
                    if sc.Name == schedule:
                        idf1.removeidfobject(sc)
                sf = idf1.newidfobject("SCHEDULE:FILE")
                set_fields(sf, data={
                    "Name": zvdfr.Schedule_Name,
                    "Schedule Type Limits Name": "Fraction",
                    "Rows to Skip at Top": 1,
                    "Column Separator": "Semicolon",
                    "File Name": str(Path(args.temp_directory) / "schedule_runs" / "ach_schedules.csv"),
                    # "File Name": "ach_schedules.csv",
                    "Column Number": columns.index(zvdfr.Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)") + 2,
                })

    del idf1.idfobjects["OUTPUT:VARIABLE"][:]  # Delete schedule reports.
    step2 = copy.deepcopy(idf1)
    set_step_outputs(step2)
    step2.save(Path(args.temp_directory) / "idf_steps" / "step_2.idf")
    idf1.save(Path(args.temp_directory) / "schedule_runs" / "new_schedule.idf")

    # Elaborate values in order to make it compatible with old and tested values
    # (maybe ACH=100 and multiply/divide all other values).
    for zvdfr in idf1.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
        if MAINBLOCK.lower() in zvdfr.Zone_or_ZoneList_Name.lower():
            for z in idf1.idfobjects["ZONE"]:
                if z.Name.lower() == zvdfr.Zone_or_ZoneList_Name.lower():
                    break
            ach = idf_editor.get_ach(z, zvdfr.Design_Flow_Rate)
            idf_editor.change_ach(idf1, 100, ach_type="ventilation", filter_by=z.Name)
            factor = 100 / ach
            column_to_edit_index = columns.index(zvdfr.Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)")
            ach_file.iloc[:, column_to_edit_index] = ach_file.iloc[:, column_to_edit_index].apply(lambda x: x/factor)

    # Elaborate values in order to make it compatible with old and tested values
    # (maybe ACH=100 and multiply/divide all other values).
    # for zvdfr in idf1.idfobjects["ZoneVentilation:DesignFlowRate".upper()]:
    #     # zvdfr.Air_Changes_per_Hour = ach
    #     if MAINBLOCK.lower() in zvdfr.Zone_or_ZoneList_Name.lower():
    #         zvdfr.Design_Flow_Rate_Calculation_Method = "AirChanges/Hour"
    #         for z in idf1.idfobjects["ZONE"]:
    #             if z.Name.lower() == zvdfr.Zone_or_ZoneList_Name.lower():
    #                 break
    #         ach = round(idf_editor.get_ach(z, zvdfr.Design_Flow_Rate), 2)
    #         # Test
    #         zvdfr.Air_Changes_per_Hour = ach
            # Test 
            # # idf_editor.change_ach(idf1, 100, ach_type="ventilation", filter_by=z.Name)           
            # zvdfr.Air_Changes_per_Hour = 100
            # # zvdfr.Design_Flow_Rate = "None"
            # factor = 100 / ach
            # column_to_edit_index = columns.index(zvdfr.Schedule_Name.upper() + ":Schedule Value [](Hourly:ON)")
            # ach_file.iloc[:, column_to_edit_index] = ach_file.iloc[:, column_to_edit_index].apply(lambda x: x/factor)

    ach_file.to_csv(Path(args.temp_directory) / "schedule_runs" / "ach_schedules.csv", sep=";")
    try:
        os.makedirs(Path(args.temp_directory) / "schedule_runs_file")
    except FileExistsError:
        pass
    step3 = copy.deepcopy(idf1)
    set_step_outputs(step3)
    step3.save(Path(args.temp_directory) / "idf_steps" / "step_3.idf")
    idf1.save(Path(args.temp_directory) / "schedule_runs_file" / "new_schedule_file.idf")
    idf_editor.set_outputs(idf1, [None], None, frequency="Hourly", reset=True)  # Re-add some outputs.

    # Add Blinds and assign them new schedules based on orientation.
    for o in idf1.windows_orientations:
        idf_editor.add_blind(
            idf1, blind_data = {
                "Name": "20001",
                "Slat Width": 0.025,
                "Slat Separation": 0.01875,
                "Slat Thickness": 0.001,
                "Slat Conductivity": 0.9,
                "Front Side Slat Beam Solar Reflectance": 0.8,
                "Back Side Slat Beam Solar Reflectance": 0.8,
                "Front Side Slat Diffuse Solar Reflectance": 0.8,
                "Back Side Slat Diffuse Solar Reflectance": 0.8,
                "Slat Beam Visible Transmittance": 0,
                "Front Side Slat Beam Visible Reflectance": 0.8,
                "Back Side Slat Beam Visible Reflectance": 0.8,
                "Front Side Slat Diffuse Visible Reflectance": 0.8,
                "Back Side Slat Diffuse Visible Reflectance": 0.8,
                "Blind to Glass Distance": 0.015,
                "Blind Bottom Opening Multiplier": 0.5,
            },
            type="exterior",
            filter_by=MAINBLOCK,
            filter_by_orientation=o
        )

    try:
        os.makedirs(Path(args.temp_directory) / "_blind_runs")
    except FileExistsError:
        pass
    idf1.save(Path(args.temp_directory) / "_blind_runs" / "with_blind.idf")
    # Create orientation schedule file
    blind_df = pd.DataFrame()
    slat_df = pd.DataFrame()
    for wpsc in idf1.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        set_fields(wpsc, data={
            "Schedule Name": "shading_schedule_"+ str(uuid.uuid4()).replace("-", "")[0:8],
            "Shading Control Type": "OnIfScheduleAllows",
            "Shading Control Is Scheduled": "Yes",
            "Type of Slat Angle Control for Blinds": "ScheduledSlatAngle ",
            "Slat Angle Schedule Name": "slat_schedule_"+ str(uuid.uuid4()).replace("-", "")[0:8],
        })
        
        blind_df[wpsc.Schedule_Name] = [0 for _ in range(8760)]
        slat_df[wpsc.Slat_Angle_Schedule_Name] = [0 for _ in range(8760)]
    
    blind_df.set_index(res.index, inplace=True)
    blind_df.to_csv(Path(args.temp_directory) / "_blind_runs" / "_blind.csv", sep=";")
    slat_df.set_index(res.index, inplace=True)
    slat_df.to_csv(Path(args.temp_directory) / "_blind_runs" / "slat.csv", sep=";")
    step4 = copy.deepcopy(idf1)
    set_step_outputs(step4)
    step4.save(Path(args.temp_directory) / "idf_steps" / "step_4.idf")

    # Create Schedule:Files for Blind
    blind_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs" / "_blind.csv", sep=";", index_col=0)
    columns = [c.replace("ON) ", "ON)") for c in list(blind_file.columns)]
    for wpsc in idf1.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        sf = idf1.newidfobject("SCHEDULE:FILE")
        set_fields(sf, data={
            "Name": wpsc.Schedule_Name,
            "Schedule Type Limits Name": "On/Off",
            "Rows to Skip at Top": 1,
            "Column Separator": "Semicolon",
            "File Name": str(Path(args.temp_directory) / "_blind_runs" / "_blind.csv"),
            "Column Number": columns.index(wpsc.Schedule_Name) + 2,
        })
    step5 = copy.deepcopy(idf1)
    set_step_outputs(step5)
    step5.save(Path(args.temp_directory) / "idf_steps" / "step_5.idf")

    # Create Schedule:Files for slat
    blind_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs" / "slat.csv", sep=";", index_col=0)
    columns = [c.replace("ON) ", "ON)") for c in list(blind_file.columns)]
    for wpsc in idf1.idfobjects["WINDOWPROPERTY:SHADINGCONTROL"]:
        sf = idf1.newidfobject("SCHEDULE:FILE")
        set_fields(sf, data={
            "Name": wpsc.Slat_Angle_Schedule_Name,
            "Schedule Type Limits Name": "Any Number",  # TODO: Change it
            "Rows to Skip at Top": 1,
            "Column Separator": "Semicolon",
            "File Name": str(Path(args.temp_directory) / "_blind_runs" / "slat.csv"),
            "Column Number": columns.index(wpsc.Slat_Angle_Schedule_Name) + 2,
        })
    step6 = copy.deepcopy(idf1)
    set_step_outputs(step6)
    step6.save(Path(args.temp_directory) / "idf_steps" / "step_6.idf")

    # Find occupancy values.
    idf_editor.change_runperiod(idf1, "-".join((day, month)), "-".join((day, month)))
    idf_editor.set_outputs(idf1, ["Zone People Occupant Count"], "*", frequency="Timestep", reset=True)
    try:
        os.makedirs(Path(args.temp_directory) / "occupancy_runs")
    except FileExistsError:
        pass
    idf1.save(Path(args.temp_directory) / "occupancy_runs" / "test.idf")
    print("Evaluating occupancy schedules...")
    idf1.run(output_directory=Path(args.temp_directory) / "occupancy_runs", readvars=True, verbose="q")
    res = pd.read_csv(Path(args.temp_directory) / "occupancy_runs" / "eplusout.csv")
    res = EnergyPlusKPI.get_occupancy(res, "kpi")
    res.index = res["Date/Time"].apply(EnergyPlusKPI.convert_to_datetime)
    res = res.resample("1H").mean()

    # Elaborate occupancy values and select hours.
    blind_filename = Path(args.temp_directory) / "_blind_runs/_blind.csv"
    slat_filename = Path(args.temp_directory) / "_blind_runs/slat.csv"
    ach_filename = Path(args.temp_directory) / "schedule_runs/ach_schedules.csv"
    blind_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs/_blind.csv", sep=";", index_col=0)
    slat_file = pd.read_csv(Path(args.temp_directory) / "_blind_runs/slat.csv", sep=";", index_col=0)
    ach_file = pd.read_csv(Path(args.temp_directory) / "schedule_runs/ach_schedules.csv", sep=";", index_col=0)

    # Change runperiod to make 24h forecast analysis supporting ACM.
    start = DAY - datetime.timedelta(days=16)
    end = DAY + + datetime.timedelta(days=2)
    idf_editor.change_runperiod(
        idf1,
        "-".join((str(start.day).zfill(2), str(start.month).zfill(2))),
        "-".join((str(end.day).zfill(2), str(end.month).zfill(2))),
    )
    idf1.idfobjects["TIMESTEP"][0].Number_of_Timesteps_per_Hour = 6
    idf_editor.set_outputs(idf1, [None], None, frequency="RunPeriod", reset=True)
    idf_editor.set_outputs(idf1, [None], None, frequency="Timestep", reset=False)
    idf_editor.set_outputs(idf1, ["Site Outdoor Air Drybulb Temperature"], "*", frequency="Timestep", reset=False)
    idf_editor.set_outputs(idf1, ["Zone Operative Temperature"], "*", frequency="Timestep", reset=False)

    current_hour = DAY.hours_of_year()
    current_mid_hour = MyDateTime.combine(DAY, MyDateTime.min.time()).hours_of_year() + 24
    # current_mid_hour = DAY.hours_of_year()
    print("Initial hour: ", current_mid_hour)

    # for h in [[0, 1, 2, 3, 4, 5, 6], 7, 8, 9, [10, 11, 12, 13, 14, 15, 16, 17], 18, 19, 20, 21, [22, 23]]:
    # for h in [[0, 1, 2, 3, 4, 5, 6], [7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17], [18, 19, 20, 21], [22, 23]]:
    for h in [[0, 1, 2, 3, 4, 5, 6], [7, 8], [9, 10], 11, 12, 13, 14, [15, 16], [17, 18, 19], [20, 21, 22, 23]]:
        try:
            h = int(h + current_mid_hour)
        except Exception:
            h = [int(x) + current_mid_hour for x in h]
        input["actions"]["edit_csv_schedule_blind"] = {
            "csv": [str(blind_filename)],
            "hours": [h],
            "columns": [list(blind_file.columns)],
            "value": [0, 1]
        }
        input["actions"]["edit_csv_schedule_slat"] = {
            "csv": [str(slat_filename)],
            "hours": [h],
            "columns": [list(slat_file.columns)],
            "value": [0, 45]
        }
        input["actions"]["edit_csv_schedule_ach"] = {
            "csv": [str(ach_filename)],
            "hours": [h],
            "columns": [list(ach_file.columns)],
            "value": [0, 0.01, 0.02, 0.03, 0.04, 0.05]
        }
        input["kpi"]["adaptive_comfort_model"]["when"] = {
            "start": "1970/"
            + datetime.datetime.strftime(DAY + datetime.timedelta(days=1), "%m/%d ")
            + "00:00:00",
            "end": "1970/"
            + datetime.datetime.strftime(DAY + datetime.timedelta(days=1), "%m/%d ")
            + "23:00:00",
        }

        # Edit schedule in order to use the current hour.
        idf_editor.set_outputs(
            idf1,
            [
                "Zone Ventilation Air Change Rate",
                "Zone Infiltration Air Change Rate",
            ],
            "*",
            "hourly",
            reset=False,
        )

        # Runner instance.
        runner = Runner(
            idf1,
            input,
            **config,
            checkpoint_interval=args.checkpoint_interval,
            graph=True
        )

        # Simulating hour h.
        runner.create_dataframe(input, args.original)
        df = copy.deepcopy(runner.df)
        to_remove = df[df["edit_csv_schedule_blind.value"] == 0]
        to_remove.sort_values(by="edit_csv_schedule_slat.value", inplace=True)
        print(to_remove["edit_csv_schedule_slat.value"])
        to_remove = to_remove.drop(columns=[
                                    "edit_csv_schedule_slat.value",
                                    "edit_csv_schedule_slat.hours",
                                    "edit_csv_schedule_slat.columns",
                                    "edit_csv_schedule_ach.hours",
                                    "edit_csv_schedule_ach.columns",
                                    "edit_csv_schedule_blind.hours",
                                    "edit_csv_schedule_blind.columns",                                 
                                    ])
        to_remove = to_remove.duplicated()
        to_remove = to_remove[to_remove].index
        runner.df.drop(to_remove, inplace=True)
        runner.df.reset_index(drop=True, inplace=True)
        print(runner.df)
        runner.df.to_csv("test.csv", sep=";")
        print("Running simulations for hour", h)
        runner.run()

        # Read eplusout.csv
        filename = Path(args.output_directory) / "data_res.csv"
        res = pd.read_csv(filename, sep=';', index_col=0)
        res = res.join(res["adaptive_comfort_model"].apply(json.loads).apply(pd.Series))
        res.sort_values(
            by=[
                "cat over III",
                "cat under III",
                "cat III up",
                "cat III down",
                # "cat II up",
                # "cat II down",
                "edit_csv_schedule_blind.value",
                "edit_csv_schedule_ach.value",
                "cat I"
            ],
            ascending=[True, True, True, True,
            # True, True,
            True, True, False],
            inplace=True
        )
        
        # Sort simulations by parameters.
        # Take best parameters
        ach = res["edit_csv_schedule_ach.value"].iloc[0]
        slat = res["edit_csv_schedule_slat.value"].iloc[0]
        blind = res["edit_csv_schedule_blind.value"].iloc[0]
        print("ACH: ", ach)
        print("Slat angle: ", slat)
        print("Blind activation: ", blind)
        print("Finished simulation hours", h)

        # Assign best parameters to current hour
        ach_file.loc[ach_file.index[h], ach_file.columns] = ach
        slat_file.loc[slat_file.index[h], slat_file.columns] = slat
        blind_file.loc[blind_file.index[h], blind_file.columns] = blind
        ach_file.to_csv(ach_filename, sep=";")
        slat_file.to_csv(slat_filename, sep=";")
        blind_file.to_csv(blind_filename, sep=";")

    ach_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
        Path(args.output_directory) / "temp_ach.csv", sep=";"
    )
    slat_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
        Path(args.output_directory) / "temp_slat.csv", sep=";"
    )
    blind_file.iloc[current_mid_hour : current_mid_hour + 24].to_csv(
        Path(args.output_directory) / "temp_blind.csv", sep=";"
    )

    # Blind activation
    # df = pd.read_csv(Path(args.output_directory) / "temp_blind.csv", index_col=0, sep=";")
    # df.columns = df.columns.str.replace(":Schedule Value", " ")
    # df.columns = df.columns.str.replace("Hourly:ON", " ")
    # df.columns = df.columns.str.replace(" ", "")
    # (df.iloc[:,0]*1).plot(kind="bar", figsize=(20, 10), color="tab:orange")
    # plt.title("Best blind activations in the future 24 hours")
    # # plt.legend(loc="center right")
    # plt.gcf().autofmt_xdate()
    # plt.savefig(Path(args.output_directory) / "24h-blind.png", transparent=False, facecolor="white")

    # Blind activation (New version)
    df = pd.read_csv(
        Path(args.output_directory) / "temp_blind.csv",
        index_col=0,
        sep=";",
        parse_dates=True,
    )
    df2 = pd.read_csv(
        Path(args.output_directory) / "temp_slat.csv",
        index_col=0,
        sep=";",
        parse_dates=True,
    )
    df2.iloc[11, 0] = 45
    df2 = df2.replace(0, 1).replace(45, 0.5)
    df.set_index(df.index.time, inplace=True)
    df2.set_index(df2.index.time, inplace=True)
    where = df | df2
    blind = df2.iloc[:, 0].where(where.iloc[:, 0])
    blind.plot(kind="bar", figsize=(20, 10), color="tab:orange")
    plt.gcf().autofmt_xdate()
    plt.savefig(
        Path(args.output_directory) / "24h-blind-v2.png",
        transparent=False,
        facecolor="white",
    )

    # ACH
    df = pd.read_csv(
        Path(args.output_directory) / "temp_ach.csv",
        index_col=0,
        sep=";",
        parse_dates=True,
    )

    df.columns = df.columns.str.replace(":Schedule Value", " ")
    df.columns = df.columns.str.replace("Hourly:ON", " ")
    df.columns = df.columns.str.replace(" ", "")
    df.set_index(df.index.time, inplace=True)
    (df.iloc[:, 0]*100).plot(kind="bar", figsize=(20, 10))
    plt.title("Best ACH values in the future 24 hours")
    plt.gcf().autofmt_xdate()
    plt.savefig(
        Path(args.output_directory) / "24h-ach.png",
        transparent=False,
        facecolor="white",
    )


if __name__ == "__main__":
    main()
