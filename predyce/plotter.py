# -*- coding: utf-8 -*-
"""This file is a draft."""
#%%1
import pandas as pd
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

plt.rcParams["figure.constrained_layout.use"] = True
plt.rcParams.update({"font.size": 15})
import json as js
import numpy as np
from pathlib import Path
import ast

CSV = Path("S:/Out/data_res.csv")
df = pd.read_csv(CSV, sep=";")

ranges = [0, 1, 2]
#%%1D en sig
plt.figure(figsize=(6, 4))
line_styles = ["-", "--", ":"]
heat_colors = ["red", "red", "red"]
cool_colors = ["blue", "blue", "blue"]
for i in ranges:
    en_sig = js.loads(df.loc[i, "energy_signature"].replace("'", '"'))
    plt.plot(
        en_sig["1D"]["cooling"]["deltaT"],
        en_sig["1D"]["cooling"]["cooling"],
        ls=line_styles[i],
        c=cool_colors[i],
        lw=3,
        label="Cooling",
    )
    # plt.plot(en_sig['1D']['heating']['deltaT'],en_sig['1D']['heating']['heating'], ls=line_styles[i], c=heat_colors[i], lw=3, label='Heating')
    plt.title("Energy Signature 1D, weekly aggregation")
    plt.xlabel(r"$T_{in} - T_{ex}\,\left[°C\right]$")
    plt.ylabel(
        "Energy consumption "
        + r"$\left[\frac{\mathrm{W}}{\mathrm{m}^3}\right]$"
    )
    plt.xlim(0, 20)

plt.legend(["Original", "Case 1", "Case 2"])
plt.savefig("e1d.png", transparent=True)
#%% 2D en sig
from matplotlib import cm

fig = plt.figure(figsize=(8, 6), constrained_layout=False)
ax = fig.add_subplot(111, projection="3d")
cool_colors = ["blue", "red", "green"]
labels = ["Original", "Case 1", "Case 2"]
surf_cool = []
for i in ranges:
    en_sig = js.loads(df.loc[i, "energy_signature"].replace("'", '"'))
    xx1c = np.array(en_sig["2D"]["cooling"]["deltaT"]).reshape(2, 2)
    xx2c = np.array(en_sig["2D"]["cooling"]["solarRadiation"]).reshape(2, 2)
    Z_cool = np.array(en_sig["2D"]["cooling"]["cooling"]).reshape(2, 2)
    # xx1h=np.array(en_sig['2D']['heating']['deltaT']).reshape(2,2)
    # xx2h = np.array(en_sig['2D']['heating']['solarRadiation']).reshape(2,2)
    # Z_heat =  np.array(en_sig['2D']['heating']['heating']).reshape(2,2)
    ax.plot_surface(
        xx1c, xx2c, Z_cool, color=cool_colors[i], alpha=0.5, linewidth=0
    )
    # ax.plot_surface(xx1h, xx2h, Z_heat, color='orange', alpha=0.3, linewidth=0)
    ax.set_xlabel(r"$T_{in} - T_{ex}\,\left[°C\right]$")
    ax.set_ylabel(
        "Global solar radiation\n" + r"$\left[\frac{W}{m^2}\right]$",
        labelpad=20,
    )
    ax.set_zlabel(
        "Energy consumption"
        + r"$\left[\frac{\mathrm{W}}{\mathrm{m}^3}\right]$",
        labelpad=10,
    )
    ax.set_title("Energy Signature 2D, weekly aggregation")
    # rotate the axes and update
    ax.view_init(15, 0)

    # Adjust text
    plt.rcParams["axes.titlepad"] = 40.0
    plt.rcParams["legend.borderpad"] = 0
    ax.tick_params(axis="x", which="major", pad=-5)
    ax.tick_params(axis="y", which="major", pad=2)
    ax.tick_params(axis="z", which="major", pad=5)
    ax.set_yticks([0, 100, 200, 300])
    ax.set_zticks([-20, 0, 20, 40, 60])

    surf_cool.append(
        mpatches.Patch(color=cool_colors[i], alpha=0.5, label=labels[i])
    )
    # surf_heat = mpatches.Patch(color='orange',label='Surface Heating')
ax.legend(handles=surf_cool, loc=4, bbox_to_anchor=(1, 0, 0.1, 1))
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_horizontalalignment("left")
# plt.savefig("e2d.png", transparent=True)
#%%
width = 0.25
fig, ax = plt.subplots()
labels = ["Original", "Case 1", "Case 2"]
shift = [-0.3, 0, 0.3]
x = [-1.2, 0, 1.2]
for i in ranges:
    acm = js.loads(df.loc[i, "adaptive_comfort_model"].replace("'", '"'))
    r = ax.bar(
        [c + shift[i] for c in x],
        list(acm.values()),
        width=width,
        label=labels[i],
    )
    plt.xticks(x, labels=list(acm.keys()))
    ax.legend()
#%%
plt.figure()

hist_multi = [
    ast.literal_eval(df.loc[i, "pmv"])[5 * 30 * 24 : 9 * 30 * 24]
    for i in range(3)
]
colors = ["tab:blue", "tab:red", "tab:green"]
for i in ranges:
    data = ast.literal_eval(df.loc[i, "pmv"])[5 * 30 * 24 : 9 * 30 * 24]
    plt.hist(data, alpha=0.6, color=colors[i], histtype="bar")
plt.legend(["Original", "Case 1", "Case 2"])
plt.xlabel("PMV")
plt.ylabel("No. hours")
plt.savefig("PMV.png")

plt.figure()
colors = ["tab:blue", "tab:red", "tab:green"]
for i in ranges:
    data = ast.literal_eval(df.loc[i, "ppd"])[5 * 30 * 24 : 9 * 30 * 24]
    plt.hist(data, alpha=0.6, color=colors[i], histtype="bar")
plt.legend(["Original", "Case 1", "Case 2"])
plt.xlabel("PPD")
plt.ylabel("No. hours")
plt.savefig("PPD.png")
