#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test script for idf_editor"""
from predyce import idf_editor
from predyce.IDF_class import IDF
from pathlib import Path
import pytest

P = Path(__file__).parent.absolute()


def load_idf(idd="IDD/V8-9-0-Energy+.idd", idf="IDF/A0.idf"):
    IDF.setiddname(str(P / idd))
    test_idf = IDF(str(P / idf))
    return test_idf


def test_set_fields():
    """Set value for a specific field in an IDF object."""
    obj = {"key": "value"}
    data = {"key": "new_value"}
    idf_editor.set_fields(obj, key="new_value")
    idf_editor.set_fields(obj, data=data)


def test_get_layer():
    IDF.setiddname(str(P / "IDD/V8-9-0-Energy+.idd"))
    test_idf = IDF(str(P / "IDF/A0.idf"))
    con = test_idf.idfobjects["CONSTRUCTION"][0]
    layer = idf_editor.get_layer(con, "RVAL")
    print(layer)


def test_insert_layer():
    IDF.setiddname(str(P / "IDD/V8-9-0-Energy+.idd"))
    test_idf = IDF(str(P / "IDF/A0.idf"))
    con = test_idf.idfobjects["CONSTRUCTION"][0]
    layer = idf_editor.get_layer(con, "last")
    idf_editor.insert_layer(con, layer, "Test layer", below=True)


def test_add_overhangs_simple():
    test_idf = load_idf()
    idf_editor.add_overhangs_simple(test_idf, 1, 90, 0.04)
    idf_editor.add_overhangs_projection_simple(test_idf, 1, 90, 0.04)


def test_add_overhangs_complex():
    test_idf = load_idf()
    idf_editor.add_overhangs_complex(test_idf, depth=1)


def test_add_overhangs_detailed():
    test_idf = load_idf()
    idf_editor.add_overhangs_detailed(test_idf, extension=1, shift=0.04)


def test_remove_shading():
    test_idf = load_idf(idf="IDF/A6.1.idf")
    idf_editor.remove_shading(test_idf)
    assert list(test_idf.idfobjects["SHADING:BUILDING:DETAILED"]) == []

    test_idf = load_idf(idf="IDF/A6.3.idf")
    idf_editor.remove_shading(test_idf)
    assert list(test_idf.idfobjects["SHADING:BUILDING:DETAILED"]) == []

    # Remove shading by specifiying a type which does not appear in the IDF.
    test_idf = load_idf(idf="IDF/A6.3.idf")
    idf_editor.remove_shading(test_idf, which="Shading:Overhang")
    assert list(test_idf.idfobjects["SHADING:BUILDING:DETAILED"]) != []

    # Remove shading even if IDF has no shading.
    test_idf = load_idf(idf="IDF/A0.idf")
    idf_editor.remove_shading(test_idf)
    assert list(test_idf.idfobjects["SHADING:BUILDING:DETAILED"]) == []


def test_add_blind():
    IDF.setiddname(str(P / "IDD/V8-9-0-Energy+.idd"))
    test_idf = IDF(str(P / "IDF/A0.idf"))
    blind_data = {
        "Name": "20001",
        "Slat Width": 0.025,
        "Slat Separation": 0.01875,
        "Slat Thickness": 0.001,
        "Slat Conductivity": 0.9,
        "Front Side Slat Beam Solar Reflectance": 0.8,
        "Back Side Slat Beam Solar Reflectance": 0.8,
        "Front Side Slat Diffuse Solar Reflectance": 0.8,
        "Back Side Slat Diffuse Solar Reflectance": 0.8,
        "Slat Beam Visible Transmittance": 0,
        "Front Side Slat Beam Visible Reflectance": 0.8,
        "Back Side Slat Beam Visible Reflectance": 0.8,
        "Front Side Slat Diffuse Visible Reflectance": 0.8,
        "Back Side Slat Diffuse Visible Reflectance": 0.8,
        "Blind to Glass Distance": 0.015,
        "Blind Bottom Opening Multiplier": 0.5,
    }
    idf_editor.add_blind(test_idf, type="exterior", blind_data=blind_data)


def test_set_block_beam_solar():
    test_idf = load_idf()
    idf_editor.set_block_beam_solar(test_idf)


def test_add_slat_angle_control():
    test_idf = load_idf()
    idf_editor.add_slat_angle_control(test_idf, 45, 120)


def test_change_opening_factor():
    test_idf = load_idf()
    idf_editor.change_opening_factor(test_idf, 0.7)


def test_change_setpoints():
    test_idf = load_idf()
    idf_editor.change_setpoint(test_idf, heat=19, cool=25)


def test_change_hvac_limits():
    test_idf = load_idf()
    idf_editor.change_hvac_limits(
        test_idf, heating="LimitCapacity", cooling="LimitFlowRateAndCapacity"
    )


def test_change_dhw_temperature():
    test_idf = load_idf()
    idf_editor.change_dhw_temperature(test_idf, supply=61, mains=63)


def test_change_supply_air_temperature():
    test_idf = load_idf()
    idf_editor.change_supply_air_temperature(test_idf, "cooling", 0.5)


def test_change_heating_supply_air_humidity_ratio():
    test_idf = load_idf()
    idf_editor.change_heating_supply_air_humidity_ratio(test_idf, 0.64)


def test_add_internal_insulation_walls():
    test_idf = load_idf()
    idf_editor.add_internal_insulation_walls(
        test_idf,
        [
            "extruded polystyrene panel XPS 35 kg/m3 15 mm",
            "plaster lime and gypsum 15 mm",
        ],
    )


def test_add_external_insulation_walls():
    test_idf = load_idf()
    idf_editor.add_external_insulation_walls(
        test_idf,
        [
            "extruded polystyrene panel XPS 35 kg/m3 15 mm",
            "plaster lime and gypsum 15 mm",
        ],
    )


def test_add_cavity_insulation_walls():
    test_idf = load_idf()
    idf_editor.add_cavity_insulation_walls(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )
    test_idf = load_idf()
    idf_editor.add_cavity_insulation_walls(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 5 mm"
    )


def test_set_simplified_windows():
    test_idf = load_idf()
    idf_editor.set_simplified_windows(test_idf, "Simplified window system")


def test_change_windows_system():
    test_idf = load_idf()
    idf_editor.change_windows_system(test_idf, "1002")


def test_change_wwr():
    test_idf = load_idf()
    idf_editor.change_wwr(test_idf, 0.3)
    idf_editor.change_wwr(test_idf, 0.5)
    wwr_data = {0: 0.5, 90: 0.2, 180: 0.7, 270: 0.45}
    idf_editor.change_wwr(test_idf, wwr_map_value=wwr_data)
    idf_editor.change_wwr(test_idf, 0)


def test_add_internal_insulation_floor():
    test_idf = load_idf()
    idf_editor.add_internal_insulation_floor(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )


def test_add_external_insulation_floor():
    test_idf = load_idf()
    idf_editor.add_external_insulation_floor(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )


def test_add_insulation_flat_roof():
    test_idf = load_idf()
    idf_editor.add_insulation_flat_roof(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )


def test_add_ceiling_insulation_tilted_roof():
    test_idf = load_idf(idf="IDF/A9.3.idf")
    idf_editor.add_ceiling_insulation_tilted_roof(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )


def test_roof_insulation_tilted_roof():
    test_idf = load_idf(idf="IDF/A9.3.idf")
    idf_editor.add_roof_insulation_tilted_roof(
        test_idf, "extruded polystyrene panel XPS 35 kg/m3 15 mm"
    )


def test_change_ach():
    test_idf = load_idf()
    ach_type = "Infiltration"
    idf_editor.change_ach(test_idf, 500, ach_type)  #  All zones
    idf_editor.change_ach(
        test_idf, 30, ach_type, "Blocco2"
    )  # All Blocco2 zones
    idf_editor.change_ach(
        test_idf, 500, ach_type, "Blocco2:Zona1"
    )  # Only Blocco2:Zona1
    idf_editor.change_ach(
        test_idf, 500, ach_type, "WrongString"
    )  # No zone edited
    idf_editor.change_ach(
        test_idf, {"Blocco2:Zona1": 300, "Blocco2:Zona2": 50}, ach_type
    )

    ach_type = "Ventilation"
    idf_editor.change_ach(test_idf, 500, ach_type)  #  All zones
    idf_editor.change_ach(
        test_idf, 30, ach_type, "Blocco2"
    )  # All Blocco2 zones
    idf_editor.change_ach(
        test_idf, 500, ach_type, "Blocco2:Zona1"
    )  # Only Blocco2:Zona1
    idf_editor.change_ach(
        test_idf, 500, ach_type, "WrongString"
    )  # No zone edited
    idf_editor.change_ach(
        test_idf, {"Blocco2:Zona1": 0, "Blocco2:Zona2": 5000}, ach_type
    )


def test_get_mean_ach():
    test_idf = load_idf()
    assert idf_editor.get_mean_ach(test_idf)


def test_set_ach_schedule():
    test_idf = load_idf()
    idf_editor.set_ach_schedule(
        test_idf,
        schedule={
            "Name": "Fixed ACH",
            "Schedule_Type_Limits_Name": "Any Number",
            "Field_1": "Through: 12/31",
            "Field_2": "For: AllDays",
            "Field_3": "Until 24:00",
            "Field_4": "1",
        },
        Delta_Temperature=2,
        Maximum_Outdoor_Temperature=24,
    )


def test_get_mean_dt():
    test_idf = load_idf()
    assert idf_editor.get_mean_dt(test_idf)


def test_add_scheduled_nat_vent():
    test_idf = load_idf()
    idf_editor.add_scheduled_nat_vent(
        test_idf,
        schedule_obj="Summer (Northern Hemisphere)",
        ach=34,
        min_ind_temp=24,
        max_ind_temp=28,
        delta_temp=1,
        params={"Fan_Pressure_Rise": 5},
    )


def test_add_mechanical_ventilation():
    test_idf = load_idf()
    idf_editor.add_mechanical_ventilation(test_idf, schedule="Test_Schedule")
    idf_editor.add_mechanical_ventilation(
        test_idf,
        schedule={
            "Name": "Test_Schedule",
            "Schedule_Type_Limits_Name": "Fracion",
            "Field_1": "Through: 31 Dec,",
            "Field_2": "For: AllDays,",
            "Field_3": "Until: 24:00",
            "Field_4": 0.5,
        },
    )
    with pytest.raises(TypeError, match="Schedule must be a dict or str"):
        idf_editor.add_mechanical_ventilation(test_idf, schedule=1)


def test_change_occupancy():
    test_idf = load_idf()
    idf_editor.change_occupancy(test_idf, 0.09)
    idf_editor.change_occupancy(test_idf, 0.08, filter_by="Blocco")
    idf_editor.change_occupancy(test_idf, 0.08, filter_by="test_string")
    idf_editor.change_occupancy(test_idf, 0.5, relative=True)


def test_get_area():
    test_idf = load_idf()
    assert idf_editor.get_area(test_idf, "")


def test_get_volume():
    test_idf = load_idf()
    assert idf_editor.get_volume(test_idf, "")


def test_get_hvac_elements():
    test_idf = load_idf()
    idf_editor.get_hvac_elements(test_idf)


def test_add_hvac():
    test_idf = load_idf(idf="IDF/A10-no impianti.idf")
    cool_sch = {
        "Name": "Cooling SP Sch",
        "Schedule_Type_Limits_Name": "Temperature",
        "Field_1": "Through: 31 Dec,",
        "Field_2": "For: Weekdays SummerDesignDay,",
        "Field_3": "Until: 5:00",
        "Field_4": 100,
        "Field_5": "Until: 19:00",
        "Field_6": 26,
        "Field_7": "Until: 24:00",
        "Field_8": 100,
        "Field_9": "For: Weekdays",
        "Field_10": "Until: 24:00",
        "Field_11": 100,
        "Field_12": "For: Holidays",
        "Field_13": "Until: 24:00",
        "Field_14": 100,
        "Field_15": "For: WinterDesignDay AllOtherDays",
        "Field_16": "Until: 24:00",
        "Field_17": 100,
    }
    idf_editor.add_hvac(test_idf, cooling_schedule=cool_sch)
    test_idf = load_idf()
    cool_sch = {
        "Name": "Cooling SP Sch",
        "Schedule_Type_Limits_Name": "Temperature",
        "Field_1": "Through: 31 Dec,",
        "Field_2": "For: Weekdays SummerDesignDay,",
        "Field_3": "Until: 5:00",
        "Field_4": 100,
        "Field_5": "Until: 19:00",
        "Field_6": 26,
        "Field_7": "Until: 24:00",
        "Field_8": 100,
        "Field_9": "For: Weekdays",
        "Field_10": "Until: 24:00",
        "Field_11": 100,
        "Field_12": "For: Holidays",
        "Field_13": "Until: 24:00",
        "Field_14": 100,
        "Field_15": "For: WinterDesignDay AllOtherDays",
        "Field_16": "Until: 24:00",
        "Field_17": 100,
    }
    with pytest.raises(
        Exception, match="The building already has an HVAC system."
    ):
        idf_editor.add_hvac(test_idf, cooling_schedule=cool_sch)


def test_activate_cooling():
    test_idf = load_idf(idf="IDF/A10-no impianti.idf")
    cool_sch = {
        "Name": "Cooling SP Sch",
        "Schedule_Type_Limits_Name": "Temperature",
        "Field_1": "Through: 31 Dec,",
        "Field_2": "For: Weekdays SummerDesignDay,",
        "Field_3": "Until: 5:00",
        "Field_4": 100,
        "Field_5": "Until: 19:00",
        "Field_6": 26,
        "Field_7": "Until: 24:00",
        "Field_8": 100,
        "Field_9": "For: Weekdays",
        "Field_10": "Until: 24:00",
        "Field_11": 100,
        "Field_12": "For: Holidays",
        "Field_13": "Until: 24:00",
        "Field_14": 100,
        "Field_15": "For: WinterDesignDay AllOtherDays",
        "Field_16": "Until: 24:00",
        "Field_17": 100,
    }
    idf_editor.activate_cooling(test_idf, cool_sch)


def test_deactivate_cooling():
    test_idf = load_idf()
    idf_editor.deactivate_cooling(test_idf)
    test_idf = load_idf()
    idf_editor.deactivate_cooling(test_idf, temp=100)


def test_change_window_opening():
    test_idf = load_idf(idf="IDF/A7.1.idf")
    idf_editor.change_window_opening(test_idf, 0.07)  # 7 %
    idf_editor.change_window_opening(test_idf, 0.7)  # 70 %
    idf_editor.change_window_opening(test_idf, 7)  # 7 %
    idf_editor.change_window_opening(test_idf, 70)  # 70 %
    with pytest.raises(
        ValueError, match="Opening value must be between 0 and 100"
    ):
        idf_editor.change_window_opening(test_idf, 101)
    with pytest.raises(
        ValueError, match="Opening value must be between 0 and 100"
    ):
        idf_editor.change_window_opening(test_idf, -1)


def test_change_cd():
    test_idf = load_idf(idf="IDF/A7.1.idf")
    idf_editor.change_cd(test_idf, 0.7)
    idf_editor.change_cd(test_idf, 30)
    with pytest.raises(ValueError, match="Cd value must be between 0 and 100"):
        idf_editor.change_cd(test_idf, 101)
    with pytest.raises(ValueError, match="Cd value must be between 0 and 100"):
        idf_editor.change_cd(test_idf, -1)


def test_change_cp():
    test_idf = load_idf(idf="IDF/A7.1.idf")
    data = {"0": 0.6, "45": -0.9, "180": 0.55}
    idf_editor.change_cp(test_idf, data)


def test_change_runperiod():
    test_idf = load_idf()
    idf_editor.change_runperiod(test_idf, "01-05", "30-09", "%d-%m")


def test_set_daylight_saving():
    test_idf = load_idf()
    idf_editor.set_daylight_saving(test_idf)
    idf_editor.set_daylight_saving(test_idf, "No")
    idf_editor.set_daylight_saving(
        test_idf, "Yes", "Last Sunday in March", "Last Sunday in October"
    )
