# PREDYCE
## Politecnico di Torino 
Polytechnic University of Turin - 
Department of Architecture and Design

<img src="https://www.polito.it/images/logo_poli.svg" width="200" style="background-color:#002B49;padding:10px;">

![Python Version](https://img.shields.io/badge/python-^3.7.1-informational?style=flat&logo=python&logoColor=white)
[![License:  Creative Commons BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.en)](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.en)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Depencency management: Poetry](https://img.shields.io/badge/dependency%20management-Poetry-2f2f55.svg)](https://python-poetry.org/)

## Projects
- E-DYCE
- PRELUDE

# Installation
## Tools
- Python 3.7.1 or higher
- Poetry

## Virtual environment with Poetry
### Install [Poetry](https://github.com/python-poetry/poetry#Installation):

Linux:
``` sh
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```
Windows PowerShell (Administrator):
``` powershell 
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```
### Add Poetry to Linux Shell:
``` sh
export PATH=$PATH:$HOME/.poetry/bin
```
### [Recommended] Configure Poetry it in order to create a virtual environment in a ```.venv``` folder inside the project directory:
```sh
poetry config virtualenvs.in-project true
```
* cd in project directory.
* Create virtual environment, upgrade pip and install Python dependencies:
``` sh
poetry run pip install --upgrade pip
poetry install
```
### Activate the virtual environment:
Linux:
``` sh
source .venv/bin/activate
```
Windows:
``` powershell
.venv\Scripts\activate
```
Alternate way on Linux and Windows:
``` sh
poetry shell
```

# Documentation
Activate the virtual environment, ```cd``` in the **docs** folder and run the following command to generate HTML documentation:
``` sh
make html
```
If you have LaTeX installed, you can also generate PDF documentation:
```
make latexpdf
```
# Authors
- Chiesa Giacomo
- Fasano Francesca
- Grasso Paolo
# License
This project is licensed under the [ Creative Commons BY-NC-ND 4.0](./LICENSE)
