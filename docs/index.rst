**********************
PREDYCE documentation
**********************
----------------------------------------
Politecnico di Torino - E-DYCE - PRELUDE
----------------------------------------
This software has been created by Politecnico di Torino.

.. predyce documentation master file, created by
   sphinx-quickstart on Thu Feb 11 16:08:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PREDYCE documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Examples
   :glob:

   sensitivity-analysis


.. toctree::
   :maxdepth: 2
   :caption: Modules
   :glob:

   modules
   

.. autosummary::
   :toctree: generated

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
