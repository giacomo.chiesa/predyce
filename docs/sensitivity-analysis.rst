Ensure that you have Python 3.7.1+, `Poetry <https://python-poetry.org/>`_ and you have followed the installation procedure.


Sensitivity Analysis
====================

What you need
-------------
* EnergyPlus installed on your system
* IDF file of the same version of EnergyPlus
* EPW file(s)
* Input JSON file

.. tip::
    IDF version is found on the top of IDF file if you open it with a text editor::

        Version, 8.9.0.001;                               !- Version Identifier
    
    The IDF version must be the same as the one of EnergyPlus installed on the system;
    if not you can install the correct version of EnergyPlus or update the
    IDF file with the built-in IDFVersionUpdater of EnergyPlus under the PreProcess
    folder. For more information visit `IDF Version Updater <https://bigladdersoftware.com/epx/docs/8-9/auxiliary-programs/idf-version-updater.html>`_ page.

.. tip::
    You do not need to specify the IDD file but only its version, e.g. "8.9".

.. tip::
    All files that you need are included as examples in the PREDYCE repository.

JSON configuration file
^^^^^^^^^^^^^^^^^^^^^^^
This file is used to create a specific pool of simulation by combining different actions and KPI outputs.

JSON input file example::

    {
        "scenario": "retrofit",
        "building_name": "MainBlock",
        "preliminary_actions": {
            "change_runperiod": {
                "start": "01-05",
                "end": "30-09",
                "fmt": "%d-%m"
            }
        },
        "actions": {
            "change_ach": {
                "ach": [0, 2.5, 5],
                "ach_type": ["ventilation"]
            },
            "add_external_insulation_walls": {
                "ins_data": [
                    ["extruded polystyrene panel XPS 35 kg/m3 15 mm",
                     "plaster lime and gypsum 15 mm"
                ],
                "Thickness": [0.03],
                "Conductivity": [0.02]
            }
        },
        "outputs": [],
        "kpi": {
            "pmv_ppd": {"clo": 0.7},
            "adaptive_comfort_model": {},
            "cidh": {},
            "adaptive_residuals": {},
            "Q_c": {},
            "Q_h": {}
        }
    }

.. warning::
    Specifying the weather data as an *action* in the JSON file will override all weather settings further specified.

* Scenario: It has no effect for now.
* Building name: The name of building which specifies zones that are included in the computations.
* Preliminary actions: Actions that are executed once before creating the pool of simulations. All building will share the same preliminary actions.
* Actions: Actions that are used to create the pool of simulations.
* Outputs: EnergyPlus output variables that are saved on results.
* KPI: KPI outputs that are saved on results (some of them may return a plot).

YAML configuration file
^^^^^^^^^^^^^^^^^^^^^^^
This file is used to specify some parameters which are usually carried among different experiments and they do not need to be changed very often.

Create you own configuration file by renaming **config.example.yml** into **config.yml**.

Here you can specify different things:

* ``plot_dir``: Directory where KPI plots are saved
* ``out_dir``: Directory where CSV file of results is saved
* ``epw``: EPW file (or directory where multiple EPW files are located)
* ``num_of_cpus``: number of parallel simulations, 0 for auto
* ``idd_version``: Version of EnergyPlus and IDF, e.g. "8.9"

Running from command line 
-------------------------
Options
^^^^^^^

* ``--output-directory (-d)`` output folder
* ``--temp-directory (-t)`` temp folder
* ``--plot-directory (-p)`` plot folder
* ``--input-file (-f)`` input JSON file
* ``--weather (-w)`` EPW file or EPW directory
* ``--idd (-i)`` IDD version
* ``--jobs (-j)`` number of parallel simulations, defaults to 0 (auto)
* ``--original (-o)`` include original model in dataframe, defaults to True
* ``--checkpoint-data`` checkpoint data path
* ``--checkpoint-interval (-c)`` checkpoint interval, defaults to 128

.. warning::
    Command line options will override YAML configuration file options.

Running
^^^^^^^
From a terminal, activate the virtual environment::

    poetry shell

Run sensitivity analysis::

    python run_sensitivity_analysis.py -d "<output_directory>" -p "<plot_directory>" -f "<path>/predyce/test/sa.json" -w "<path>/predyce/test/EPW/ITA_Rome.162420_IWEC.epw" -i "8.9" "../test/IDF/building.idf" -j 1 -o

Outputs
-------
The outputs are organized as following:

* In the output folder you get a data_res.csv file with the list of all executed simulations and the respective results.
* In the plot folder you get a list of numbered subfolders which follow the indexing of data_res.csv. Inside each folder you can find the plots for the corresponding simulation.
